#include <coo.h>
#include <csf.h>
#include <utils.h>
#include <matrix.h>
#include <fstream>
#include <iostream>

using namespace Gist;
int main(int argc, char const *argv[]) {

    if(argc < 3){
        std::cout << "Usage : inpath outpath\n";
        return 1;
    }
    std::string X_path(argv[1]);
    std::cout << X_path << std::endl;
    std::ifstream f(X_path, std::ios::in); 
    Coo::coo X_coo(f);

    Csf::csf X_csf(X_coo);
    X_path.assign(argv[2]);
    std::ofstream fo(X_path);
    X_csf.write(fo);

    fo.close();
    f.close();
    return 0;
}
