#include <coo.h>
#include <csf.h>
#include <device.h>
#include <matrix.h>
#include <utils.h>
#include <fstream>
#include <vector>

int main(int argc, char const *argv[]) {

    if(argc < 4){
        std::cout << "Usage : X F mode\n";
        return 1;
    }
    std::string X_path(argv[1]);
    std::cout << X_path << std::endl;
    std::ifstream f(X_path, std::ios::in); 
    coo::coo X_coo(f);

    csf::csf X_csf(X_coo);
    size_t nmodes = X_csf.num_modes();
    std::vector<size_t> dimensions = X_csf.dims();

    std::vector<matrix::matrix> matrices;
    matrices.reserve(nmodes);
    size_t F = std::atoi(argv[2]);
    for(size_t i = 0; i < nmodes; i++) {
        matrix::matrix m(dimensions[i], F);
        m.random_init(0.0, 1.0);
        matrices.push_back(std::move(m));
        /*
        std::vector<std::vector<float>> m(dimensions[i], 
                std::vector<float>(F, 0));
        for(size_t j = 0; j < dimensions[i]; j++)
            for(size_t k = 0; k < F; k++)
                m[j][k] = (k + j + 1) * 0.1;
        matrices.push_back(matrix::matrix(m));
        */
    }

    size_t mode = std::atoi(argv[3]);
    utils::mttkrp_malloc auxmem(X_csf, matrices, mode);
    X_csf.mttkrp(mode, matrices, auxmem);
    return 0;
}
