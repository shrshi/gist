#include <coo.h>
#include <csf.h>
#include <utils.h>
#include <matrix.h>
#include <device.h>
#include <fstream>
#include <iostream>
#include <cassert>

using namespace Gist;
int main(int argc, char const *argv[]) {

    if(argc < 4){
        std::cout << "Usage : X F mode [niter]\n";
        return 1;
    }
    std::string X_path(argv[1]);
    std::cout << X_path << std::endl;
    std::ifstream f(X_path, std::ios::in); 

    if(X_path.substr(X_path.size() - 3).compare(std::string("bin")) != 0) {
        std::cout << "Usage : input tensor in binary\n" << std::endl;
        return 1;
    }
    Csf::csf X_csf(f);
    
    /*
    X_path.replace(X_path.size() - 3, 3, "tns");
    std::cout << X_path << std::endl;
    std::ifstream fi(X_path);
    Coo::coo X_coo(fi);
    Csf::csf X_csf_(X_coo);
    assert(X_csf == X_csf_);
    */

    guint nmodes = X_csf.num_modes();
    std::vector<guint> dimensions = X_csf.dims();

    std::vector<Matrix::matrix> matrices;
    matrices.reserve(nmodes);
    guint F = std::atoi(argv[2]);
    for(guint i = 0; i < nmodes; i++) {
        Matrix::matrix m(dimensions[i], F);
        m.random_init(0.0, 1.0);
        matrices.push_back(std::move(m));
    }

    size_t mode = std::atoi(argv[3]);

    int niter = 1;
    if(argc > 4) niter = std::atoi(argv[4]);
    for(int i = 0; i < niter; i++)
        X_csf.mttkrp(mode, matrices);

    return 0;
}
