#include <type.h>
#include <constants.h>
#include <kernels.h>
#include <device.h>
#include <err.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <vector>
#include <iostream>
#include <sstream>

using namespace Gist;
int main(int argc, char const *argv[]) {
    
    using bits = Utils::bitops;
    using dbits = Device::objptr<Utils::bitops>;
    using da = Device::array<gfloat>;

    guint num_elements, nsegments;
    bits flags;
    std::vector<gfloat> vals;

    num_elements = 10 * blockdim * num_elements_per_thread;
    vals.resize(num_elements);
    for(guint i = 0; i < num_elements; i++)
        vals[i] = 1;
    nsegments = 10;
    flags.resize(num_elements);
    for(guint i = 0; i < nsegments; i++)
        flags.set(i * num_elements/nsegments);

    da dev_in(vals);
    dbits dev_flags(flags);
    da dev_out(num_elements);

    guint num_elements_per_block = std::min((int)num_elements, (int)blockdim * num_elements_per_thread);
    guint num_blocks = std::max((gfloat)1.0, std::ceil((gfloat) num_elements / (gfloat)num_elements_per_block));
    guint shmem_alloc = warp_size * (sizeof(gfloat) + sizeof(guint));

    std::printf("num_elements = %lu, num_elements_per_block = %lu, num_blocks = %lu\n", num_elements, num_elements_per_block, num_blocks);

    Kernels::segscan <<< num_blocks, blockdim, shmem_alloc >>> (
            num_elements, 
            dev_in.object(), 
            dev_flags.object(), 
            dev_out.object());
    cuErrChk(cudaPeekAtLastError());
    cuErrChk(cudaDeviceSynchronize());
}
