#include <coo.h>
#include <csf.h>
#include <matrix.h>
#include <device.h>
#include <fstream>

int main(int argc, char const *argv[]) {

    if(argc < 2){
        std::cout << "Usage : X R1 R2 ..\n";
        return 1;
    }
    std::string X_path(argv[1]);
    std::cout << X_path << std::endl;
    std::ifstream f(X_path, std::ios::in); 
    coo::coo X_coo(f);

    csf::csf X_csf(X_coo);
    size_t nmodes = X_csf.num_modes();
    std::vector<size_t> dimensions = X_csf.dims();
    if(argc < nmodes + 2) {
        std::cout << "Usage : X R1 R2 ..\n";
        std::cout << "Provide rank for every mode\n";
        return 1;
    }

    std::vector<matrix::matrix> matrices;
    matrices.reserve(nmodes);
    for(size_t i = 0; i < nmodes; i++) {
        matrix::matrix m(dimensions[i], std::atoi(argv[2 + i]));
        m.random_init(1.0, 2.0);
        matrices.push_back(matrix::matrix(m));
    }
    std::cout << "Mode 0\n";
    device::objptr<matrix::matrix> out = X_csf.ttmc(0, matrices);
    /*
    for(size_t i = 0; i < nmodes; i++){
        std::cout << "Mode " << i << std::endl;
        device::objptr<matrix::matrix> out = X_csf.ttmc(i, matrices);
    }
    return 0;
    */
}
