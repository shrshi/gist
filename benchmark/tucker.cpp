#include <coo.h>
#include <csf.h>
#include <device.h>
#include <matrix.h>
#include <fstream>

int main(int argc, char const *argv[]) {

    if(argc < 2){
        std::cout << "Usage : X R1 R2 ..\n";
        return 1;
    }
    std::string X_path(argv[1]);
    std::cout << X_path << std::endl;
    std::ifstream f(X_path, std::ios::in); 
    coo::coo X_coo(f);

    csf::csf X_csf(X_coo);
    size_t nmodes = X_csf.num_modes();
    std::vector<size_t> dimensions = X_csf.dims();
    if(argc < nmodes + 2) {
        std::cout << "Usage : X R1 R2 ..\n";
        std::cout << "Provide rank for every mode\n";
        return 1;
    }

    std::vector<matrix::matrix> matrices;
    matrices.reserve(nmodes);
    for(size_t i = 0; i < nmodes; i++) {
        matrix::matrix m(dimensions[i], std::atoi(argv[2 + i]));
        m.random_init(0.0, 1.0);
        matrices.push_back(matrix::matrix(m));
    }

    float tol = 5.0e-3, fit = 0.0, fit_diff, fit_prev;
    size_t max_iters = 100;
    
    float norm_X = X_csf.norm(), norm_core, norm_residual;
    for(size_t i = 0; i < max_iters; i++) {
        std::cout << "In iteration " << i << std::endl;

        fit_prev = fit;

        norm_core = X_csf.tucker_iteration(matrices); 
        std::cout << "norm_core : " << norm_core << std::endl;

        norm_residual = std::sqrt(norm_X * norm_X - norm_core * norm_core);
        fit = 1 - norm_residual / norm_X;
        std::cout << "fit : " << fit << std::endl;
        fit_diff = std::fabs(fit_prev - fit);
        std::cout << "fit_diff : " << fit_diff << std::endl;

        if(i && fit_diff < tol) break;
    }

    return 0;
}
