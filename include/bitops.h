#ifndef BITOPS_H
#define BITOPS_H

#include <type.h>

namespace Gist {
namespace Bitops {

    struct bitops {
        guint *buf = nullptr; //8 bytes
        guint nbits = 0; //4 bytes
        guint size = 0; //4 bytes
        static constexpr guint width = guint_typewidth; //4 bytes
    };

}
}
