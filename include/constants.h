#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <type.h>
#include <cuda_runtime_api.h>

namespace Gist {

    constexpr int warp_size = 32;
    constexpr int warp_width = 5;
    constexpr int shmem_size = 48000; //bytes
    // TODO: blockDim is fixed at 128!
    constexpr int blockdim = 128;
    /* num_elements_per_thread needs to be smaller 
     * than guint_typewidth - 1! Also needs to be 
     * a power of 2 because bit operations for 
     * remainder and division
     */
    constexpr int num_elements_per_thread = 4; 
    constexpr int num_elements_per_thread_width = 2;

    template <class T>
    __device__ constexpr const T& min(const T& a, const T& b) {
        return (a < b ? a : b);
    }

    template <class T>
    __device__ constexpr const T& max(const T& a, const T& b) {
        return (a > b ? a : b);
    }

    template <class T>
    __device__ constexpr const T& abs(const T& a, const T& b) {
        return (a > b ? a : b);
    }
}
#endif
