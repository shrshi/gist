#ifndef COO_H
#define COO_H

#include <type.h>
#include <vector>
#include <iostream>
#include <vector>

namespace Gist {
namespace Tree {
    class tree;
}

namespace Matrix {
    class matrix;
}

namespace Coo {

    class coo {
        guint nmodes = 0;
        guint *dimensions = nullptr;
        guint nnz = 0;
        guint **indices = nullptr;
        gfloat *values = nullptr;
        void swap_indices(guint i, guint j);
        int compare_indices(guint i, guint j, const std::vector<guint> &sort_order);
        void quick_sort_indices(guint low, guint high, const std::vector<guint> &sort_order);
        public:
            explicit coo(std::ifstream &f);
            ~coo() {
                if(dimensions) delete[] dimensions;
                if(indices){
                    for(guint m = 0; m < nmodes; m++)
                        delete[] indices[m];
                    delete[] indices;
                }
                if(values)
                    delete[] values;
            }
            coo(const coo&) = default;
            coo& operator=(const coo&) = default;
            coo(coo&&) = default;
            coo& operator=(coo&&) = default;                  
            void print(std::ostream &f) const;
            void sort(const std::vector<guint> &sort_order);
            std::vector<gfloat> mttkrp(guint, const std::vector<Matrix::matrix>&);

            guint num_modes() const { return nmodes; }
            std::vector<guint> dims() const;
            guint num_nonzeros() const { return nnz; }

            friend class Tree::tree;
    };
}
}
#endif
