#ifndef CSF_H
#define CSF_H

#include <type.h>
#include <tree.h>
#include <fstream>
#include <vector>

using namespace std;
namespace Gist {
namespace Coo {
    class coo;
}

namespace Tree {
    class tree;
}

namespace Matrix {
    class matrix;
}

namespace Utils {
    struct mttkrp_malloc;
    struct cpd_malloc;
}

namespace Device {

    template <class T> class array;
    template <class T> class objptr;
    template <> class objptr<Matrix::matrix>;
}

namespace Csf {

    class csf {
        guint nmodes;
        vector<guint> dimensions;
        guint nnz;
        vector<Tree::tree> tensors;
        public:
            explicit csf(Coo::coo &coo_t);
            explicit csf(ifstream &f);
            ~csf() = default;
            csf(const csf&) = delete;
            csf& operator=(const csf&) = delete;
            csf(csf&&) = delete;
            csf& operator=(csf&&) = delete;
            void print(ostream &f) const;
            void stringify(stringstream &f) const;
            void print(const guint, ostream &f) const;
            void stringify(const guint, stringstream &f) const;
            void write(ofstream &f);
            vector<gfloat> mttkrp_cpu(guint, vector<Matrix::matrix>&);
            Device::array<gfloat> mttkrp(guint, vector<Matrix::matrix>&);
            Device::objptr<Matrix::matrix> mttkrp(guint, vector<Matrix::matrix>&, Utils::mttkrp_malloc&); 
            gfloat cpd_iteration(vector<Matrix::matrix>&, Utils::cpd_malloc &, string);
            gfloat cpd_fit(Utils::cpd_malloc &, const Matrix::matrix &);

            Device::objptr<Matrix::matrix> ttmc(guint, vector<Matrix::matrix>&); 
            gfloat tucker_iteration(vector<Matrix::matrix>&);

            gfloat norm() const;
            guint num_modes() const { return nmodes; }
            vector<guint> dims() const { return dimensions; }
            guint num_nz() const { return nnz; }

            vector<guint> level_map(guint mode) const; 
            vector<guint> rootsize() const;
            vector<guint> max_lsize() const;
            void init_tree_offsets(guint);

            friend bool operator==(const csf &lhs, const csf &rhs);
    };

}
}
#endif
