#ifndef DEVICE_H
#define DEVICE_H

#include <type.h>
#include <matrix.h>
#include <tree.h>
#include <utils.h>
#include <err.h>
#include <cuda_runtime_api.h>
#include <cassert>
#include <iostream>
#include <vector>

namespace Gist {
using namespace Matrix;
namespace Device {

    template <class T>
    class ptr {
        protected:
            T *obj = nullptr;
        private:
            void init(guint num = 1) {
                cuErrChk(cudaMalloc((void**)&obj, num * sizeof(T)));
            }
            void swap(ptr<T> &rhs) {
                std::swap(obj, rhs.obj);
            }
        protected:
            ptr(guint num) { init(num); }
        public:
            ptr() { init(); }
            ~ptr(){
                if(obj) {
                    cuErrChk(cudaFree(obj));
                    obj = nullptr;
                }
            }
            ptr(const ptr&) = delete;
            ptr& operator=(const ptr&) = delete;
            ptr(ptr&& rhs) {
                rhs.swap(*this);
            }
            ptr& operator=(ptr &&rhs) {
                rhs.swap(*this);
                return *this;
            }
            T *object() const { return obj; }
    };

    template <class T>
    class array : public ptr<T> {
        guint size = 0;
        void swap(array &rhs) { 
            std::swap(size, rhs.size);
        }
        public:
            array(guint num) : ptr<T>(num) { size = num; }
            array(T *arr, guint n) : ptr<T>(n) {
                size = n;
                cuErrChk(cudaMemcpy(ptr<T>::obj, arr, 
                            size * sizeof(T), cudaMemcpyHostToDevice));
            }
            array(std::vector<T> &&arr) : ptr<T>(arr.size()) {
                size = arr.size();
                cuErrChk(cudaMemcpy(ptr<T>::obj, arr.data(), 
                            size * sizeof(T), cudaMemcpyHostToDevice));
            }
            array(const std::vector<T> &arr) : ptr<T>(arr.size()) {
                size = arr.size();
                cuErrChk(cudaMemcpy(ptr<T>::obj, arr.data(), 
                            size * sizeof(T), cudaMemcpyHostToDevice));
            }
            ~array(){ size = 0; }
            array(const array&) = delete;
            array& operator=(const array&) = delete;
            array(array &&rhs) : ptr<T>(std::move(rhs)) {
                rhs.swap(*this);
            }
            array& operator=(array &&rhs) {
                ptr<T>::operator=(std::move(rhs));
                rhs.swap(*this);
                return *this;
            }
            std::vector<T> retrieve(){
                std::vector<T> ret(size, 0);
                cuErrChk(cudaMemcpy(ret.data(), ptr<T>::obj, 
                        size * sizeof(T), cudaMemcpyDeviceToHost));
                return ret;
            }
            void constant(T c) {
                T *cs = new T[size];
                for(guint i = 0; i < size; i++)
                    cs[i] = c;
                cuErrChk(cudaMemcpy(ptr<T>::obj, cs, size * sizeof(T), 
                            cudaMemcpyHostToDevice));
                delete[] cs;
            }
            void assign(T *v, guint n, cudaStream_t stream = 0) {
                assert(n <= size);
                cuErrChk(cudaMemcpyAsync(ptr<T>::obj, v, n * sizeof(T), cudaMemcpyHostToDevice, stream));
            }
            guint length() const { return size; }
    };

    /*
       *
       * SPECIALIZATIONS (to be moved?)
       *
   */
    template <class T>
    class objptr : public ptr<T> {};

    template <>
    class objptr<matrix> : public ptr<matrix> {
        gfloat *vals = nullptr;
        guint nrows = 0, ncols = 0;
        void init(const matrix &o);
        void swap(objptr &rhs);
        public:
            objptr() {}
            objptr(const matrix &m);
            objptr(const guint nr, const guint nc);
            ~objptr();
            objptr(const objptr&) = delete;
            objptr& operator=(const objptr&) = delete;
            objptr(objptr&& rhs); 
            objptr& operator=(objptr &&rhs);
            void clone(const objptr &o, cudaStream_t stream = 0);
            void resize(guint nr, guint nc, cudaStream_t stream = 0);
            void assign(const matrix &, cudaStream_t stream = 0);
            matrix retrieve();
            void retrieve(matrix &m, cudaStream_t s = 0);
            void constant(gfloat s, cudaStream_t stream = 0);
            gfloat* values() const { return vals; }
            guint num_rows() const { return nrows; }
            guint num_cols() const { return ncols; }

            void rand_init();
            void rand_init(const std::vector<guint> &, cudaStream_t s = 0);
            void rand_init(guint num_cols);
            void mmul(const objptr<matrix> &lhs, 
                    const objptr<matrix> &rhs, 
                    bool lhs_transpose = false, 
                    bool rhs_transpose = false,
                    gfloat alpha = 1.0f,
                    gfloat beta = 0.0f, 
                    cudaStream_t stream = 0);
            void densify(guint out_nrows, 
                    const std::vector<guint> &cur_rows);
            void qr();
            void qr_Q();
            void svd(objptr<matrix>&, objptr<matrix>&, 
                    array<gfloat>&);
            void left_singular_vectors();
            objptr<matrix> submatrix(const std::vector<guint> &row_selector);
            gfloat norm();
            array<gfloat> normalize(std::string nrmtype);
            int chol(cusolverDnHandle_t handle = nullptr, cudaStream_t stream = 0);
            void evd(array<gfloat>&);
            array<gfloat> linsolve(objptr<matrix> &A, int,
                    objptr<matrix> &B, const std::vector<guint>&, 
                    std::string nrmtype);
            void hadamard_product(const std::vector<objptr<matrix>>&, int except_pos, matrix**, cudaStream_t stream = 0);
            void hadamard_product(std::vector<guint> pos, const matrix &m);
            void multiply_rows(const array<gfloat> &s);
    };

    template <>
    class objptr<Tree::dev_level> : public ptr<Tree::dev_level> {
        guint *lidx = nullptr;
        guint lsize = 0;
        public:
            objptr(const Tree::dev_level &t);
            objptr(const guint);
            void assign(const Tree::dev_level &, cudaStream_t s = 0);
            ~objptr();
    };

    template <>
    class objptr<Utils::bitops> : public ptr<Utils::bitops> {
        guint *buf = nullptr;
        guint nbits = 0;
        guint size = 0;
        public:
            objptr(const Utils::bitops &b);
            ~objptr();
    };
}
}
#endif
