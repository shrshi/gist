#ifndef KERNELS_H
#define KERNELS_H

#include <type.h>
#include <cuda_runtime_api.h>
#include <cuda.h>

namespace Gist {
namespace Matrix {
    class matrix;
}

namespace Tree {
    class dev_level;
}

namespace Utils {
    class bitops;
}

namespace Kernels {

    using Matrix::matrix;

    __global__ void select_scale(const guint num_elements_per_block, 
            matrix const *__restrict__ factor, 
            Tree::dev_level const *__restrict__ pos, 
            gfloat const *__restrict__ scale, 
            gfloat *__restrict__ out);

    __global__ void segred(const guint num_elements_per_block, 
            const guint num_elements, 
            Utils::bitops const *__restrict__ flags, 
            gfloat *__restrict__ out);

    __global__ void segscan(const guint num_elements, 
            gfloat *__restrict__ dvals, 
            guint *__restrict__ dpos, 
            gfloat *__restrict__ outval);

    __global__ void scan(const guint num_elements, 
            guint const *__restrict__ in, 
            guint *__restrict__ out);

    __global__ void segscan(const guint num_elements, 
            gfloat const *__restrict__ in, 
            Utils::bitops const *__restrict__ flags, 
            gfloat *__restrict__ out);

    __device__ void retrieve(Utils::bitops const *__restrict__, 
            guint, guint, guint &, guint&);

    __global__ void mttkrp3(Matrix::matrix const *__restrict__ l2_matrix, 
            Tree::dev_level const *__restrict__ l2pos, 
            gfloat const *__restrict__ scale, 
            Utils::bitops const *__restrict__ l2_flags, 
            uint64_t const *__restrict__ l2_segments,
            Matrix::matrix const *__restrict__ l1_matrix, 
            Tree::dev_level const *__restrict__ l1pos, 
            Utils::bitops const *__restrict__ l1_flags, 
            uint64_t const *__restrict__ l1_segments,
            gfloat *__restrict__ outval
            );

    __global__ void mttkrp4(
            Matrix::matrix const *__restrict__ l3_matrix, 
            Tree::dev_level const *__restrict__ l3pos, 
            gfloat const *__restrict__ scale, 
            Utils::bitops const *__restrict__ l3_flags, 
            guint const *__restrict__ l3_segments,
            Matrix::matrix const *__restrict__ l2_matrix, 
            Tree::dev_level const *__restrict__ l2pos, 
            Utils::bitops const *__restrict__ l2_flags, 
            guint const *__restrict__ l2_segments,
            Matrix::matrix const *__restrict__ l1_matrix, 
            Tree::dev_level const *__restrict__ l1pos, 
            Utils::bitops const *__restrict__ l1_flags, 
            guint const *__restrict__ l1_segments,
            gfloat *__restrict__ outval
            );

    __global__ void setup (matrix const *__restrict__ leaf_matrix, 
            Tree::dev_level const *__restrict__ leaf, 
            gfloat const *__restrict__ vals, const guint out_ncols);

    __global__ void setup (matrix const *__restrict__ leaf_matrix, 
            Tree::dev_level const *__restrict__ leaf, 
            gfloat const *__restrict__ vals, 
            matrix *__restrict__ setup_leaves);

    __global__ void setup (matrix const *__restrict__ in, 
            guint const *__restrict__ rows, 
            gfloat const *__restrict__ vals, 
            matrix *__restrict__ out);

    __global__ void kronecker_product (matrix *__restrict__ out,
            Tree::dev_level const *__restrict__ rows, 
            matrix const *__restrict__ lhs,
            matrix const *__restrict__ rhs);

    __global__ void hadamard_product (matrix *__restrict__ out,
            Tree::dev_level const *__restrict__ rows, 
            matrix *__restrict__ lhs,
            matrix const *__restrict__ rhs);

    __global__ void gram_hadamard(matrix *__restrict__ out, 
            matrix **__restrict__ grams, 
            const guint num_matrices);

    __global__ void scale(matrix *__restrict__ A, 
            gfloat const *__restrict__ s, 
            matrix *__restrict__ out = NULL, 
            bool inplace = true);

    __global__ void insert(matrix *__restrict__ out, 
            matrix *__restrict__ submtx, 
            guint *__restrict__ inds);

    __global__ void power(gfloat *__restrict__ v, guint len, gfloat power, 
            gfloat *__restrict__);

    __global__ void abs(gfloat *__restrict__ v, const guint len, gfloat *__restrict__ out); 

    __global__ void copy(gfloat *__restrict__ dest, 
            gfloat const *__restrict__ src, 
            guint len);

    __global__ void constant(gfloat *__restrict__ arr,
            const guint n,
            const gfloat s);

    __global__ void batched_multiply(gfloat *__restrict__ arr, 
            guint len, 
            gfloat const *__restrict__ mults, guint, guint batchsize);

    __global__ void diag(gfloat *__restrict__ out, 
            gfloat const *__restrict__ in, guint n);

    __global__ void diagonal(gfloat *__restrict__ out, 
            gfloat const *__restrict__ in, guint n);

}
}
#endif
