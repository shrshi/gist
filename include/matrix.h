#ifndef MATRIX_H
#define MATRIX_H

#include <type.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cstddef>
#include <cstring>
#include <vector>
#include <iostream>
#include <iterator>

#ifdef USE_LAPACKE
#include <lapacke.h>
#endif

namespace Gist {
namespace Tree {
    struct dev_level;
}

namespace Matrix {
    class matrix;
}

namespace Device {

    template <class T> class objptr;
    template <> class objptr<Matrix::matrix>;
}

namespace Utils {
    class bitops;
}

namespace Kernels {
    __global__ void mttkrp3(Matrix::matrix const *__restrict__ l2_matrix, 
            Tree::dev_level const *__restrict__ l2pos, 
            gfloat const *__restrict__ scale, 
            Utils::bitops const *__restrict__ l2_flags, 
            uint64_t const *__restrict__ l2_segments,
            Matrix::matrix const *__restrict__ l1_matrix, 
            Tree::dev_level const *__restrict__ l1pos, 
            Utils::bitops const *__restrict__ l1_flags, 
            uint64_t const *__restrict__ l1_segments,
            gfloat *__restrict__ outval
            );
}

namespace Matrix {

    class matrix {
        guint nrows = 0;
        guint ncols = 0;
        gfloat* values = nullptr; //stored in column major format
        public:
            matrix(); 
            matrix(uint64_t, uint64_t);
            matrix(uint64_t, uint64_t, gfloat);
            matrix(const uint64_t, const uint64_t, const std::vector<gfloat> &); 
            matrix(std::vector<std::vector<gfloat>>&);
            matrix(const std::initializer_list<std::initializer_list<gfloat>> il);
            ~matrix();
            matrix(const matrix&);
            void swap(matrix&);
            matrix& operator=(matrix&) = delete;
            matrix(matrix&&);
            matrix& operator=(matrix&&);
            matrix& operator+=(const matrix&);

            void random_init(gfloat a = 0.0, gfloat b = 1.0);
            void print(std::ostream &) const;
            __device__ void printf() const;
            void stringify(std::stringstream &f) const;

            __host__ __device__ guint num_cols() const { return ncols; }
            __host__ __device__ guint num_rows() const { return nrows; }
            gfloat valueat(guint row, guint col) const {
                return values[col * nrows + row];
            }

            __device__ void select_scale(Tree::dev_level const *__restrict__, gfloat const *__restrict__, gfloat*) const;
            __device__ void hadamard(guint const, Tree::dev_level const *__restrict__, guint const *__restrict__, guint const, guint const, gfloat *, guint) const;

            __device__ void select_scale(volatile gfloat *__restrict__, const guint, Tree::dev_level const *__restrict__, gfloat const *__restrict__ ) const;
            void select_scale(const matrix &in, const Tree::dev_level &pos, const std::vector<gfloat> &scale);

            __device__ void hadamard(Tree::dev_level const *__restrict__, Tree::dev_level const *__restrict__, guint const *__restrict__, gfloat*) const;
            __device__ void hadamard(Tree::dev_level const *__restrict__, Tree::dev_level const *__restrict__, guint const *__restrict__, gfloat&) const;

            __device__ void multiply_rows(Tree::dev_level const *__restrict__, gfloat const *__restrict__, matrix *__restrict__) const;
            __device__ void multiply_rows(guint const *__restrict__, 
                    gfloat const *__restrict__, 
                    matrix *__restrict__) const;
            __device__ void multiply_rows(Tree::dev_level const *__restrict__ , gfloat const *__restrict__ , const guint , gfloat *) const;
            __device__ void divide_cols(matrix *__restrict__ A, 
                    gfloat const *__restrict__ s);
            __device__ void divide_cols(gfloat const *__restrict__ s);
            __device__ void insert(matrix const *__restrict__ submtx, 
                    guint const *__restrict__ inds);
            void insert(const matrix &, std::vector<guint>&);

            __device__ void hadamard_product(
                    Tree::dev_level const *__restrict__, 
                    matrix const *__restrict__,
                    matrix const *__restrict__); 
            __device__ void hadamard_product(matrix **__restrict__, guint);
            __device__ void kronecker_product(
                    Tree::dev_level const *__restrict__,
                    matrix const *__restrict__,
                    matrix const *__restrict__);
            
            matrix kronecker_product(const std::vector<guint> &row_selector, 
                    const matrix &rhs) const;
            matrix hadamard_product(const std::vector<guint> &row_selector, 
                    const matrix &rhs) const;
            int lapacke_linsolve(matrix &B);
            std::vector<gfloat> normalize(std::string nrmtype);

            friend class Device::objptr<matrix>;
            friend bool operator==(const matrix &lhs, const matrix &rhs);
            friend matrix operator*(const matrix &lhs, const matrix &rhs);
            friend __global__ void Kernels::mttkrp3(
                    Matrix::matrix const *__restrict__ l2_matrix, 
                    Tree::dev_level const *__restrict__ l2pos, 
                    gfloat const *__restrict__ scale, 
                    Utils::bitops const *__restrict__ l2_flags, 
                    uint64_t const *__restrict__ l2_segments,
                    Matrix::matrix const *__restrict__ l1_matrix, 
                    Tree::dev_level const *__restrict__ l1pos, 
                    Utils::bitops const *__restrict__ l1_flags, 
                    uint64_t const *__restrict__ l1_segments,
                    gfloat *__restrict__ outval
                );
    };

    __host__ __device__ matrix operator+(const matrix&, const matrix&);
}
}
#endif
