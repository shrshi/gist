#ifndef SVD_H
#define SVD_H

#include <vector>

namespace matrix {
    class matrix;
}

namespace device {
    template <class T> class objptr;
    template <> class objptr<matrix::matrix>;
}

namespace svd {

    device::objptr<matrix::matrix> rsvd(const size_t, const size_t, const std::vector<size_t> &, const device::objptr<matrix::matrix> &);

}

#endif
