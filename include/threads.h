#ifndef BLOCK_WARP_H
#define BLOCK_WARP_H

#include <cuda_runtime_api.h>
#include <cuda.h>
#include <utils.h>

namespace Gist {
namespace Block {

    __device__ void segred(const guint num_elements_per_block, 
            volatile guint *__restrict__ flags, 
            volatile gfloat *__restrict__ vals);

    __device__ void scan(guint *__restrict__);
    __device__ void segscan(gfloat *__restrict__, bool *__restrict__);
}

namespace Warp {

    __device__ void segscan(const guint num_elements_per_block, 
            const guint num_elements_per_thread, 
            volatile guint *__restrict__ flags, 
            volatile gfloat *__restrict__ vals, 
            bool debug_print = false);

    __device__ void scan(guint &val, guint &total, const guint num_elements);
    __device__ void scan(guint &val);
    __device__ void scan(guint *vals);
    __device__ void segscan(gfloat &val, bool &flag, const guint num_elements);
    __device__ void segscan(gfloat &val, bool &flag);
    __device__ void segscan(gfloat *vals, bool *flags);
    __device__ bool isopen(const bool flag, const guint num_elements);
    __device__ bool ORflags(const bool flag, const guint num_elements);
    __device__ void broadcast(gfloat &val, const guint num_elements);
}

namespace Thread {

    __device__ void set(guint &, guint, bool);
    __device__ bool test(guint &, guint);
    __device__ void segscan(gfloat *vals, bool *flags);
    __device__ void scan(guint *vals);
}
}
/*
namespace tree {
    struct dev_level;
}

namespace cub_wrapper {

    device::array<size_t> create_offsets(const tree::dev_level &, const size_t);

    void segred(device::objptr<matrix::matrix> &, device::objptr<matrix::matrix> &, 
            tree::dev_level &, cudaStream_t stream = 0);
    
    void segred(device::objptr<matrix::matrix> &, device::objptr<matrix::matrix> &, 
            device::array<size_t> &, cudaStream_t stream = 0);

    void segmax(device::array<float> &dev_in, 
            device::array<float> &dev_out, 
            device::array<size_t> &dev_offsets);

}
*/

#endif
