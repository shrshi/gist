#ifndef TREE_H
#define TREE_H

#include <type.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cstddef>
#include <vector>
#include <iostream>

namespace Gist {
namespace Matrix {
    class matrix;
}

namespace Device {

    template <class T> class array;
    template <class T> class objptr;
    template <> class objptr<Matrix::matrix>;
}

namespace Utils {
    struct mttkrp_malloc;
    class bitops;
}

namespace Coo {
    class coo;
}

namespace Tree {

    struct dev_level;

    class tree {
        guint depth;
        guint *level_mode_map = nullptr;
        guint *level_size = nullptr;
        guint **level_ptr = nullptr;
        Utils::bitops *level_flags = nullptr;
        guint **level_idx = nullptr;
        gfloat *values = nullptr;

        //both rank and cub_offsets will be recomputed for different rank
        guint rank = 0;
        guint **cub_offsets = nullptr;

        void swap(tree &other);
        void insert_nodes(guint, std::vector<guint>&, guint&, const Coo::coo &);
        public :
            tree() : depth{0} {}
            explicit tree(guint d) : depth{d} {}
            tree(const Coo::coo &, const std::vector<guint> &);
            tree(std::ifstream &, guint);
            ~tree();
            tree(const tree&) = delete;
            tree& operator=(const tree&) = delete;
            tree(tree&& rhs) : tree() {
                rhs.swap(*this);
            }
            tree& operator=(tree&& rhs) {
                rhs.swap(*this);
                return *this;
            }
            void print(std::ostream &f) const;
            void write(std::ofstream &f);
            void stringify(std::stringstream &f) const;
            std::vector<gfloat> mttkrp3_cpu(std::vector<Matrix::matrix>&);
            Device::array<gfloat> mttkrp3(std::vector<Matrix::matrix>&);
            Device::array<gfloat> mttkrp4(std::vector<Matrix::matrix>&);
            Device::objptr<Matrix::matrix> ttmc(std::vector<Matrix::matrix>); 
            Device::objptr<Matrix::matrix> mttkrp(std::vector<Matrix::matrix>&, Utils::mttkrp_malloc&, cudaStream_t &stream);
                    
            std::vector<guint> root_idx() const;
            std::vector<guint> level_map() const;
            guint max_lsize() const;
            gfloat norm() const;

            void create_cub_offsets(guint);

            friend struct dev_level;
            friend bool operator==(const tree &lhs, const tree &rhs);
    };

    struct dev_level {
        guint lsize = 0;
        guint *lidx = nullptr;
        dev_level(const tree&, guint);
        dev_level(guint, guint*);
        ~dev_level();
    };
}
}

#endif
