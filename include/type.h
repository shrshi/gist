#ifndef TYPE_H
#define TYPE_H

#include <cstdint>
#include <limits>
#include <type_traits>
#include <cuda_runtime.h>
#include <constants.h>

namespace Gist {

    constexpr int guint_typewidth = 32;
    using guint = uint32_t;
    constexpr uint32_t guint_max = UINT32_MAX;

    constexpr int gfloat_typewidth = 32;
    using gfloat = float;
    constexpr float gfloat_min = std::numeric_limits<float>::min();
    constexpr float gfloat_max = std::numeric_limits<float>::max();

    typedef std::conditional<num_elements_per_thread == 4, float4, gfloat>::type vgfloat;
    typedef std::conditional<num_elements_per_thread == 4, uint4, guint>::type vguint;
}

#endif
