#ifndef UTILS_H
#define UTILS_H

#include <type.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cusolverDn.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <random>
#include <cmath>

namespace Gist {
namespace Matrix {
    class matrix;
}

namespace Csf {
    class csf;
}

namespace Tree {
    struct dev_level;
}

namespace Device {
    template <class T> class objptr;
    template <class T> class array;
    template <> class objptr<Matrix::matrix>;
    template <> class objptr<Tree::dev_level>;
}

namespace Utils {
    class bitops;
}

namespace Kernels {
    __device__ void retrieve(Utils::bitops const *__restrict__, guint, guint, guint &, guint&);
    __global__ void mttkrp3(
            Matrix::matrix const *__restrict__, 
            Tree::dev_level const *__restrict__, 
            gfloat const *__restrict__, 
            Utils::bitops const *__restrict__, 
            uint64_t const *__restrict__,
            Matrix::matrix const *__restrict__, 
            Tree::dev_level const *__restrict__, 
            Utils::bitops const *__restrict__, 
            uint64_t const *__restrict__,
            gfloat *__restrict__);
}

namespace Utils {

    void permute(std::vector<Matrix::matrix> &mats, const std::vector<guint> order, bool rev = false);

    /*
    struct mttkrp_malloc {
        Device::array<guint> dev_cub_offsets;
        Device::objptr<Tree::dev_level> dev_idx;
        Device::objptr<Matrix::matrix> dev_matrix;
        Device::array<gfloat> dev_values;
        Device::objptr<Matrix::matrix> dev_out;

        mttkrp_malloc(Csf::csf&, const std::vector<Matrix::matrix>&, guint);
    };
    */


    /*
    struct cpd_malloc {

        std::vector<device::objptr<matrix::matrix>> grams;
        device::objptr<matrix::matrix> dev_M;
        device::objptr<matrix::matrix> dev_mttkrp;
        device::objptr<matrix::matrix> dev_mttkrp_clone;
        device::objptr<matrix::matrix> dev_A;
        device::array<gfloat> dev_lambda;
        matrix::matrix **mtxs = nullptr;
        cusolverDnHandle_t solver_handle;

        cpd_malloc(csf::csf&, const std::vector<matrix::matrix>&);
        ~cpd_malloc();
    };
    */

    //returns num_rows for matrix in array with max num_rows
    guint max_element(const std::vector<Matrix::matrix> &arr, const guint except);
    guint max_element(std::vector<guint> &&arr); 

    class bitops {
        guint nbits = 0;
        guint size = 0;
        guint *buf = nullptr;
        public:
            /* width is power of 2! */
            static constexpr guint width = guint_typewidth;
            bitops();
            bitops(guint nb);
            ~bitops();
            void resize(guint);
            __host__ __device__ void set(guint);
            __host__ __device__ void unset(guint);
            __host__ __device__ void toggle(guint);
            __host__ __device__ bool test(guint) const;
            __host__ __device__ void retrieve(guint, guint, guint &) const;
            void multiretrieve(guint, guint, guint &, guint &) const;
            void print(std::ostream &) const;
            std::vector<guint> toarray() const;
            void printf() const;
            __host__ __device__ guint num_bits() const { return nbits; }
            __host__ __device__ guint bufsize() const { return size; }
            void copy(const bitops &in, guint bit_pos);
            __device__ void subseq(guint, guint, guint*, bitops*) const;
            __device__ void subseq(guint start, guint end, volatile guint *) const;

            friend class Device::objptr<bitops>;
            friend __device__ void Kernels::retrieve(
                    bitops const *__restrict__, 
                    guint, guint, guint &, guint&);
            friend __global__ void Kernels::mttkrp3(
                    Matrix::matrix const *__restrict__, 
                    Tree::dev_level const *__restrict__, 
                    gfloat const *__restrict__, 
                    Utils::bitops const *__restrict__, 
                    uint64_t const *__restrict__,
                    Matrix::matrix const *__restrict__, 
                    Tree::dev_level const *__restrict__, 
                    Utils::bitops const *__restrict__, 
                    uint64_t const *__restrict__,
                    gfloat *__restrict__);
    };
}
}
#endif
