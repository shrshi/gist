#include <csf.h>
#include <utils.h>
#include <device.h>

namespace csf {
    /*
    device::objptr<matrix::matrix> csf::mttkrp(size_t mode, 
            std::vector<matrix::matrix>& matrices, 
            utils::mttkrp_malloc &auxmem) {

        cudaStream_t stream;
        cuErrChk(cudaStreamCreate(&stream));

        return tensors[mode].mttkrp(matrices, auxmem, stream);
        cuErrChk(cudaStreamDestroy(stream));
    }
    */

    /*
    float csf::cpd_iteration(std::vector<matrix::matrix> &matrices, 
            utils::cpd_malloc &auxmem, std::string nrmtype) {
        
        auto &dev_M = auxmem.dev_M;
        auto &dev_mttkrp = auxmem.dev_mttkrp;
        auto &dev_A = auxmem.dev_A;
        auto &dev_mttkrp_clone = auxmem.dev_mttkrp_clone;
        auto &dev_lambda = auxmem.dev_lambda;
        auto &grams = auxmem.grams;
        auto &mtxs = auxmem.mtxs;
        auto &handle = auxmem.solver_handle;

        cudaStream_t hadamard_stream, mttkrp_stream;
        cuErrChk(cudaStreamCreate(&hadamard_stream));
        cuErrChk(cudaStreamCreate(&mttkrp_stream));
        for(size_t m = 0; m < nmodes; m++){
            dev_M.constant(1.0f, hadamard_stream);
            dev_M.hadamard_product(grams, m, mtxs, hadamard_stream);

            cuErrChk(cudaStreamSynchronize(hadamard_stream));
            dev_M.retrieve().print(std::cout);

            int info = dev_M.chol(handle, hadamard_stream);

            dev_mttkrp = tensors[m].mttkrp(matrices, mttkrp_stream);
            dev_A.resize(matrices[m].num_rows(), matrices[m].num_cols());
            dev_A.constant(0.0f, hadamard_stream);
            cuErrChk(cudaDeviceSynchronize());

            if(m == nmodes - 1)
                dev_mttkrp_clone.clone(dev_mttkrp, mttkrp_stream);

            dev_lambda = dev_A.linsolve(
                    dev_M, info, dev_mttkrp, tensors[m].root_idx(), 
                    nrmtype);

            cuErrChk(cudaDeviceSynchronize());
            dev_A.retrieve().print(std::cout);

            dev_A.retrieve(matrices[m], mttkrp_stream);
            grams[m].mmul(dev_A, dev_A, true, false, 1.0f, 0.0f, hadamard_stream);
        }

        cuErrChk(cudaDeviceSynchronize());
        float ret = cpd_fit(auxmem, matrices[nmodes-1]);

        cuErrChk(cudaStreamDestroy(hadamard_stream));
        cuErrChk(cudaStreamDestroy(mttkrp_stream));

        return ret;
    }
    */

    /*
    float csf::cpd_fit(utils::cpd_malloc &auxmem, 
            const matrix::matrix &mat) {
        
        const auto &lambda = auxmem.dev_lambda;
        auto &mttkrp = auxmem.dev_mttkrp_clone;
        const auto &grams = auxmem.grams;
        auto &mtxs = auxmem.mtxs;

        size_t F = grams[0].num_rows();

        float X_norm = norm();
        X_norm = X_norm * X_norm;

        std::cout << "X_norm : " << X_norm << std::endl;

        float Y_norm;
        cublasHandle_t handle;
        cuErrChk(cublasCreate(&handle));
        device::objptr<matrix::matrix> dev_hada(F, F);
        dev_hada.hadamard_product(grams, -1, mtxs);
        float alpha = 1.0f, beta = 0.0f;
        device::array<float> y(F);
        cuErrChk(cublasSgemv(handle, CUBLAS_OP_N, 
                    dev_hada.num_rows(), dev_hada.num_cols(), 
                    &alpha, dev_hada.values(), dev_hada.num_rows(), 
                    lambda.object(), 1, &beta, y.object(), 1));
        cuErrChk(cublasSdot(handle, F, lambda.object(), 1, 
                    y.object(), 1, &Y_norm));

        cuErrChk(cudaDeviceSynchronize());
        std::cout << "Y_norm : " << Y_norm << std::endl;

        float XY_norm;
        mttkrp.hadamard_product(tensors[nmodes-1].root_idx(), mat);
        device::array<float> ones(mttkrp.num_rows());
        ones.constant(1.0f);
        device::array<float> xy(F);
        cuErrChk(cublasSgemv(handle, CUBLAS_OP_T, 
                    mttkrp.num_rows(), mttkrp.num_cols(), 
                    &alpha, mttkrp.values(), mttkrp.num_rows(), 
                    ones.object(), 1, 
                    &beta, xy.object(), 1));
        cuErrChk(cublasSdot(handle, F, lambda.object(), 1, 
                    xy.object(), 1, &XY_norm));

        cuErrChk(cudaDeviceSynchronize());
        std::cout << "XY_norm : " << XY_norm << std::endl;
        
        float residual = X_norm + Y_norm - (2 * XY_norm);
        std::cout << "residual : " << residual << std::endl;

        return 1 - (std::sqrt(residual) / std::sqrt(X_norm));
    }
    */
}
