#include <type.h>
#include <device.h>
#include <matrix.h>
#include <tree.h>
#include <utils.h>
#include <err.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cassert>
#include <iostream>

namespace Gist {
using namespace Matrix;
namespace Device {

    void objptr<matrix>::init(const matrix &o) {
        nrows = o.num_rows();
        ncols = o.num_cols();
        cuErrChk(cudaMemcpy(obj, &o, sizeof(matrix), cudaMemcpyHostToDevice));
    }

    void objptr<matrix>::swap(objptr &rhs) {
        std::swap(nrows, rhs.nrows);
        std::swap(ncols, rhs.ncols);
        std::swap(vals, rhs.vals);
    }

    objptr<matrix>::objptr(const matrix &m) : ptr<matrix>() {
        init(m);
        cuErrChk(cudaMalloc((void**)&vals, m.nrows * m.ncols * sizeof(gfloat)));
        cuErrChk(cudaMemcpy(vals, m.values, m.nrows * m.ncols * sizeof(gfloat), cudaMemcpyHostToDevice));
        cuErrChk(cudaMemcpy(&(obj->values), &vals, sizeof(gfloat*), cudaMemcpyHostToDevice));
    }

    objptr<matrix>::objptr(const guint nr, const guint nc) : ptr<matrix>() {
        matrix m;
        m.nrows = nr; m.ncols = nc; m.values = nullptr;
        init(m);
        cuErrChk(cudaMalloc((void**)&vals, m.nrows * m.ncols * sizeof(gfloat)));
        cuErrChk(cudaMemcpy(&(obj->values), &vals, sizeof(gfloat*), cudaMemcpyHostToDevice));
    }

    objptr<matrix>::~objptr() {
        if(obj) {
            cuErrChk(cudaFree(vals));
            vals = nullptr; nrows = 0; ncols = 0;
        }
    }

    objptr<matrix>::objptr(objptr&& rhs) : ptr<matrix>(std::move(rhs)) {
        rhs.swap(*this);
    }

    objptr<matrix>& objptr<matrix>::operator=(objptr &&rhs) {
        ptr<matrix>::operator=(std::move(rhs));
        rhs.swap(*this);
        return *this;
    }

    void objptr<matrix>::resize(guint nr, guint nc, cudaStream_t stream) {
        matrix m;
        m.nrows = nr; m.ncols = nc; m.values = nullptr;
        init(m);
        cuErrChk(cudaMemcpyAsync(&(obj->values), &vals, sizeof(gfloat*), cudaMemcpyHostToDevice, stream));
    }

    void objptr<matrix>::assign(const matrix &m, cudaStream_t stream) {
        resize(m.nrows, m.ncols, stream);
        cuErrChk(cudaMemcpyAsync(vals, m.values, m.nrows * m.ncols * sizeof(gfloat), cudaMemcpyHostToDevice, stream));
        cuErrChk(cudaMemcpyAsync(&(obj->values), &vals, sizeof(gfloat*), cudaMemcpyHostToDevice, stream));
    }

    matrix objptr<matrix>::retrieve() {
        matrix m;
        cuErrChk(cudaMemcpy(&m, obj, sizeof(matrix), cudaMemcpyDeviceToHost));
        m.values = new gfloat[m.nrows * m.ncols];
        cuErrChk(cudaMemcpy(m.values, vals, m.nrows * m.ncols * sizeof(gfloat), cudaMemcpyDeviceToHost));
        return m;
    }

    void objptr<matrix>::retrieve(matrix &m, cudaStream_t s) {
        matrix _m;
        cuErrChk(cudaMemcpyAsync(&_m, obj, sizeof(matrix), cudaMemcpyDeviceToHost, s));
        m.nrows = _m.nrows; m.ncols = _m.ncols;
        _m.values = nullptr;
        assert(m.num_rows() == nrows && m.num_cols() == ncols);
        cuErrChk(cudaMemcpyAsync(m.values, vals, m.nrows * m.ncols * sizeof(gfloat), cudaMemcpyDeviceToHost, s));

        /*
        cuErrChk(cudaMemcpy(&_m, obj, sizeof(matrix), cudaMemcpyDeviceToHost));
        m.nrows = _m.nrows; m.ncols = _m.ncols;
        _m.values = nullptr;
        assert(m.num_rows() == nrows && m.num_cols() == ncols);
        cuErrChk(cudaMemcpy(m.values, vals, m.nrows * m.ncols * sizeof(gfloat), cudaMemcpyDeviceToHost));
        m.print(std::cout);
        */
    }

    objptr<Tree::dev_level>::objptr(const Tree::dev_level &t) : ptr<Tree::dev_level>() {
        cuErrChk(cudaMemcpy(obj, &t, sizeof(Tree::dev_level), cudaMemcpyHostToDevice));
        if(t.lidx) {
            lsize = t.lsize;
            cuErrChk(cudaMalloc((void**)&lidx, t.lsize * sizeof(guint)));
            cuErrChk(cudaMemcpy(lidx, t.lidx, t.lsize * sizeof(guint), cudaMemcpyHostToDevice));
            cuErrChk(cudaMemcpy(&(obj->lidx), &lidx, sizeof(guint*), cudaMemcpyHostToDevice));
        }
    }

    objptr<Tree::dev_level>::~objptr() {
        if(obj) {
            if(lidx) cuErrChk(cudaFree(lidx));
            lidx = nullptr;
            lsize = 0;
        }
    }

    objptr<Tree::dev_level>::objptr(const guint size) : ptr<Tree::dev_level>() {
        Tree::dev_level t(size, nullptr);
        cuErrChk(cudaMemcpy(obj, &t, sizeof(Tree::dev_level), cudaMemcpyHostToDevice));
        lsize = t.lsize;
        cuErrChk(cudaMalloc((void**)&lidx, t.lsize * sizeof(guint)));
        cuErrChk(cudaMemcpy(&(obj->lidx), &lidx, sizeof(guint*), cudaMemcpyHostToDevice));
    }

    void objptr<Tree::dev_level>::assign(const Tree::dev_level &t, cudaStream_t stream) {
        lsize = t.lsize;
        cuErrChk(cudaMemcpyAsync(obj, &t, sizeof(Tree::dev_level), cudaMemcpyHostToDevice, stream));
        cuErrChk(cudaMemcpyAsync(&(obj->lidx), &lidx, sizeof(Tree::dev_level), cudaMemcpyHostToDevice, stream));
        cuErrChk(cudaMemcpyAsync(lidx, t.lidx, t.lsize * sizeof(gfloat), cudaMemcpyHostToDevice, stream));
    }

    objptr<Utils::bitops>::objptr(const Utils::bitops &b) : ptr<Utils::bitops>() {
        cuErrChk(cudaMemcpy(obj, &b, sizeof(Utils::bitops), cudaMemcpyHostToDevice));
        if(b.size) {
            nbits = b.nbits;
            size = b.size;
            cuErrChk(cudaMalloc((void**)&buf, b.size * sizeof(guint)));
            cuErrChk(cudaMemcpy(buf, b.buf, b.size * sizeof(guint), cudaMemcpyHostToDevice));
            cuErrChk(cudaMemcpy(&(obj->buf), &buf, sizeof(guint*), cudaMemcpyHostToDevice));
        }
    }

    objptr<Utils::bitops>::~objptr() {
        if(obj) {
            if(buf) cuErrChk(cudaFree(buf));
            buf = nullptr;
            size = 0;
            nbits = 0;
        }
    }
}
}
