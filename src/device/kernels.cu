#include <type.h>
#include <constants.h>
#include <device.h>
#include <kernels.h>
#include <matrix.h>
#include <utils.h>
#include <tree.h>
#include <threads.h>
#include <cub/cub.cuh>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <cstdlib>

namespace Gist {
namespace Kernels {
    using Matrix::matrix;
    using Tree::dev_level;
    using Utils::bitops;

    __global__ void scan(const guint num_elements, 
            guint const *__restrict__ in, 
            guint *__restrict__ out) {
        
        extern __shared__ guint vals[];
        guint p = threadIdx.x + (blockIdx.x * blockDim.x), i;

        for(int i = 0; i < num_elements_per_thread; i++){
            if(p*num_elements_per_thread + i < num_elements)
                vals[threadIdx.x + (i * blockDim.x)] = in[p*num_elements_per_thread + i];
            else vals[threadIdx.x + (i * blockDim.x)] = 0;
        }
        __syncwarp();

        Block::scan(vals);
        __syncthreads();
        for(i = 0; i < num_elements_per_thread && p + i < num_elements; i++)
            out[p*num_elements_per_thread + i] = vals[threadIdx.x + (i * blockDim.x)];
    }

    __global__ void segscan(const guint num_elements, 
            gfloat const *__restrict__ in, 
            bitops const *__restrict__ bitflags, 
            gfloat *__restrict__ out) {
        
        extern __shared__ gfloat shared_mem[];
        gfloat *vals = (gfloat*) (shared_mem);
        bool *flags = (bool*) &vals[blockDim.x * num_elements_per_thread];
        guint p = threadIdx.x + (blockIdx.x * blockDim.x), i;

        for(int i = 0; i < num_elements_per_thread; i++){
            if(p*num_elements_per_thread + i < num_elements) {
                vals[threadIdx.x + (i * blockDim.x)] = in[p*num_elements_per_thread + i];
                flags[threadIdx.x + (i * blockDim.x)] = bitflags->test(p*num_elements_per_thread + i);
            }
            else{
                vals[threadIdx.x + (i * blockDim.x)] = 0;
                flags[threadIdx.x + (i * blockDim.x)] = false;
            }
        }
        __syncwarp();

        Block::segscan(vals, flags);
        __syncthreads();
        for(i = 0; i < num_elements_per_thread && p + i < num_elements; i++)
            out[p*num_elements_per_thread + i] = vals[threadIdx.x + (i * blockDim.x)];
    }

    /*
    __forceinline__ __device__ void set(guint &a, const guint &pos, const bool &val) {
        if(val) a |= 1 << (pos % guint_typewidth);
        else a &= ~(1 << (pos % guint_typewidth));
    }

    __forceinline__ __device__ bool test(guint &a, const guint &pos) {
        if(a & (1 << (pos % guint_typewidth)))
            return true;
        return false;
    }

    __global__ void mttkrp3(matrix const *__restrict__ l2_matrix, 
            dev_level const *__restrict__ l2pos, 
            gfloat const *__restrict__ scale, 
            bitops const *__restrict__ l2_flags, 
            guint const *__restrict__ l2_segments,
            matrix const *__restrict__ l1_matrix, 
            dev_level const *__restrict__ l1pos, 
            bitops const *__restrict__ l1_flags, 
            guint const *__restrict__ l1_segments, 
            gfloat *__restrict__ outval
            ) {

        guint global_tpos = threadIdx.x + (blockIdx.x * blockDim.x), p, i, start, row, col;
        const int lane = threadIdx.x & (warp_size - 1);
        extern __shared__ gfloat shared_mem[];
        gfloat *l1a_c = (gfloat*) (shared_mem);
        guint *l1flags = (guint*) &l1a_c[blockdim * num_elements_per_thread];
        gfloat *scratch_floats = (gfloat*) &l1flags[blockdim * num_elements_per_thread];
        guint *scratch_flags = (guint*) &scratch_floats[warp_size];

        start = (threadIdx.x >> warp_width) * num_elements_per_thread * warp_size;
        for(i = 0; i < num_elements_per_thread; i++) {
            l1a_c[start + (i * warp_size) + lane] = 0.0f;
            l1flags[start + (i * warp_size) + lane] = 0;
        }

        guint block_segments[num_elements_per_thread], block_offset = 0, block_nsegments;
        gfloat vals[num_elements_per_thread];
        bool flags[num_elements_per_thread];
        guint is_segment_end = 0, bitflags, nflags, nelements, nsegments; //store as bools as bits in word

        nflags = l2pos->lsize;
        for(int i = 0; i < num_elements_per_thread; i++) {
            p = global_tpos * num_elements_per_thread + i;
            row = l2pos->lidx[p % nflags] - 1;
            col = min(p / nflags, l2_matrix->ncols - 1);
            vals[i] = l2_matrix->values[col * l2_matrix->nrows + row] * 
                scale[p % nflags];
        }
        nelements = nflags * l2_matrix->num_cols();
        nsegments = l2_segments[nflags - 1];
        l2_flags->retrieve(global_tpos * num_elements_per_thread, num_elements_per_thread, bitflags);
        if(!threadIdx.x) set(bitflags, 0, false);
        l2_flags->retrieve(global_tpos * num_elements_per_thread + 1, num_elements_per_thread, is_segment_end);

        for(i = 0; i < num_elements_per_thread; i++) {
            p = (global_tpos * num_elements_per_thread) + i;
            if(p < nelements) {
                flags[i] = test(bitflags, i);
                if(p == (blockIdx.x + 1) * blockDim.x * num_elements_per_thread - 1)
                    set(is_segment_end, i, true);
            }
            else {
                flags[i] = false;
                if(p - i < nelements)
                    set(is_segment_end, i, false);
                else
                    is_segment_end = 0;
            }
        }

        p = blockDim.x * blockIdx.x * num_elements_per_thread;
        block_offset = l2_segments[p % nflags] + (p / nflags) * nsegments;
        for(i = 0; i < num_elements_per_thread; i++)
            block_segments[i] = flags[i] ? 1 : 0;
        typedef cub::BlockScan<guint, blockdim> BlockScan;
        __shared__ typename BlockScan::TempStorage temp_storage;
        BlockScan(temp_storage).InclusiveSum(block_segments, block_segments, block_nsegments);
        block_nsegments++;
        //Block::scan(block_segments, scratch_flags);

        //std::printf("l2a: thd %d vals %.2f %.2f %.2f %.2f flags %d %d %d %d is_segment_end %d %d %d %d\n", global_tpos, vals[0], vals[1], vals[2], vals[3], flags[0], flags[1], flags[2], flags[3], Thread::test(is_segment_end, 0), Thread::test(is_segment_end, 1), Thread::test(is_segment_end, 2), Thread::test(is_segment_end, 3));
        //std::printf("l2a: thd %d vals %.2f flags %d is_segment_end %d\n", global_tpos, vals[0], flags[0], test(is_segment_end, 0));
        Block::segscan(vals, flags, scratch_floats, scratch_flags);
        //std::printf("l2a_scan: thd %d vals %.2f flags %d is_segment_end %d block_segments %d block_nsegments %d block_offset %d\n", global_tpos, vals[0], flags[0], test(is_segment_end, 0), block_segments[0], block_nsegments, block_offset);
        __syncwarp();
        for(i = 0; i < num_elements_per_thread; i++) {
            p = global_tpos * num_elements_per_thread + i;
            col = min(p / nflags, l1_matrix->ncols - 1);
            row = l1pos->lidx[(block_segments[i] + block_offset - 1) % nsegments] - 1;
            vals[i] *= l1_matrix->values[col * l1_matrix->nrows + row];
        }
        for(i = 0; i < num_elements_per_thread; i++) {
            p = (global_tpos * num_elements_per_thread) + i;
            if(test(is_segment_end, i)) {
                l1a_c[block_segments[i]] = vals[i];

                l1a_c[start + (i * warp_size) + lane] = vals[i];
                l1flags[start + (i * warp_size) + lane] = l1_flags->test(block_offset + block_segments[i] - (p / nflags * nsegments) - 1) ? 1 : 0;
                if(block_segments[i] == 0 && l1flags[start + (i * warp_size) + lane])
                    l1flags[start + (i * warp_size) + lane] = 0;
                //std::printf("thd %d storing vals[%d]=%.2f in shmem at position %d\n", global_tpos, i, vals[i], block_segments[i]);
            }
        }
        __syncthreads();
        
        l1_flags->retrieve(block_offset + (threadIdx.x * num_elements_per_thread) - 1, num_elements_per_thread, bitflags);
        if(!threadIdx.x) set(bitflags, 0, false);
        l1_flags->retrieve(block_offset + (threadIdx.x * num_elements_per_thread), num_elements_per_thread, is_segment_end);
        for(i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x * num_elements_per_thread + i;
            if(p < block_nsegments) {
                vals[i] = l1a_c[p];
                flags[i] = test(bitflags, i);
                if(p == block_nsegments - 1)
                    set(is_segment_end, i, true);
            }
            else {
                vals[i] = 0.0f;
                flags[i] = false;
                if(p - i < block_nsegments)
                    set(is_segment_end, i, false);
                else
                    is_segment_end = 0;
            }
            vals[i] = l1a_c[start + (i * warp_size) + lane];
            flags[i] = l1flags[start + (i * warp_size) + lane];
            assert(flags[i] == test(bitflags, i));
        }
        __syncwarp();
        for(i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x * num_elements_per_thread + i;
            if(i < num_elements_per_thread - 1){
                set(is_segment_end, i, flags[i+1]);
            }
            else if(lane < warp_size - 1) {
                set(is_segment_end, i, l1flags[start + lane + 1]);
            }
            else if(p < blockDim.x * num_elements_per_thread) {
                set(is_segment_end, i, l1flags[start + (warp_size * num_elements_per_thread)]);
            }
            else
                set(is_segment_end, i, 0);
        }
        //std::printf("l1a: thd %d vals %.2f %.2f %.2f %.2f flags %d %d %d %d is_segment_end %d %d %d %d\n", global_tpos, vals[0], vals[1], vals[2], vals[3], flags[0], flags[1], flags[2], flags[3], Thread::test(is_segment_end, 0), Thread::test(is_segment_end, 1), Thread::test(is_segment_end, 2), Thread::test(is_segment_end, 3));
        //std::printf("l1a: thd %d vals %.2f flags %d is_segment_end %d\n", global_tpos, vals[0], flags[0], test(is_segment_end, 0));

        nflags = l1_flags->num_bits();
        nsegments = l1_segments[nflags - 1];
        nelements = nflags * l2_matrix->num_cols();
        start = block_offset - 1; //starting element in l1
        block_offset = l1_segments[(block_offset - 1) % nflags] + ((block_offset - 1) / nflags) * nsegments;
        for(i = 0; i < num_elements_per_thread; i++)
            block_segments[i] = flags[i] ? 1 : 0;
        //BlockScan(temp_storage).InclusiveSum(block_segments, block_segments);
        Block::scan(block_segments, scratch_flags);
        //std::printf("l1 : thd %d block_segments %d offset %d\n", global_tpos, block_segments[0], block_offset);
        //std::printf("l1 : thd %d block_segments %d %d %d %d offset %d\n", global_tpos, block_segments[0], block_segments[1], block_segments[2], block_segments[3], block_offset);

        Block::segscan(vals, flags, scratch_floats, scratch_flags);
        //std::printf("l1a_scan: thd %d vals %.2f flags %d is_segment_end %d block_segments %d block_offset %d\n", global_tpos, vals[0], flags[0], test(is_segment_end, 0), block_segments[0], block_offset);

        __syncwarp();
        for(i = 0; i < num_elements_per_thread; i++) {
            if(test(is_segment_end, i)) {
                p = block_offset + block_segments[i] - 1;
                //if(flags[i] && threadIdx.x < blockDim.x - 1) {
                if(flags[i] && (threadIdx.x * num_elements_per_thread + i) < block_nsegments - 1) {
                    outval[p] = vals[i];
                    //std::printf("thd %d storing %.2f at %d\n", global_tpos, vals[i], p);
                }
                //else if(!flags[i] || threadIdx.x == blockDim.x - 1){
                else if(!flags[i] || (threadIdx.x * num_elements_per_thread + i) == block_nsegments - 1){
                    atomicAdd(&(outval[p]), vals[i]);
                    //std::printf("thd %d adding %.2f to pos %d\n", global_tpos, vals[i], p);
                }
            }
        }
    }
    */

        /*
    __global__ void mttkrp4(
            Matrix::matrix const *__restrict__ l3_matrix, 
            Tree::dev_level const *__restrict__ l3pos, 
            gfloat const *__restrict__ scale, 
            Utils::bitops const *__restrict__ l3_flags, 
            guint const *__restrict__ l3_segments,
            Matrix::matrix const *__restrict__ l2_matrix, 
            Tree::dev_level const *__restrict__ l2pos, 
            Utils::bitops const *__restrict__ l2_flags, 
            guint const *__restrict__ l2_segments,
            Matrix::matrix const *__restrict__ l1_matrix, 
            Tree::dev_level const *__restrict__ l1pos, 
            Utils::bitops const *__restrict__ l1_flags, 
            guint const *__restrict__ l1_segments,
            gfloat *__restrict__ outval
            ) {

        guint global_tpos = threadIdx.x + (blockIdx.x * blockDim.x), p, i, q, start;
        extern __shared__ gfloat shared_mem[];
        gfloat *lXa_c = (gfloat*) (shared_mem);
        guint *lXflags = (guint*) &lXa_c[blockdim * num_elements_per_thread];
        gfloat *scratch_floats = (gfloat*) &lXflags[blockdim * num_elements_per_thread];
        guint *scratch_flags = (guint*) &scratch_floats[warp_size];
        guint *is_block_open = (guint*) &scratch_flags[warp_size];
        if(threadIdx.x == 0)
            *is_block_open = 0;

        guint block_nsegments, block_segments[num_elements_per_thread], block_offset = 0;
        guint nflags = l3_flags->num_bits(); //nflags = l3pos->lsize
        guint nelements = nflags * l3_matrix->num_cols();
        guint nsegments = l3_segments[nflags - 1];

        p = blockDim.x * blockIdx.x * num_elements_per_thread;
        block_offset = l3_segments[p % nflags] + (p / nflags) * nsegments;

        for(i = 0; i < num_elements_per_thread; i++) {
            p = global_tpos * num_elements_per_thread + i;
            block_segments[i] = l3_segments[p % nflags] + (p / nflags * nsegments) - block_offset;
        }
        p = min(nelements - 1, (blockDim.x * (blockIdx.x + 1)) * num_elements_per_thread - 1);
        block_nsegments = l3_segments[p % nflags]  + (p / nflags * nsegments) - block_offset + 1;
        //std::printf("l3 : thd %d nsegments %d block_segments %d offset %d\n", global_tpos, block_nsegments, block_segments[0], block_offset); 
        gfloat vals[num_elements_per_thread];
        bool flags[num_elements_per_thread];
        guint is_segment_end = 0; //store as bools as bits in word

        l3_matrix->select_scale(l3pos, scale, vals);
        for(i = 0; i < num_elements_per_thread; i++) {
            p = (global_tpos * num_elements_per_thread) + i;
            flags[i] = l3_flags->test(p % nflags);
            if(p < nelements - 1) {
                if(p == (blockIdx.x + 1) * blockDim.x * num_elements_per_thread - 1)
                    Thread::set(is_segment_end, i, true);
                else
                    Thread::set(is_segment_end, i, l3_flags->test((p+1) % nflags));
            }
            else if(p == nelements - 1)
                Thread::set(is_segment_end, i, true);
            else
                Thread::set(is_segment_end, i, false);
        }
        __syncwarp();

        //std::printf("l3a: thd %d vals %.3f %.3f %.3f %.3f flags %d %d %d %d is_segment_end %d %d %d %d\n", global_tpos, vals[0], vals[1], vals[2], vals[3], flags[0], flags[1], flags[2], flags[3], Thread::test(is_segment_end, 0), Thread::test(is_segment_end, 1), Thread::test(is_segment_end, 2), Thread::test(is_segment_end, 3));
        Block::segscan(vals, flags, scratch_floats, scratch_flags);
        //std::printf("l3a_scan: thd %d vals %.3f %.3f %.3f %.3f flags %d %d %d %d is_segment_end %d %d %d %d\n", global_tpos, vals[0], vals[1], vals[2], vals[3], flags[0], flags[1], flags[2], flags[3], Thread::test(is_segment_end, 0), Thread::test(is_segment_end, 1), Thread::test(is_segment_end, 2), Thread::test(is_segment_end, 3));
        //std::printf("l3a_scan: thd %d vals %.2f flags %d is_segment_end %d\n", global_tpos, vals[0], flags[0], Thread::test(is_segment_end, 0));
        l2_matrix->hadamard(nflags, l2pos, block_segments, block_offset, nsegments, vals, (blockIdx.x * blockDim.x * num_elements_per_thread) + 1);
        for(i = 0; i < num_elements_per_thread; i++) {
            p = (global_tpos * num_elements_per_thread) + i;
            if(Thread::test(is_segment_end, i)) {
                lXa_c[block_segments[i]] = vals[i];
                lXflags[block_segments[i]] = l2_flags->test(block_offset + block_segments[i] - (p / nflags * nsegments) - 1) ? 1 : 0;
                if(flags[i] == 0) *is_block_open = 1; //shmem
            }
        }
        __syncthreads();

        //number of l2 elements in current block = block_nsegments
        is_segment_end = 0;
        for(i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x * num_elements_per_thread + i;
            flags[i] = p < block_nsegments ? lXflags[p] : 1;
            if(!p) {
                if(!flags[i] && !(*is_block_open)) *is_block_open = 1;
                if(flags[i] && *is_block_open) flags[i] = 0;
            }
            vals[i] = p < block_nsegments ? lXa_c[p] : 0;
            if(p < block_nsegments - 1)
                Thread::set(is_segment_end, i, lXflags[p+1]);
            else if(p == block_nsegments - 1)
                Thread::set(is_segment_end, i, true);
            else
                Thread::set(is_segment_end, i, false);
        }
        __syncwarp();
        //std::printf("l2a: thd %d vals %.2f flags %d\n", global_tpos, vals[0], flags[0]);
        //std::printf("l2a: thd %d vals %.3f %.3f %.3f %.3f flags %d %d %d %d is_segment_end %d %d %d %d\n", global_tpos, vals[0], vals[1], vals[2], vals[3], flags[0], flags[1], flags[2], flags[3], Thread::test(is_segment_end, 0), Thread::test(is_segment_end, 1), Thread::test(is_segment_end, 2), Thread::test(is_segment_end, 3));
        Block::segscan(vals, flags, scratch_floats, scratch_flags);
        //std::printf("l2a_scan: thd %d vals %.2f flags %d\n", global_tpos, vals[0], flags[0]);
        nflags = l2_flags->num_bits();
        nsegments = l2_segments[nflags - 1];
        //number of l2a_c elements excluding current block = block_offset - 1
        //number of l2a_c elements including current block = block_offset - 1 + block_nsegments
        start = block_offset - 1; //starting element in l2
        block_offset = l2_segments[(block_offset - 1) % nflags] + ((block_offset - 1) / nflags) * nsegments;
        for(i = 0; i < num_elements_per_thread; i++) {
            p = start + (threadIdx.x * num_elements_per_thread) + i;
            block_segments[i] = l2_segments[p % nflags] + (p / nflags) * nsegments - block_offset;
        }
        p = start + block_nsegments - 1;
        block_nsegments = l2_segments[p % nflags]  + (p / nflags) * nsegments - block_offset + 1;
        //std::printf("l2 : thd %d block_segments %d offset %d p %d block_nsegments %d\n", global_tpos, block_segments[0], block_offset, p, block_nsegments);
        //std::printf("l2 : thd %d block_segments %d %d %d %d offset %d block_nsegments %d\n", global_tpos, block_segments[0], block_segments[1], block_segments[2], block_segments[3], block_offset, block_nsegments);
        l1_matrix->hadamard(nflags, l1pos, block_segments, block_offset, nsegments, vals, start + 1);
        for(i = 0; i < num_elements_per_thread; i++) {
            p = start + (threadIdx.x * num_elements_per_thread) + i;
            if(Thread::test(is_segment_end, i)) {
                lXa_c[block_segments[i]] = vals[i];
                lXflags[block_segments[i]] = l1_flags->test(block_offset + block_segments[i] - (p / nflags * nsegments) - 1) ? 1 : 0;
            }
        }
        __syncthreads();

        //number of l1 elements in current block = block_nsegments
        is_segment_end = 0;
        for(i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x * num_elements_per_thread + i;
            flags[i] = p < block_nsegments ? lXflags[p] : 1;
            if(!p && *is_block_open) flags[i] = 0;
            vals[i] = p < block_nsegments ? lXa_c[p] : 0;
            if(p < block_nsegments - 1)
                Thread::set(is_segment_end, i, lXflags[p+1]);
            else if(p == block_nsegments - 1)
                Thread::set(is_segment_end, i, true);
            else
                Thread::set(is_segment_end, i, false);
        }
        __syncwarp();
        //std::printf("l1a: thd %d vals %.3f flags %d is_segment_end %d\n", global_tpos, vals[0], flags[0], Thread::test(is_segment_end, 0));
        Block::segscan(vals, flags, scratch_floats, scratch_flags);
        //std::printf("l1a_scan: thd %d vals %.3f flags %d is_segment_end %d\n", global_tpos, vals[0], flags[0], Thread::test(is_segment_end, 0));
        nflags = l1_flags->num_bits();
        nsegments = l1_segments[nflags - 1];
        //number of l1a_c elements excluding current block = block_offset - 1
        //number of l1a_c elements including current block = block_offset - 1 + block_nsegments
        start = block_offset - 1; //starting element in l1
        block_offset = l1_segments[(block_offset - 1) % nflags] + ((block_offset - 1) / nflags) * nsegments;
        for(i = 0; i < num_elements_per_thread; i++) {
            p = start + (threadIdx.x * num_elements_per_thread) + i;
            block_segments[i] = l1_segments[p % nflags] + (p / nflags) * nsegments - block_offset;
        }
        p = start + block_nsegments - 1;
        block_nsegments = l1_segments[p % nflags]  + (p / nflags) * nsegments - block_offset + 1;
        
        for(i = 0; i < num_elements_per_thread; i++) {
            if(Thread::test(is_segment_end, i)) {
                q = block_offset + block_segments[i] - 1;
                if(flags[i] && q != block_offset + block_nsegments - 2) {
                    outval[q] = vals[i];
                }
                else if(!flags[i] || q == block_offset + block_nsegments - 2){
                    atomicAdd(&(outval[q]), vals[i]);
                }
            }
        }
    }
        */
}
}
