#include <device.h>
#include <matrix.h>
#include <kernels.h>
#include <tree.h>
#include <segred.h>
#include <err.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <curand.h>
#include <cublas_v2.h>
#include <cusolverDn.h>
#include <cassert>
#include <vector>
#include <algorithm>
#include <cmath>

namespace Gist {
using namespace Matrix;
namespace Device {

    void objptr<matrix>::rand_init() {
        curandGenerator_t gen;
        cuErrChk(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
        cuErrChk(curandSetPseudoRandomGeneratorSeed(gen, 1234ULL));
        cuErrChk(curandGenerateNormal(gen, vals, nrows * ncols, 0.0, 1.0));
        cuErrChk(cudaDeviceSynchronize());
        cuErrChk(curandDestroyGenerator(gen));
    }

    void objptr<matrix>::rand_init(const std::vector<guint> &colpos, cudaStream_t stream) {
        curandGenerator_t gen;
        cuErrChk(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
        cuErrChk(curandSetStream(gen, stream));
        cuErrChk(curandSetPseudoRandomGeneratorSeed(gen, 1234ULL));

        for(guint i = 0; i < colpos.size(); i++) {
            cuErrChk(curandGenerateUniform(gen, 
                        vals + (nrows * colpos[i]), 
                        nrows));
            if(stream == 0) cuErrChk(cudaDeviceSynchronize());
        }
        cuErrChk(curandDestroyGenerator(gen));
    }

    void objptr<matrix>::mmul(const objptr<matrix> &lhs, const objptr<matrix> &rhs, bool lhs_transpose, bool rhs_transpose, gfloat alpha, gfloat beta, cudaStream_t stream) {
        guint m = lhs.num_rows();
        guint k = lhs.num_cols();
        guint m_ = rhs.num_rows();
        guint n = rhs.num_cols();

        guint common_dim;
        if(lhs_transpose && !rhs_transpose) {
            assert(m == m_ && nrows == k && ncols == n);
            common_dim = m;
        }
        else if(!lhs_transpose && rhs_transpose) {
            assert(k == n && nrows == m && ncols == m_);
            common_dim = k;
        }
        else if(lhs_transpose && rhs_transpose) {
            assert(m == n && nrows == k && ncols == m_);
            common_dim = m;
        }
        else {
            assert(k == m_ && nrows == m && ncols == n);
            common_dim = k; 
        }

        cublasHandle_t handle;
        cuErrChk(cublasCreate(&handle));
        cuErrChk(cublasSetStream(handle, stream));

        cublasOperation_t lhs_op = lhs_transpose ? CUBLAS_OP_T : CUBLAS_OP_N;
        cublasOperation_t rhs_op = rhs_transpose ? CUBLAS_OP_T : CUBLAS_OP_N;

        cuErrChk(cublasSgemm(handle, lhs_op, rhs_op, nrows, ncols, common_dim,
                &alpha, lhs.values(), m, rhs.values(), m_, &beta, vals, nrows));
        if(stream == 0) cuErrChk(cudaDeviceSynchronize());
        cublasDestroy(handle);
    }

    void objptr<matrix>::densify(guint out_nrows, const std::vector<guint> &cur_rows) {
        assert(nrows == cur_rows.size());
        matrix m(out_nrows, ncols);
        gfloat *cur_values = new gfloat[nrows * ncols];
        cuErrChk(cudaMemcpy(cur_values, vals, nrows * ncols * sizeof(gfloat), cudaMemcpyDeviceToHost));
        guint row_pos;
        for(guint j = 0; j < ncols; j++){
            row_pos = 0;
            for(guint i = 0; i < out_nrows; i++)
                if(i == cur_rows[row_pos] - 1){
                    m.values[j * out_nrows + i] = cur_values[j * nrows + row_pos];
                    row_pos++;
                }
                else
                    m.values[j * out_nrows + i] = 0.0f;
        }
        cuErrChk(cudaFree(vals));
        init(m);
        assert(nrows == out_nrows);
        cuErrChk(cudaMalloc((void**)&vals, m.nrows * m.ncols * sizeof(gfloat)));
        cuErrChk(cudaMemcpy(vals, m.values, m.nrows * m.ncols * sizeof(gfloat), cudaMemcpyHostToDevice));
        cuErrChk(cudaMemcpy(&(obj->values), &vals, sizeof(gfloat*), cudaMemcpyHostToDevice));
        delete[] cur_values;
    }

    /*
       computes qr decomposition. The upper triangular matrix R is 
       overwriiten on the upper half of matrix. The matrix Q is not 
       formed explicitly, instead, a sequence of householder vectors 
       are stored in lower triangular part of matrix
       */
    void objptr<matrix>::qr() {
        cusolverDnHandle_t handle;
        cuErrChk(cusolverDnCreate(&handle));

        int lwork_geqrf = 0, *devInfo, info_gpu;
        gfloat *tau, *workspace;

        cuErrChk(cusolverDnSgeqrf_bufferSize(handle, nrows, ncols, vals,
                nrows, &lwork_geqrf));

        cuErrChk(cudaMalloc((void**)&tau, ncols * sizeof(gfloat)));
        cuErrChk(cudaMalloc((void**)&workspace, lwork_geqrf * sizeof(gfloat)));
        cuErrChk(cudaMalloc((void**)&devInfo, sizeof(int)));

        cuErrChk(cusolverDnSgeqrf(handle, nrows, ncols, vals, nrows, tau,
                workspace, lwork_geqrf, devInfo));
        cuErrChk(cudaMemcpy(&info_gpu, devInfo, 
                    sizeof(int), cudaMemcpyDeviceToHost));
        assert(info_gpu == 0);

        cuErrChk(cudaFree(devInfo));
        cuErrChk(cudaFree(tau));
        cuErrChk(cudaFree(workspace));
        cusolverDnDestroy(handle);
    }

    /*
       return Q matrix overwritten on input matrix
       */
    void objptr<matrix>::qr_Q() {
        qr();

        cusolverDnHandle_t handle;
        cuErrChk(cusolverDnCreate(&handle));

        int lwork_orgqr, *devInfo, info_gpu;
        gfloat *workspace, *tau;
        cuErrChk(cudaMalloc((void**)&tau, ncols * sizeof(gfloat)));

        cuErrChk(cusolverDnSorgqr_bufferSize(handle, nrows, ncols, ncols, vals,
                nrows, tau, &lwork_orgqr));

        cuErrChk(cudaMalloc((void**)&workspace, lwork_orgqr * sizeof(gfloat)));
        cuErrChk(cudaMalloc((void**)&devInfo, sizeof(int)));

        cuErrChk(cusolverDnSorgqr(handle, nrows, ncols, 
                    ncols, vals, nrows, tau, workspace, lwork_orgqr, devInfo));
        cuErrChk(cudaMemcpy(&info_gpu, devInfo, 
                    sizeof(int), cudaMemcpyDeviceToHost));
        assert(info_gpu == 0);
        // finally vals store Q!
        cuErrChk(cudaFree(devInfo));
        cuErrChk(cudaFree(tau));
        cuErrChk(cudaFree(workspace));
        cusolverDnDestroy(handle);
    }

    void objptr<matrix>::svd(objptr<matrix> &U, objptr<matrix> &VT, array<gfloat> &S) {
        cusolverDnHandle_t handle;
        cuErrChk(cusolverDnCreate(&handle));

        int lwork, *devInfo, hostInfo;
        gfloat *workspace, *rwork;
        signed char jobu = 'A', jobvt = 'A';

        cuErrChk(cusolverDnSgesvd_bufferSize(handle, nrows, ncols, &lwork));

        cuErrChk(cudaMalloc((void**)&workspace, lwork * sizeof(gfloat)));
        cuErrChk(cudaMalloc((void**)&devInfo, sizeof(int)));
        cuErrChk(cudaMalloc((void**)&rwork, nrows * sizeof(gfloat)));

        cuErrChk(cusolverDnSgesvd(handle, jobu, jobvt, nrows, ncols,
                    vals, nrows, S.object(), U.values(), nrows, VT.values(),
                    nrows, workspace, lwork, rwork, devInfo));
        
        cuErrChk(cudaMemcpy(&hostInfo, devInfo, sizeof(int), cudaMemcpyDeviceToHost));
        assert(hostInfo == 0);
        cuErrChk(cudaFree(devInfo));
        cuErrChk(cudaFree(workspace));
        cuErrChk(cudaFree(rwork));
        cusolverDnDestroy(handle);
    }

    void objptr<matrix>::left_singular_vectors() {
        cusolverDnHandle_t handle;
        cuErrChk(cusolverDnCreate(&handle));

        int lwork = 0, info_gpu;
        cuErrChk(cusolverDnSgesvd_bufferSize(handle, nrows, ncols, &lwork));
        gfloat *workspace;
        cuErrChk(cudaMalloc((void**)&workspace, lwork * sizeof(gfloat)));
        gfloat *S;
        cuErrChk(cudaMalloc((void**)&S, ncols * sizeof(gfloat)));
        int *devInfo;
        cuErrChk(cudaMalloc((void**)&devInfo, sizeof(int)));

        // left singular vectors are overwritten on vals
        cuErrChk(cusolverDnSgesvd(handle, 'O', 'N', nrows, ncols, vals, nrows, 
                S, nullptr, nrows, nullptr, ncols, workspace, lwork, nullptr, 
                devInfo));
        cuErrChk(cudaMemcpy(&info_gpu, devInfo, sizeof(int), cudaMemcpyDeviceToHost));
        assert(info_gpu == 0);

        cuErrChk(cudaFree(devInfo));
        cuErrChk(cudaFree(S));
        cuErrChk(cudaFree(workspace));
        cusolverDnDestroy(handle);
    }

    objptr<matrix> objptr<matrix>::submatrix(const std::vector<guint> &row_selector) {
        guint out_nrows = row_selector.size();
        matrix m(out_nrows, ncols);
        gfloat *cur_values = new gfloat[nrows * ncols];
        cuErrChk(cudaMemcpy(cur_values, vals, nrows * ncols * sizeof(gfloat), cudaMemcpyDeviceToHost));
        for(guint j = 0; j < ncols; j++)
            for(guint i = 0; i < out_nrows; i++)
                m.values[j * out_nrows + i] = 
                    cur_values[j * nrows + (row_selector[i] - 1)];

        objptr<matrix> ret(m);
        delete[] cur_values;

        return ret;
    }

    gfloat objptr<matrix>::norm() {
        cublasHandle_t handle;
        cuErrChk(cublasCreate(&handle));

        gfloat ret = 0.0;
        cuErrChk(cublasSnrm2(handle, nrows * ncols, vals, 1, &ret));
        cuErrChk(cudaDeviceSynchronize());
        cublasDestroy(handle);

        return ret;
    }

    int objptr<matrix>::chol(cusolverDnHandle_t solver_handle, cudaStream_t stream) {

        gfloat *diag_dups;
        int numblocks = (ncols / 1024) + (ncols % 1024 ? 1 : 0);
        cuErrChk(cudaMalloc((void**)&diag_dups, ncols * sizeof(gfloat)));
        // copy only diagonal elements
        kernels::diag <<< numblocks, 1024, 0, stream >>> (diag_dups, vals, ncols);

        bool del_handle = false;
        if(solver_handle == nullptr){
            cuErrChk(cusolverDnCreate(&solver_handle));
            del_handle = true;
        }
        cuErrChk(cusolverDnSetStream(solver_handle, stream));

        assert(nrows == ncols);
        int Lwork, *devInfo, hostInfo;
        gfloat *wrkspce = NULL;

        cuErrChk(cusolverDnSpotrf_bufferSize(solver_handle, 
                    CUBLAS_FILL_MODE_LOWER, nrows, vals, nrows, &Lwork));

        cuErrChk(cudaMalloc((void**)&wrkspce, Lwork * sizeof(gfloat)));
        cuErrChk(cudaMalloc((void**)&devInfo, sizeof(int)));

        cuErrChk(cusolverDnSpotrf(solver_handle, 
                    CUBLAS_FILL_MODE_LOWER, nrows, vals, nrows, 
                    wrkspce, Lwork, devInfo));

        cuErrChk(cudaMemcpy(&hostInfo, devInfo, 
                    sizeof(int), cudaMemcpyDeviceToHost));

        if(hostInfo != 0){
            //_TODO_: replace this with copy of upper triangular part to
            // lower triangular part - NOT REQUIRED
            /*
            std::cout << "cannot compute chol\n";
            cuErrChk(cudaStreamSynchronize(stream));
            retrieve().print(std::cout);

            guint nelems = ((ncols - 1) * ncols) / 2;
            numblocks = (nelems / 1024) + (nelems % 1024 ? 1 : 0);
            guint segs[ncols - 1]; segs[0] = ncols - 1;
            for(guint i = 1; i < ncols - 1; i++)
                segs[i] = segs[i-1] + (ncols - i - 1);
            kernels::mirror <<< numblocks, 1024, 0, stream >>> (object(), 
                    segs);
            */
            numblocks = (ncols / 1024) + (ncols % 1024 ? 1 : 0);
            kernels::diagonal <<< numblocks, 1024, 0, stream >>> (vals, 
                    diag_dups, ncols);
        }
        cuErrChk(cudaFree(wrkspce));
        cuErrChk(cudaFree(devInfo));
        if(del_handle) cusolverDnDestroy(solver_handle);
        cuErrChk(cudaFree(diag_dups));
        return hostInfo;
    }

    void objptr<matrix>::evd(array<gfloat> &lambda) {
        cusolverDnHandle_t handle;
        cuErrChk(cusolverDnCreate(&handle));
        
        const double tol = 1e-5;
        const int max_sweeps = 50;
        syevjInfo_t params;
        cuErrChk(cusolverDnCreateSyevjInfo(&params));
        cuErrChk(cusolverDnXsyevjSetTolerance(params, tol));
        cuErrChk(cusolverDnXsyevjSetMaxSweeps(params, max_sweeps));

        int lwork, *devInfo, hostInfo;
        gfloat *workspace;

        cuErrChk(cusolverDnSsyevj_bufferSize(handle, 
                    CUSOLVER_EIG_MODE_VECTOR, 
                    CUBLAS_FILL_MODE_UPPER, 
                    nrows, vals, nrows, 
                    lambda.object(), &lwork, params));

        cuErrChk(cudaMalloc((void**)&workspace, lwork * sizeof(gfloat)));
        cuErrChk(cudaMalloc((void**)&devInfo, sizeof(int)));

        cuErrChk(cusolverDnSsyevj(handle, 
                    CUSOLVER_EIG_MODE_VECTOR,
                    CUBLAS_FILL_MODE_UPPER,
                    nrows, vals, nrows, lambda.object(), 
                    workspace, lwork, devInfo, params));

        cuErrChk(cudaMemcpy(&hostInfo, devInfo, sizeof(int), cudaMemcpyDeviceToHost));
        assert(hostInfo == 0);
        cuErrChk(cudaFree(workspace));
        cuErrChk(cudaFree(devInfo));
        cusolverDnDestroy(handle);
        cusolverDnDestroySyevjInfo(params);
    }

    array<gfloat> objptr<matrix>::normalize(std::string nrmtype) {
        
        cublasHandle_t handle;
        cuErrChk(cublasCreate(&handle));

        array<gfloat> nrms(ncols);
        if(nrmtype == std::string("L2")) {

            array<gfloat> ones(nrows);
            objptr<matrix> sqmat(nrows, ncols);
            gfloat alpha = 1.0f, beta = 0.0f;

            int numblocks = (nrows * ncols / 1024) + 
                ((nrows * ncols) % 1024 ? 1 : 0);
            kernels::power <<< numblocks, 1024, 0 >>> (
                    vals, 
                    nrows * ncols, 
                    2.0f, sqmat.vals);
            /*
            cuErrChk(cudaPeekAtLastError());
            cuErrChk(cudaDeviceSynchronize());
            */
            ones.constant(1.0f);
            cuErrChk(cublasSgemv(handle, CUBLAS_OP_T, 
                        sqmat.nrows, sqmat.ncols, 
                        &alpha, sqmat.vals, sqmat.nrows, 
                        ones.object(), 1, 
                        &beta, nrms.object(), 1));
            kernels::power <<< 1, 1024, 0 >>> (
                    nrms.object(), 
                    nrms.length(), 
                    0.5f, nrms.object());
            /*
            cuErrChk(cudaPeekAtLastError());
            cuErrChk(cudaDeviceSynchronize());
            */
            kernels::scale <<< ncols, 512 >>> (
                    object(), 
                    nrms.object(), NULL, true);
            /*
            cuErrChk(cudaPeekAtLastError());
            cuErrChk(cudaDeviceSynchronize());
            */
        }
        else if(nrmtype == std::string("max")) {
            
            array<gfloat> absmat(nrows * ncols);
            int numblocks = (nrows * ncols / 1024) + 
                ((nrows * ncols) % 1024 ? 1 : 0);
            std::vector<guint> offsets(ncols + 1, 0);
            for(guint i = 1; i <= ncols; i++)
                offsets[i] = offsets[i-1] + nrows;
            array<guint> dev_offsets(offsets);

            kernels::abs <<< numblocks, 1024 >>> (
                    vals, nrows * ncols, 
                    absmat.object());

            cub_wrapper::segmax(absmat, nrms, dev_offsets);

            kernels::scale <<< ncols, 512, 0 >>> (
                    object(), 
                    nrms.object(), NULL, true);
            /*
            cuErrChk(cudaPeekAtLastError());
            cuErrChk(cudaDeviceSynchronize());
            */
        }
        else std::cout << "p norm not supported\n";

        cuErrChk(cudaDeviceSynchronize());
        cublasDestroy(handle);
        return nrms;
    }

    array<gfloat> objptr<matrix>::linsolve(objptr<matrix> &A, 
            int chol_info, 
            objptr<matrix> &B, const std::vector<guint> &inds, 
            std::string nrmtype) {

        assert(A.num_rows() == A.num_cols());
        //another check for symmetry of A

        cublasHandle_t blas_handle;
        cuErrChk(cublasCreate(&blas_handle));

        guint F = A.num_rows();
        array<gfloat> lambda(F);

        /*
        int info = A.chol();
        */
        if(chol_info == 0) {
            /*
            * A is full rank SPD and Cholesky factor L is stored in lower
            * triangular half of A
            */
            std::cout << "Linsolve with full rank matrix\n";
            gfloat alpha = 1.0f;
            cuErrChk(cublasStrsm(blas_handle, CUBLAS_SIDE_RIGHT, 
                        CUBLAS_FILL_MODE_LOWER, CUBLAS_OP_T, 
                        CUBLAS_DIAG_NON_UNIT, B.nrows, B.ncols, 
                        &alpha, A.vals, A.nrows, B.vals, B.nrows));

            cuErrChk(cublasStrsm(blas_handle, CUBLAS_SIDE_RIGHT, 
                        CUBLAS_FILL_MODE_LOWER, CUBLAS_OP_N, 
                        CUBLAS_DIAG_NON_UNIT, B.nrows, B.ncols, 
                        &alpha, A.vals, A.nrows, B.vals, B.nrows));

            lambda = B.normalize(nrmtype);

            array<guint> dev_inds(inds);
            kernels::insert <<< F, 512, 0 >>> (
                    object(),
                    B.object(), 
                    dev_inds.object());

            /*
            cuErrChk(cudaPeekAtLastError());
            cuErrChk(cudaDeviceSynchronize());
            */

            /*
               * Note that since A is full rank, AX = 0 implies X is zero
               * Thus, the remaining rows of object() corresponding to 
               * rows I \ inds in B (which are zero) will also be zero
               */
        }
        else {
            /*
            * A is not full rank so compute pseudo inverse using SVD
            */

            /*
            cudaStream_t stream1, stream2;
            cuErrChk(cudaStreamCreate(&stream1));
            cuErrChk(cudaStreamCreate(&stream2));
            */

            std::cout << "Linsolve with non-invertible matrix\n";
            objptr<matrix> out_(B.num_rows(), F);
            objptr<matrix> out(B.num_rows(), F);
            objptr<matrix> scaled_out(F, F);

            A.evd(lambda);
            kernels::scale <<< 1, 512, 0 >>> (
                    A.object(), 
                    lambda.object(), 
                    scaled_out.object(), 
                    false);
            /*
            cuErrChk(cudaPeekAtLastError());
            cuErrChk(cudaDeviceSynchronize());
            */

            out_.mmul(B, scaled_out, false, false);
            out.mmul(out_, A, false, true);

            lambda = out.normalize(nrmtype);

            array<guint> dev_inds(inds);
            kernels::insert <<< F, 512, 0 >>> (
                    object(),
                    out.object(), 
                    dev_inds.object());
            /*
            cuErrChk(cudaPeekAtLastError());
            cuErrChk(cudaDeviceSynchronize());
            */

            /*
               * Since A is not full rank, it has zero singular values. 
               * This implies that solutions to AX = 0 need not be zero - 
               * they can be any linear combination of right singular vectors
               * corresponding to zero singular values. So fill rows I \ inds
               * of B accordingly
               */

            /*
            std::vector<gfloat> eigvals = lambda.retrieve();
            std::vector<guint> zero_sig_right_singular_vecs;

            for(i = 0; i < F; i++)
                if(std::abs(eigvals[i]) <= tol)
                    zero_sig_right_singular_vecs.push_back(i);
            if(!zero_sig_right_singular_vecs.empty()) {

                objptr<matrix> H(nrows - B.num_rows(), F);
                objptr<matrix> other(nrows - B.num_rows(), F);
                std::vector<guint> other_inds(nrows - B.num_rows(), 0);

                H.constant(0.0f, stream2);
                H.rand_init(zero_sig_right_singular_vecs, stream2);
                //TODO: execute in stream2
                other.mmul(H, A, false, true);
                j = 0;
                for(i = 0; i < nrows; i++)
                    if(i+1 < inds[j])
                        other_inds[i-j] = i+1;
                    else j++;
                array<guint> dev_other_inds(other_inds);            
                kernels::insert <<< F, 512, 0, stream2 >>> (
                        object(),
                        other.object(), 
                        dev_other_inds.object());
                cuErrChk(cudaPeekAtLastError());
                cuErrChk(cudaDeviceSynchronize());
            }
            
            lambda = normalize(nrmtype);
            cuErrChk(cudaStreamDestroy(stream1));
            cuErrChk(cudaStreamDestroy(stream2));
            */
        }

        cuErrChk(cudaPeekAtLastError());
        cuErrChk(cudaDeviceSynchronize());

        cuErrChk(cublasDestroy(blas_handle));
        return lambda;
    }

    void objptr<matrix>::hadamard_product(
            const std::vector<objptr<matrix>> &mats, int except_pos, matrix **mtxs, cudaStream_t stream) {

        guint n = mats.size();
        matrix *m;
        /*
        matrix **mtxs, *m;
        cuErrChk(cudaMalloc((void***)&mtxs, n * sizeof(matrix*)));
        */
        if(except_pos >= 0) {
            guint flag = 0;
            for(guint i = 0; i < n; i++){
                assert(mats[i].nrows == nrows && mats[i].ncols == ncols);
                if(i != except_pos){
                    m = mats[i].object();
                    cuErrChk(cudaMemcpy(&(mtxs[i-flag]), &(m), 
                                    sizeof(matrix*), 
                                    cudaMemcpyHostToDevice));
                }
                else flag++;
            }
            n--;
        }
        else {
            for(guint i = 0; i < n; i++){
                assert(mats[i].nrows == nrows && mats[i].ncols == ncols);
                m = mats[i].object();
                cuErrChk(cudaMemcpy(&(mtxs[i]), &(m), 
                                sizeof(matrix*), 
                                cudaMemcpyHostToDevice));
            }
        }
        int blockdim = 1024;
        int numblocks = (nrows * ncols / blockdim) + ((nrows * ncols) % blockdim ? 1 : 0);
        kernels::gram_hadamard <<<numblocks, blockdim, 0, stream>>> (
                object(),
                mtxs, n);
        /*
        cuErrChk(cudaFree(mtxs));
        */
    }

    void objptr<matrix>::hadamard_product(std::vector<guint> pos, const matrix &m) {
        
        objptr<matrix> dev_m(m);
        tree::dev_level rowpos(pos.size(), pos.data());
        objptr<tree::dev_level> dev_rowpos(rowpos);

        int numblocks = m.num_cols();
        kernels::hadamard_product <<< numblocks, 1024 >>> (
                object(), 
                dev_rowpos.object(),
                object(),
                dev_m.object());
        cuErrChk(cudaDeviceSynchronize());
    }

    void objptr<matrix>::constant(gfloat s, cudaStream_t stream) {
        int blockdim = 1024;
        int numblocks = (nrows * ncols / blockdim) + 
            (nrows * ncols % blockdim ? 1 : 0);
        kernels::constant <<< numblocks, blockdim, 0, stream >>> (
                vals, nrows * ncols, s);
        /*
        gfloat *v;
        cuErrChk(cudaMallocHost((void**)&v, nrows * ncols));
        for(guint i = 0; i < nrows * ncols; i++)
            v[i] = s;
        cuErrChk(cudaMemcpyAsync(vals, v, nrows * ncols * sizeof(gfloat), 
                    cudaMemcpyHostToDevice, stream));
        cuErrChk(cudaStreamSynchronize(stream));
        cuErrChk(cudaFreeHost(v));
        */
    }

    void objptr<matrix>::multiply_rows(const array<gfloat> &s) {

        int numblocks = (nrows*ncols / 1024) + ((nrows*ncols) % 1024 ? 1 : 0);
        kernels::batched_multiply <<< numblocks, 1024 >>> (
                vals, 
                nrows * ncols, 
                s.object(), s.length(), nrows);
        cuErrChk(cudaDeviceSynchronize());
    }
}
}
