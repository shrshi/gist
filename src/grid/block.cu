#include <type.h>
#include <constants.h>
#include <threads.h>
#include <utils.h>
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <cassert>

namespace Gist {
namespace Block {


    __forceinline__ __device__ void warp_segscan(gfloat &val, bool &flag, const int &lane) {
        gfloat val_;
        bool flag_;
        for(int i = 1; i < warp_size; i *= 2) {
            val_ = __shfl_up_sync(0xffffffff, val, i);
            flag_ = __shfl_up_sync(0xffffffff, flag, i);
            val = flag || lane < i ? val : val + val_;
            flag = flag || flag_;
        }
    }

    __forceinline__ __device__ void warp_segscan(gfloat *vals, bool *flags, const int &lane) {
        gfloat threadtotal, val_;
        bool flag_, threadflag;

        for(int i = 1; i < num_elements_per_thread; i++) {
            vals[threadIdx.x + (i * blockDim.x)] += flags[threadIdx.x + (i * blockDim.x)] ? 0 : vals[threadIdx.x + ((i-1) * blockDim.x)];
            flags[threadIdx.x + (i * blockDim.x)] = flags[threadIdx.x + ((i-1) * blockDim.x)] || flags[threadIdx.x + (i * blockDim.x)];
        }
        threadtotal = vals[threadIdx.x + ((num_elements_per_thread - 1) * blockDim.x)];
        threadflag = flags[threadIdx.x] || flags[threadIdx.x + ((num_elements_per_thread - 1) * blockDim.x)];
        __syncwarp();

        for(int i = 1; i < warp_size; i *= 2) {
            val_ = __shfl_up_sync(0xffffffff, threadtotal, i);
            flag_ = __shfl_up_sync(0xffffffff, threadflag, i);
            threadtotal += threadflag || lane < i ? 0 : val_;
            threadflag = threadflag || flag_;
        }

        val_ = __shfl_up_sync(0xffffffff, threadtotal, 1);
        flag_ = __shfl_up_sync(0xffffffff, threadflag, 1);
        if(lane != 0)
            for(int i = 0; i < num_elements_per_thread; i++)
                if(!flags[threadIdx.x + (i * blockDim.x)]) {
                    vals[threadIdx.x + (i * blockDim.x)] += val_;
                    if(flag_) flags[threadIdx.x + (i * blockDim.x)] = true;
                }
                else break;
    }


    __device__ void segscan(gfloat *__restrict__ vals, bool *__restrict__ flags) {

        gfloat val_ = 0;
        bool flag_ = true;

        guint p, i;
        const int warpid = threadIdx.x >> warp_width;
        const int warp_last = (warpid << warp_width) + (warp_size - 1);
        const int lane = threadIdx.x & (warp_size - 1);

        warp_segscan(vals, flags, lane);
        __syncthreads();
        if(warpid == 0){
            p = (lane << warp_width) + (warp_size - 1);
            if(p < blockDim.x) {
                val_ = vals[p + ((num_elements_per_thread - 1) * blockDim.x)]; 
                flag_ = flags[p + ((num_elements_per_thread - 1) * blockDim.x)];
            }
            __syncwarp();
            warp_segscan(val_, flag_, lane);
            if(p < blockDim.x) {
                vals[p + ((num_elements_per_thread - 1) * blockDim.x)] = val_;
                flags[p + ((num_elements_per_thread - 1) * blockDim.x)] = flag_;
            }
        }
        __syncthreads();
        if(warpid != 0){
            val_ = vals[((warpid - 1) << warp_width) + (warp_size - 1) + ((num_elements_per_thread - 1) * blockDim.x)];
            p = num_elements_per_thread - (threadIdx.x == warp_last ? 1 : 0);
            for(i = 0; i < p; i++)
                if(!flags[threadIdx.x + (i * blockDim.x)]) {
                    vals[threadIdx.x + (i * blockDim.x)] += val_;
                    flags[threadIdx.x + (i * blockDim.x)] = true;
                }
                else break;
        }
    }

    __forceinline__ __device__ void warp_scan(guint &val, const int lane, guint val_) {
        #pragma unroll
        for(int i = 1; i < warp_size; i *= 2) {
            val_ = __shfl_up_sync(0xffffffff, val, i);
            if(lane >= i) val += val_;
        }
    }

    __forceinline__ __device__ void warp_scan(guint *vals, const int lane, guint val_) {
        guint threadtotal;

        for(int i = 1; i < num_elements_per_thread; i++)
            vals[threadIdx.x + (i * blockDim.x)] += vals[threadIdx.x + ((i-1) * blockDim.x)];
        threadtotal = vals[threadIdx.x + ((num_elements_per_thread - 1) * blockDim.x)];
        __syncwarp();

        for(int i = 1; i < warp_size; i *= 2) {
            val_ = __shfl_up_sync(0xffffffff, threadtotal, i);
            if(lane >= i) threadtotal += val_;
        }
        val_ = __shfl_up_sync(0xffffffff, threadtotal, 1);
        if(lane != 0)
            for(int i = 0; i < num_elements_per_thread; i++)
                vals[threadIdx.x + (i * blockDim.x)] += val_;

    }


    __device__ void scan(guint *__restrict__ vals) {
        guint p;
        const int warpid = threadIdx.x >> warp_width;
        const int warp_last = (warpid << warp_width) + (warp_size - 1);
        const int lane = threadIdx.x & (warp_size - 1);
        guint val_;

        warp_scan(vals, lane, val_);
        val_ = 0;
        __syncthreads();
        if(warpid == 0){
            p = (lane << warp_width) + (warp_size - 1);
            if(p < blockDim.x) 
                val_ = vals[p + ((num_elements_per_thread - 1) * blockDim.x)]; 
            __syncwarp();
            warp_scan(val_, lane, p);
            p = (lane << warp_width) + (warp_size - 1);
            if(p < blockDim.x)
                vals[p + ((num_elements_per_thread - 1) * blockDim.x)] = val_;
        }
        __syncthreads();
        if(warpid != 0){
            //val_ = vals[((warpid - 1) << warp_width) + (warp_size - 1) + ((num_elements_per_thread - 1) * blockDim.x)];
            p = num_elements_per_thread - (threadIdx.x == warp_last ? 1 : 0);
            for(int i = 0; i < p; i++)
                vals[threadIdx.x + (i * blockDim.x)] += vals[((warpid - 1) << warp_width) + (warp_size - 1) + ((num_elements_per_thread - 1) * blockDim.x)];
        }
    }

}
}
