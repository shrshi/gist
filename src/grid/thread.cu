#include <type.h>
#include <constants.h>
#include <threads.h>
#include <cassert>

namespace Gist {
namespace Thread {

    __device__ void set(guint &a, guint pos, bool val) {
        assert(pos < guint_typewidth);
        if(val) a |= 1 << (pos % guint_typewidth);
        else a &= ~(1 << (pos % guint_typewidth));
    }

    __device__ bool test(guint &a, guint pos) {
        assert(pos < guint_typewidth);
        if(a & (1 << (pos % guint_typewidth)))
            return true;
        return false;
    }

    __device__ void segscan(gfloat *vals, bool *flags) {
        #pragma unroll
        for(int i = 1; i < num_elements_per_thread; i++) {
            vals[i] += flags[i] ? 0 : vals[i-1];
            flags[i] = flags[i-1] | flags[i];
        }
    }

    __device__ void scan(guint *vals) {
        #pragma unroll
        for(int i = 1; i < num_elements_per_thread; i++)
            vals[i] += vals[i-1];
    }
}
}
