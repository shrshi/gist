#include <constants.h>
#include <type.h>
#include <threads.h>
#include <utils.h>
#include <cuda_runtime_api.h>
#include <cuda.h>

namespace Gist {
namespace Warp {

    __device__ void scan(guint &val, guint &total, const guint num_elements) {
        guint full_mask = 0xffffffff;
        guint p = threadIdx.x + (blockIdx.x * blockDim.x);
        guint mask = __ballot_sync(full_mask, p < num_elements);
        guint lane = threadIdx.x & (warp_size - 1);

        total = val;
        guint recvsum;
        if(p < num_elements) {
            for(int i = 1; i < 32; i *= 2){
                recvsum = __shfl_xor_sync(mask, total, i);
                total += recvsum;
                if(lane ^ i < lane) 
                    val += recvsum;
            }
        }
    }

    __device__ void scan(guint &val) {
        const int lane = threadIdx.x & (warp_size - 1);
        guint val_;
        for(int i = 1; i < warp_size; i *= 2) {
            val_ = __shfl_up_sync(0xffffffff, val, i);
            if(lane >= i) val += val_;
        }
    }

    __device__ void scan(guint *vals) {
        const int lane = threadIdx.x & (warp_size - 1);
        const int warpid = threadIdx.x >> warp_width;
        guint threadtotal, val_;
        int i;

        Thread::scan(vals);
        threadtotal = vals[num_elements_per_thread - 1];
        __syncwarp();

        for(i = 1; i < warp_size; i *= 2) {
            val_ = __shfl_up_sync(0xffffffff, threadtotal, i);
            if(lane >= i) threadtotal += val_;
        }
        val_ = __shfl_up_sync(0xffffffff, threadtotal, 1);
        if(lane != 0)
            for(int i = 0; i < num_elements_per_thread; i++)
                vals[i] += val_;
    }


    __device__ void segscan(gfloat &val, bool &flag, const guint num_elements) {
        const guint full_mask = 0xffffffff;
        const int lane = threadIdx.x & (warp_size - 1);
        guint p = threadIdx.x + (blockIdx.x * blockDim.x);
        guint mask = __ballot_sync(full_mask, p < num_elements);
        gfloat val_;
        bool flag_;
        if(p < num_elements) {
            for(int i = 1; i < warp_size; i *= 2) {
                val_ = __shfl_up_sync(mask, val, i);
                flag_ = __shfl_up_sync(mask, flag, i);
                val = flag || lane < i ? val : val + val_;
                flag = flag || flag_;
                //std::printf("thd %d i %d val %f flag %d\n", p, i, val, flag);
            }
        }
    }

    __device__ void segscan(gfloat &val, bool &flag) {
        const int lane = threadIdx.x & (warp_size - 1);
        gfloat val_;
        bool flag_;
        for(int i = 1; i < warp_size; i *= 2) {
            val_ = __shfl_up_sync(0xffffffff, val, i);
            flag_ = __shfl_up_sync(0xffffffff, flag, i);
            val = flag || lane < i ? val : val + val_;
            flag = flag || flag_;
        }
    }

    __device__ void segscan(gfloat *vals, bool *flags) {
        const int lane = threadIdx.x & (warp_size - 1);
        gfloat threadtotal, val_;
        bool flag_, threadflag;

        Thread::segscan(vals, flags);
        threadtotal = vals[num_elements_per_thread - 1];
        threadflag = flags[0] || flags[num_elements_per_thread - 1];
        __syncwarp();

        //std::printf("before: thd %d threadtotal %f threadflag %d\n", threadIdx.x + (blockIdx.x * blockDim.x), threadtotal, threadflag);
        for(int i = 1; i < warp_size; i *= 2) {
            val_ = __shfl_up_sync(0xffffffff, threadtotal, i);
            flag_ = __shfl_up_sync(0xffffffff, threadflag, i);
            threadtotal += threadflag || lane < i ? 0 : val_;
            threadflag = threadflag || flag_;
        }

        //std::printf("after: thd %d threadtotal %f threadflag %d\n", threadIdx.x + (blockIdx.x * blockDim.x), threadtotal, threadflag);
        val_ = __shfl_up_sync(0xffffffff, threadtotal, 1);
        flag_ = __shfl_up_sync(0xffffffff, threadflag, 1);
        if(lane != 0)
            #pragma unroll
            for(int i = 0; i < num_elements_per_thread; i++)
                if(!flags[i]) {
                    vals[i] += val_;
                    if(flag_) flags[i] = true;
                }
                else break;
    }

    __device__ bool isopen(const bool flag, const guint num_elements) {
        const guint full_mask = 0xffffffff;
        const int lane = threadIdx.x & (warp_size - 1);
        guint p = threadIdx.x + (blockIdx.x * blockDim.x);
        guint mask = __ballot_sync(full_mask, p < num_elements);
        int ret = __all_sync(mask, flag);
        return (ret ? false : true);
    }

    __device__ bool ORflags(const bool flag, const guint num_elements) {
        const guint full_mask = 0xffffffff;
        const int lane = threadIdx.x & (warp_size - 1);
        guint p = threadIdx.x + (blockIdx.x * blockDim.x);
        guint mask = __ballot_sync(full_mask, p < num_elements);
        int ret = __any_sync(mask, flag);
        return ret;
    }

    //broadcasts val at last thread in warp
    __device__ void broadcast(gfloat &val, const guint num_elements) {
        const guint full_mask = 0xffffffff;
        const int lane = threadIdx.x & (warp_size - 1);
        guint p = threadIdx.x + (blockIdx.x * blockDim.x);
        guint mask = __ballot_sync(full_mask, p < num_elements);
        val = __shfl_sync(mask, val, warp_size - 1);
    }

    __device__ void segscan(const guint num_elements_per_block, 
            const guint num_elements_per_thread, 
            volatile guint *__restrict__ flags, 
            volatile gfloat *__restrict__ vals, bool debug_print) {

        //std::printf("Warp Blk: %d Thread: %d\n", blockIdx.x, threadIdx.x);
        const guint lane = threadIdx.x & ((1 << warp_width) - 1);
        if(debug_print && lane == 0) 
            std::printf("thd %d num_elements_per_block %d\n", threadIdx.x, num_elements_per_block);
        guint p;
        for(guint i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x + (i * blockDim.x);
            if(p < num_elements_per_block){
                if(lane >= 1) {
                    vals[p] = flags[p] ? vals[p] : vals[p-1] + vals[p];
                    flags[p] = flags[p-1] | flags[p];
                }
                if(lane >= 2) {
                    vals[p] = flags[p] ? vals[p] : vals[p-2] + vals[p];
                    flags[p] = flags[p-2] | flags[p];
                }
                if(lane >= 4) {
                    vals[p] = flags[p] ? vals[p] : vals[p-4] + vals[p];
                    flags[p] = flags[p-4] | flags[p];
                }
                if(lane >= 8) {
                    vals[p] = flags[p] ? vals[p] : vals[p-8] + vals[p];
                    flags[p] = flags[p-8] | flags[p];
                }
                if(lane >= 16) {
                    vals[p] = flags[p] ? vals[p] : vals[p-16] + vals[p];
                    flags[p] = flags[p-16] | flags[p];
                }
            }
        }
    }
}
}
