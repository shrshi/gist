#include <type.h>
#include <csf.h>
#include <tree.h>
#include <utils.h>
#include <matrix.h>
#include <vector>
#include <cassert>
#include <iostream>

namespace Gist{
using Matrix::matrix;
using Tree::dev_level;
namespace Tree {

    std::vector<gfloat> tree::mttkrp3_cpu(std::vector<matrix>& factors) {
        assert(depth == 3);
        Utils::permute(factors, std::vector<guint>(level_mode_map, level_mode_map + depth));
        rank = factors[0].num_cols();

        std::vector<gfloat> out(level_size[0] * rank, 0);
        //std::printf("l2a_scan : ");
        //std::printf("l1a : ");
        for(guint r = 0; r < rank; r++)
            for(guint i = 0; i < level_size[0]; i++) {
                out[r * level_size[0] + i] = 0;
                for(guint ptri = level_ptr[0][i]; ptri < level_ptr[0][i+1]; ptri++) {
                    gfloat sum = 0;
                    for(guint ptrj = level_ptr[1][ptri]; ptrj < level_ptr[1][ptri+1]; ptrj++) {
                        sum += factors[2].valueat(level_idx[2][ptrj] - 1, r) * values[ptrj];
                        //std::printf("%.2f ", sum);
                    }
                    out[r * level_size[0] + i] += sum * factors[1].valueat(level_idx[1][ptri] - 1, r);
                    //std::printf("%.2f ", sum * factors[1].valueat(level_idx[1][ptri] - 1, r));
                    //std::printf("%.2f ", out[r * level_size[0] + i]);
                    /*
                    if(r * level_size[0] + i == 17470){
                        for(guint ptrj = level_ptr[1][ptri]; ptrj < level_ptr[1][ptri+1]; ptrj++) {
                            std::printf("Nonzero : %d %d %d %.6f\n", level_idx[0][i], level_idx[1][ptri], level_idx[2][ptrj], values[ptrj]);
                            std::printf("Mtx vals : %.6f %.6f\n", factors[1].valueat(level_idx[1][ptri] - 1, r), factors[2].valueat(level_idx[2][ptrj] - 1, r));
                        }
                    }
                    */
                }
            }
        //std::printf("\n");
        Utils::permute(factors, std::vector<guint>(level_mode_map, level_mode_map + depth), true);
        return out;
    }
}
}
