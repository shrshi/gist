#include <type.h>
#include <constants.h>
#include <tree.h>
#include <utils.h>
#include <matrix.h>
#include <device.h>
#include <kernels.h>
#include <err.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cub/cub.cuh>
#include <iostream>
#include <algorithm>

namespace Gist {
using Matrix::matrix;
using dmatrix = Device::objptr<Matrix::matrix>;
using Tree::dev_level;
using ddev_level = Device::objptr<Tree::dev_level>;
template<class T>
using darray = Device::array<T>;
using dbitops = Device::objptr<Utils::bitops>;
namespace Tree {
    
    //assuming third order tensor
    darray<gfloat> tree::mttkrp3(std::vector<matrix> &factors) {

        assert(depth == 3);
        Utils::permute(factors, std::vector<guint>(level_mode_map, level_mode_map + depth));
        rank = factors[0].num_cols();

        guint num_l2flags = level_flags[1].num_bits();
        guint num_l1flags = level_flags[0].num_bits();
        darray<guint> l2_arrflags(level_flags[1].toarray());
        darray<guint> l2_segments(num_l2flags);
        darray<guint> l1_arrflags(level_flags[0].toarray());
        darray<guint> l1_segments(num_l1flags);

        void *d_temp_storage = NULL;
        size_t temp_storage_bytes = 0;
        cub::DeviceScan::InclusiveSum(d_temp_storage, temp_storage_bytes, l2_arrflags.object(), l2_segments.object(), num_l2flags);
        cuErrChk(cudaMalloc((void**)&d_temp_storage, temp_storage_bytes));
        cub::DeviceScan::InclusiveSum(d_temp_storage, temp_storage_bytes, l2_arrflags.object(), l2_segments.object(), num_l2flags);
        cuErrChk(cudaFree(d_temp_storage));

        d_temp_storage = NULL;
        temp_storage_bytes = 0;
        cub::DeviceScan::InclusiveSum(d_temp_storage, temp_storage_bytes, l1_arrflags.object(), l1_segments.object(), num_l1flags);
        cuErrChk(cudaMalloc((void**)&d_temp_storage, temp_storage_bytes));
        cub::DeviceScan::InclusiveSum(d_temp_storage, temp_storage_bytes, l1_arrflags.object(), l1_segments.object(), num_l1flags);
        cuErrChk(cudaFree(d_temp_storage));

        guint numblocks = std::ceil((double)((uint64_t)(num_l2flags) * (uint64_t)(rank)) / (double)((uint64_t)blockdim * (uint64_t)num_elements_per_thread));
        auto l2segs = l2_segments.retrieve();
        auto l1segs = l1_segments.retrieve();
        std::vector<uint64_t> l2bsegs(numblocks);
        std::vector<uint64_t> l1bsegs(numblocks);
        for(uint64_t i = 0; i < numblocks; i++){
            uint64_t block_offset = i * (uint64_t)blockdim * (uint64_t)num_elements_per_thread;
            assert(num_l1flags == l2segs[num_l2flags - 1]);
            assert(level_size[0] == l1segs[num_l1flags - 1]);
            //l2bsegs[i] = l2segs[block_offset % num_l2flags] + (block_offset / num_l2flags) * num_l1flags;
            l2bsegs[i] = l2segs[block_offset % (uint64_t)num_l2flags];
            block_offset = l2bsegs[i] + ((uint64_t)(block_offset / (uint64_t)num_l2flags) * (uint64_t)num_l1flags);
            l1bsegs[i] = l1segs[(uint64_t)(block_offset - 1) % (uint64_t)num_l1flags] + (uint64_t)(((block_offset - 1) / (uint64_t)num_l1flags) * (uint64_t)level_size[0]);
        }
        
        /*
        std::cout << "l2-segments : ";
        auto l2segs = l2_segments.retrieve();
        for(auto i : l2segs)
            std::cout << i << " ";
        std::cout << std::endl;
        std::cout << "l1-segments : ";
        auto l1segs = l1_segments.retrieve();
        for(auto i : l1segs)
            std::cout << i << " ";
        std::cout << std::endl;
        */

        darray<uint64_t> l2_block_segments(l2bsegs);
        darray<uint64_t> l1_block_segments(l1bsegs);
        dmatrix l2_matrix(factors[2]);
        dmatrix l1_matrix(factors[1]);
        ddev_level l2pos(dev_level(*this, 2));
        ddev_level l1pos(dev_level(*this, 1));
        darray<gfloat> scale(values, level_size[2]);
        dbitops l2_flags(level_flags[1]);
        dbitops l1_flags(level_flags[0]);

        std::printf("mttkrp3 on %d blocks ", numblocks);
        darray<gfloat> outval((uint64_t)level_size[0] * (uint64_t)rank);
        outval.constant(0.0f);

        //guint shmem_alloc = (blockdim * num_elements_per_thread * sizeof(gfloat)) + (blockdim * num_elements_per_thread * (sizeof(guint) + sizeof(gfloat) + sizeof(bool))) + sizeof(guint);
        //guint shmem_alloc = (blockdim * num_elements_per_thread * (2 * sizeof(gfloat) + sizeof(guint))) + (blockdim * sizeof(guint)) + sizeof(guint);
        //guint shmem_alloc = (blockdim * num_elements_per_thread * sizeof(gfloat)) + (warp_size * (sizeof(gfloat) + 2 * sizeof(guint))) + sizeof(guint);
        //guint shmem_alloc = (blockdim * num_elements_per_thread * sizeof(gfloat)) + (blockdim * (sizeof(gfloat) + 2 * sizeof(guint))) + sizeof(guint);
        //guint shmem_alloc = (blockdim * num_elements_per_thread * sizeof(gfloat)) + (blockdim * (sizeof(gfloat) + sizeof(guint))) + sizeof(guint);
        guint shmem_alloc = blockdim * (sizeof(gfloat) + sizeof(guint));
        assert(shmem_alloc <= shmem_size);
        cudaEvent_t start, stop;
        /* SETTING L! CACHE PREFERENCE
        cuErrChk(cudaFuncSetCacheConfig(Kernels::mttkrp3, cudaFuncCachePreferL1));
        */
        cudaEventCreate(&start);
        cudaEventCreate(&stop);

        cudaEventRecord(start);
        Kernels::mttkrp3 <<< numblocks, blockdim, shmem_alloc >>> (
                l2_matrix.object(), 
                l2pos.object(),
                scale.object(),
                l2_flags.object(),
                l2_block_segments.object(),
                l1_matrix.object(),
                l1pos.object(),
                l1_flags.object(), 
                l1_block_segments.object(), 
                outval.object()
                );
        cudaEventRecord(stop);
        cuErrChk(cudaPeekAtLastError());
        cuErrChk(cudaDeviceSynchronize());

        cudaEventSynchronize(stop);
        float milliseconds = 0;
        cudaEventElapsedTime(&milliseconds, start, stop);
        std::printf("in %.3f ms\n", milliseconds);

        /*
        auto oval = outval.retrieve();
        std::cout << "outval : ";
        for(auto e : oval)
            std::cout << e << " ";
        std::cout << std::endl;
        */

        Utils::permute(factors, std::vector<guint>(level_mode_map, level_mode_map + depth), true);

        return outval;
    }

    darray<gfloat> tree::mttkrp4(std::vector<matrix> &factors) {
        darray<gfloat> outval(level_size[0] * factors[0].num_cols());
        outval.constant(0.0f);
        return outval;
    }
}
}
