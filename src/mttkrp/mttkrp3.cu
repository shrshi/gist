#include <type.h>
#include <constants.h>
#include <device.h>
#include <kernels.h>
#include <matrix.h>
#include <utils.h>
#include <tree.h>
#include <threads.h>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <cstdlib>

namespace Gist {
namespace Kernels {
    using Matrix::matrix;
    using Tree::dev_level;
    using Utils::bitops;

    __device__ void retrieve(bitops const *__restrict__ flags, 
            guint startpos, guint n, guint &ret, guint &segment_end) {

        guint nbits, size, width, pos, w, retrieved = 0, t;
        nbits = flags->nbits;
        size = flags->size;
        width = flags->width;
        /*
           * retrieve n+1 elements into ret
           * discard last bit for flags
           * discard first bit for segment_end
           */
        n += 1;
        pos = startpos / width;
        w = width;
        if(pos == size - 1) {
            w = nbits & (width - 1);
            if(!w) w = width;
        }
        startpos = startpos & (width - 1); //startpos = startpos % width;

        ret = flags->buf[pos] >> startpos;
        w = w - startpos;
        t = ~0 >> (width-w);
        if(n <= w) t = t >> (w-n);
        ret = ret & t;
        retrieved += n < w ? n : w;
        n = n - w;

        for(int i = 1; n && n < width && i < 3; i++) {
            pos = pos + 1;
            if(pos == size) pos = 0;
            w = width;
            if(pos == size - 1) {
                w = nbits & (width - 1);
                if(!w) w = width;
            }

            t = ~0 >> (width-w);
            if(n <= w) t = t >> (w-n);
            t = flags->buf[pos] & t; 
            t = t << retrieved;
            ret = t | ret;

            retrieved += n < w ? n : w;
            n = n - w; 
        }
        segment_end = ret >> 1;
    }

    __global__ void mttkrp3(matrix const *__restrict__ l2_matrix, 
            dev_level const *__restrict__ l2pos, 
            gfloat const *__restrict__ scale, 
            bitops const *__restrict__ l2_flags, 
            uint64_t const *__restrict__ l2_segments,
            matrix const *__restrict__ l1_matrix, 
            dev_level const *__restrict__ l1pos, 
            bitops const *__restrict__ l1_flags, 
            uint64_t const *__restrict__ l1_segments, 
            gfloat *__restrict__ outval
            ) {

        extern __shared__ gfloat shared_mem[];
        gfloat *svals = (gfloat*) (shared_mem);
        guint *sblock_segments = (guint*) &svals[blockDim.x];
        guint is_segment_end = 0, thflags; //store as bools as bits in word
        uint64_t block_offset = 0;
        gfloat val_ = 0, val__ = 0;
        guint seg_ = 0, p, j, flag_;
        const int warpid = threadIdx.x >> warp_width;
        const int lane = threadIdx.x & (warp_size - 1);

        guint nflags = l2_flags->nbits, row = 0, row_;
        guint pos, w;
        vgfloat thvals; thvals.x = 0; thvals.y = 0; thvals.z = 0; thvals.w = 0;
        vguint thsegs;
        thflags = 0; is_segment_end = 0;
        int col = (((uint64_t)threadIdx.x + ((uint64_t)blockIdx.x * (uint64_t)blockDim.x)) * (uint64_t)num_elements_per_thread) / (uint64_t)nflags;
        if(col >= l2_matrix->ncols) col = -1;
        if(col != -1) {
            row = (((uint64_t)threadIdx.x + ((uint64_t)blockIdx.x * (uint64_t)blockDim.x)) * (uint64_t)num_elements_per_thread) % (uint64_t)nflags;
            j = l2_flags->size;
            pos = row >> 5;
            w = guint_typewidth;
            if(pos == j - 1) {
                w = nflags & (guint_typewidth - 1); if(!w) w = guint_typewidth;
            }
            row_ = row & (guint_typewidth - 1); //startpos = startpos % width;
            seg_ = row_ + num_elements_per_thread + 1 <= w ? 5 : 4;
            thflags = (l2_flags->buf[pos] >> row_) & ((unsigned int)(1 << (seg_ + 1)) - 1);
            pos++; flag_ = (pos == j || seg_ == 5 ? 0 : l2_flags->buf[pos]);
            thflags = thflags | (flag_ & 1) << 4;
            is_segment_end = thflags >> 1;
            if(pos == j && seg_ == 4) is_segment_end |= (unsigned int)(1 << 3);

            thvals = reinterpret_cast<const vgfloat*>(scale)[row >> num_elements_per_thread_width];
            thsegs = reinterpret_cast<const vguint*>(l2pos->lidx)[row >> num_elements_per_thread_width];
            uint64_t nrows = l2_matrix->nrows;
            thvals.x *= l2_matrix->values[(col * nrows) + thsegs.x - 1];
            thvals.y *= l2_matrix->values[(col * nrows) + thsegs.y - 1];
            thvals.z *= l2_matrix->values[(col * nrows) + thsegs.z - 1];
            thvals.w *= l2_matrix->values[(col * nrows) + thsegs.w - 1];
        }

        thsegs.x = thflags & 1;
        thsegs.y = (thflags & 2) >> 1;
        thsegs.z = (thflags & 4) >> 2;
        thsegs.w = (thflags & 8) >> 3;

        if(threadIdx.x == 0) {
            thflags = (thflags >> 1) << 1;
            thsegs.x = 0;
        }
        if(threadIdx.x == blockDim.x - 1 && col != -1) 
            is_segment_end = is_segment_end | (1 << (num_elements_per_thread - 1));

        block_offset = l2_segments[blockIdx.x];
        //if(blockIdx.x == 0) std::printf("l2a: thd %d thvals %.2f %.2f %.2f %.2f thsegs %d %d %d %d is_segment_end %d %d %d %d block_offset %d\n", threadIdx.x + (blockIdx.x * blockDim.x), thvals.x, thvals.y, thvals.z, thvals.w, thsegs.x, thsegs.y, thsegs.z, thsegs.w, is_segment_end & 1, (is_segment_end & 2) >> 1, (is_segment_end & 4) >> 2, (is_segment_end & 8) >> 3, block_offset);
        thsegs.y += thsegs.x; thsegs.w += thsegs.z; thsegs.z += thsegs.y; thsegs.w += thsegs.y;
        if(!(thflags & 2)) thvals.y += thvals.x;
        if(!(thflags & 4)) thvals.z += thvals.y;
        if(!(thflags & 8)) thvals.w += thvals.z;

        svals[threadIdx.x] = thvals.w; 
        sblock_segments[threadIdx.x] = thsegs.w;
        __syncthreads();
        if(warpid == 0) {
            gfloat vals_[4]; guint segs_[4];
            vals_[0] = svals[4*lane];
            segs_[0] = sblock_segments[4*lane];
            for(int i = 1; i < 4; i++) {
                segs_[i] = sblock_segments[(4*lane) + i];
                vals_[i] = svals[(4*lane) + i];
                if(!segs_[i]) vals_[i] += vals_[i-1];
                segs_[i] += segs_[i-1];
            }
            val_ = __shfl_up_sync(0xffffffff, vals_[3], 1);
            seg_ = __shfl_up_sync(0xffffffff, segs_[3], 1);
            if(!lane) { 
                val_ = 0; 
                seg_ = 0;
            }
            for(int i = 1; i < warp_size; i *= 2) {
                val__ = __shfl_up_sync(0xffffffff, val_, i);
                j = __shfl_up_sync(0xffffffff, seg_, i);
                val_ = seg_ || lane < i ? val_ : val_ + val__;
                if(lane >= i) seg_ += j;
            }
            svals[4*lane] = val_; sblock_segments[4*lane] = seg_;
            for(int i = 1; i < 4; i++){
                svals[(4*lane) + i] = !segs_[i-1] ? vals_[i-1] + val_ : vals_[i-1];
                sblock_segments[(4*lane) + i] = segs_[i-1] + seg_;
            }
        }
        __syncthreads();
        val_ = svals[threadIdx.x];
        if(!thsegs.x) thvals.x += val_;
        if(!thsegs.y) thvals.y += val_;
        if(!thsegs.z) thvals.z += val_;
        if(!thsegs.w) thvals.w += val_;
        seg_ = sblock_segments[threadIdx.x];
        thsegs.x += seg_; thsegs.y += seg_; thsegs.z += seg_; thsegs.w += seg_;
        //if(blockIdx.x == 0) std::printf("l2a_scan: thd %d thvals %.2f %.2f %.2f %.2f thsegs %d %d %d %d\n", threadIdx.x + (blockIdx.x * blockDim.x), thvals.x, thvals.y, thvals.z, thvals.w, thsegs.x, thsegs.y, thsegs.z, thsegs.w);

        gfloat thvals_[4] = {0};
        if(col == -1) {
            thflags = 0; is_segment_end = 0;
        }
        if(col != -1) {
            nflags = l1_flags->nbits;
            row = (thsegs.x + block_offset - 1) % nflags; p = row;
            uint64_t nrows = l1_matrix->nrows;

            if(is_segment_end & 1)
                thvals_[0] = thvals.x * l1_matrix->values[(col * nrows) + l1pos->lidx[row++] - 1];
            if(is_segment_end & 2)
                thvals_[1] = thvals.y * l1_matrix->values[(col * nrows) + l1pos->lidx[row++] - 1];
            if(is_segment_end & 4)
                thvals_[2] = thvals.z * l1_matrix->values[(col * nrows) + l1pos->lidx[row++] - 1];
            if(is_segment_end & 8)
                thvals_[3] = thvals.w * l1_matrix->values[(col * nrows) + l1pos->lidx[row] - 1];
            row = p;
            j = l1_flags->size;
            pos = row >> 5; w = guint_typewidth;
            if(pos == j - 1) {
                w = nflags & (guint_typewidth - 1); if(!w) w = guint_typewidth;
            }
            row_ = row & (guint_typewidth - 1); //startpos = startpos % width;
            seg_ = row_ + num_elements_per_thread + 1 <= w ? num_elements_per_thread + 1 : w - row_;
            thflags = (l1_flags->buf[pos] >> row_) & ((unsigned int)(1 << (seg_ + 1)) - 1);
            pos++; if(pos == j) pos = 0; 
            flag_ = (seg_ == num_elements_per_thread + 1 ? 0 : l1_flags->buf[pos]);
            flag_ = flag_ & ((unsigned int)(1 << (num_elements_per_thread + 1 - seg_)) - 1);
            thflags = thflags | flag_ << seg_;
            if(thsegs.x == 0) thflags = (thflags >> 1) << 1;
            flag_ = thflags >> 1;
            if(!(is_segment_end & 1)) {
                thflags <<= 1; flag_ <<= 1;
            }
            if(!(is_segment_end & 2)) {
                seg_ = thflags & 1; thflags = (thflags >> 1) << 2; thflags |= seg_;
                seg_ = flag_ & 1; flag_ = (flag_ >> 1) << 2; flag_ |= seg_;
            }
            if(!(is_segment_end & 4)) {
                seg_ = thflags & 3; thflags = (thflags >> 2) << 3; thflags |= seg_;
                seg_ = flag_ & 3; flag_ = (flag_ >> 2) << 3; flag_ |= seg_;
            }
            if(!(is_segment_end & 8)) {
                seg_ = thflags & 7; thflags = (thflags >> 3) << 4; thflags |= seg_;
                seg_ = flag_ & 7; flag_ = (flag_ >> 3) << 4; flag_ |= seg_;
            }
            is_segment_end = flag_;
        }
        thvals.x = thvals_[0]; thvals.y = thvals_[1]; 
        thvals.z = thvals_[2]; thvals.w = thvals_[3];

        thsegs.x = thflags & 1;
        thsegs.y = (thflags & 2) >> 1;
        thsegs.z = (thflags & 4) >> 2;
        thsegs.w = (thflags & 8) >> 3;

        if(threadIdx.x == blockDim.x - 1 && col != -1) 
            is_segment_end = is_segment_end | (1 << (num_elements_per_thread - 1));

        block_offset = l1_segments[blockIdx.x];
        //if(blockIdx.x == 0) std::printf("l1a: thd %d thvals %.2f %.2f %.2f %.2f thsegs %d %d %d %d is_segment_end %d %d %d %d block_offset %d\n", threadIdx.x + (blockIdx.x * blockDim.x), thvals.x, thvals.y, thvals.z, thvals.w, thsegs.x, thsegs.y, thsegs.z, thsegs.w, is_segment_end & 1, (is_segment_end & 2) >> 1, (is_segment_end & 4) >> 2, (is_segment_end & 8) >> 3, block_offset);

        thsegs.y += thsegs.x; thsegs.w += thsegs.z; thsegs.z += thsegs.y; thsegs.w += thsegs.y;
        if(!(thflags & 2)) thvals.y += thvals.x;
        if(!(thflags & 4)) thvals.z += thvals.y;
        if(!(thflags & 8)) thvals.w += thvals.z;
        svals[threadIdx.x] = thvals.w; 
        sblock_segments[threadIdx.x] = thsegs.w;
        __syncthreads();
        if(warpid == 0) {
            gfloat vals_[4]; guint segs_[4];
            vals_[0] = svals[4*lane];
            segs_[0] = sblock_segments[4*lane];
            for(int i = 1; i < 4; i++) {
                segs_[i] = sblock_segments[(4*lane) + i];
                vals_[i] = svals[(4*lane) + i];
                if(!segs_[i]) vals_[i] += vals_[i-1];
                segs_[i] += segs_[i-1];
            }
            val_ = __shfl_up_sync(0xffffffff, vals_[3], 1);
            seg_ = __shfl_up_sync(0xffffffff, segs_[3], 1);
            if(!lane) { 
                val_ = 0; 
                seg_ = 0;
            }
            for(int i = 1; i < warp_size; i *= 2) {
                val__ = __shfl_up_sync(0xffffffff, val_, i);
                j = __shfl_up_sync(0xffffffff, seg_, i);
                val_ = seg_ || lane < i ? val_ : val_ + val__;
                if(lane >= i) seg_ += j;
            }
            svals[4*lane] = val_; sblock_segments[4*lane] = seg_;
            for(int i = 1; i < 4; i++){
                svals[(4*lane) + i] = !segs_[i-1] ? vals_[i-1] + val_ : vals_[i-1];
                sblock_segments[(4*lane) + i] = segs_[i-1] + seg_;
            }
        }
        __syncthreads();
        val_ = svals[threadIdx.x];
        if(!thsegs.x) thvals.x += val_;
        if(!thsegs.y) thvals.y += val_;
        if(!thsegs.z) thvals.z += val_;
        if(!thsegs.w) thvals.w += val_;
        seg_ = sblock_segments[threadIdx.x];
        thsegs.x += seg_; thsegs.y += seg_; thsegs.z += seg_; thsegs.w += seg_;
        //if(blockIdx.x == 0) std::printf("l1a_scan: thd %d thvals %.2f %.2f %.2f %.2f thsegs %d %d %d %d\n", threadIdx.x + (blockIdx.x * blockDim.x), thvals.x, thvals.y, thvals.z, thvals.w, thsegs.x, thsegs.y, thsegs.z, thsegs.w);

        if(is_segment_end & 1) 
            atomicAdd(&(outval[block_offset + thsegs.x - 1]), thvals.x);
        if(is_segment_end & 2) 
            atomicAdd(&(outval[block_offset + thsegs.y - 1]), thvals.y);
        if(is_segment_end & 4) 
            atomicAdd(&(outval[block_offset + thsegs.z - 1]), thvals.z);
        if(is_segment_end & 8) 
            atomicAdd(&(outval[block_offset + thsegs.w - 1]), thvals.w);
    }
}
}
