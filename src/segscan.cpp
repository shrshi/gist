#include <type.h>
#include <constants.h>
#include <tree.h>
#include <matrix.h>
#include <device.h>
#include <utils.h>
#include <kernels.h>

namespace Gist {
namespace Tree {
    using Matrix::matrix;
    using dm = Device::objptr<Matrix::matrix>;
    using Tree::dev_level;
    using ddev_level = Device::objptr<Tree::dev_level>;
    using darray = Device::array<float>;
    using dbitops = Device::objptr<Utils::bitops>;
    using dsegscan_storage = Device::objptr<Kernels::segscan_storage>;

    void tree::segscan(const dm &factor, std::vector<Kernels::segscan_storage> &auxstorage, guint level) {

        guint num_elements = level_size[depth-1] * rank;
        /*
           * shmem_size = 
           *    num_elements_per_block * sizeof(gfloat) [for data] + 
           *    num_elements_per_block * sizeof(guint) [for min_indices] + 
           *    ceil(num_elements_per_block / bitops.width) [for flags]
           *
        */
        guint num_elements_per_block = std::floor(shmem_size - 1) / (sizeof(gfloat) + sizeof(gfloat) + (gfloat)(1. / Utils::bitops.width));
        guint num_blocks = std::max(1, std::ceil((gfloat) num_elements / (gfloat)num_elements_per_block));

        darray scale(values, level_size[depth-1]);
        ddev_level pos(dev_level(level_size[depth-1], level_idx[depth-1]));
        dbitops flags(level_flags[depth-2]);
        
        Kernels::segscan <<< num_blocks, blockdim, shmem_size >>> (
                num_elements_per_block, 
                factor.object(), 
                pos.object(), 
                scale.object(), 
                flags.object(), 
                auxstorage[level]);
    }
}
}
