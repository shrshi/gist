#include <type.h>
#include <coo.h>
#include <matrix.h>
#include <fstream>
#include <cassert>
#include <vector>
#include <string>
#include <unordered_map>

namespace Gist {
namespace Coo {

    coo::coo(std::ifstream &f) {
        if(!f) {
            std::cout << "File does not exist!\n";
            // some exception handling
        }
        std::string line;
        f >> nmodes;
        dimensions = new guint[nmodes];
        for(guint m = 0; m < nmodes; m++)
            f >> dimensions[m];
        std::getline(f, line);
        nnz = 0;
        while(std::getline(f, line)){
            nnz++;
            if(!nnz) {
                std::cout << "Overflow! Increase gist unsigned int type width\n" << std::flush;
                assert(nnz > 0);
            }
        }
        indices = new guint*[nmodes];
        for(guint m = 0; m < nmodes; m++)
            indices[m] = new guint[nnz];
        values = new gfloat[nnz];

        f.clear();
        f.seekg(0, std::ios::beg);
        std::getline(f, line);
        std::getline(f, line);
        guint nz = 0;
        //while(true) {
        while(nz < nnz) {
            for(guint i = 0; i < nmodes; i++)
                f >> indices[i][nz];
            f >> values[nz];
            //if(!f.good()) break;
            nz++;
        }
        assert(nz == nnz);
    }
    void coo::print(std::ostream &f) const {
        f << nmodes << std::endl;
        for(guint i = 0; i < nmodes; i++)
            f << dimensions[i] << " ";
        f << std::endl;
        for(guint i = 0; i < nnz; i++){
            for(guint m = 0; m < nmodes; m++)
                f << indices[m][i] << " ";
            f << values[i] << std::endl;
        }
    }

    void coo::sort(const std::vector<guint> &sort_order) {
        quick_sort_indices(0, nnz, sort_order);
    }

    void coo::quick_sort_indices(guint low, guint high, const std::vector<guint> &sort_order) {
        guint i, j, pivot;
        if(high-low < 2) return;
        pivot = (high+low) / 2;
        for(i = low, j = high-1; ; ++i, --j) {
            while(compare_indices(i, pivot, sort_order) < 0) 
                ++i;
            while(compare_indices(pivot, j, sort_order) < 0) 
                --j;
            if(i >= j) break;

            swap_indices(i, j);

            if(i == pivot) 
                pivot = j;
            else if(j == pivot) 
                pivot = i;
        }
        quick_sort_indices(low, i, sort_order);
        quick_sort_indices(i, high, sort_order);
    }

    int coo::compare_indices(guint i, guint j, const std::vector<guint> &sort_order) {
        guint idx_i, idx_j;
        for(guint k = 0; k < sort_order.size(); k++){
            idx_i = indices[sort_order[k]][i];
            idx_j = indices[sort_order[k]][j];
            if(idx_i < idx_j)
                return -1;
            else if(idx_i > idx_j)
                return 1;
        }
        return 0;
    }

    void coo::swap_indices(guint i, guint j) {
        for(guint k = 0; k < nmodes; k++)
            std::swap(indices[k][i], indices[k][j]);
        std::swap(values[i], values[j]);
    }

    std::vector<guint> coo::dims() const {
        std::vector<guint> ret(dimensions, dimensions + nmodes);
        return ret;
    }

    std::vector<gfloat> coo::mttkrp(guint mode, const std::vector<Matrix::matrix> &factors) {
        std::vector<guint> sort_order(nmodes, 0);
        for(guint i = 0; i < nmodes; i++)
            sort_order[i] = i;
        sort_order[0] = mode;
        sort_order[mode] = 0;
        sort(sort_order); // ensures that COO tensor is sorted

        guint rank = factors[0].num_cols();
        std::unordered_map<guint, guint> outmap;
        uint64_t nrows, row = 0;
        gfloat prod;

        for(guint k = 0; k < nnz; k++)
            if(outmap.find(indices[mode][k]) == outmap.end()){
                outmap[indices[mode][k]] = row;
                row++;
            }
        /*
        for(guint k = 1; k < nnz; k++)
            if(indices[mode][k] != indices[mode][k-1])
                nrows++;
                */
        nrows = row;
        std::vector<gfloat> ret((uint64_t)nrows * (uint64_t)rank, 0.0f);
        for(uint64_t r = 0; r < rank; r++){
            for(uint64_t k = 0; k < nnz; k++){
                prod = values[k];
                for(guint m = 0; m < nmodes; m++) {
                    if(m == mode) continue;
                    prod *= factors[m].valueat(indices[m][k] - 1 , r);
                }
                row = outmap[indices[mode][k]];
                ret[r * nrows + row] += prod;
            }
        }
        return ret;
    }
}
}
