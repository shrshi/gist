#include <type.h>
#include <csf.h>
#include <coo.h>
#include <tree.h>
#include <device.h>
#include <algorithm>
#include <cstdint>
#include <vector>
#include <sstream>
#include <cassert>
#include <iomanip>

namespace Gist {
template<class T>
using darray = Device::array<T>;
namespace Csf {

    csf::csf(Coo::coo &coo_t) {
        nmodes = coo_t.num_modes();
        dimensions = coo_t.dims();
        nnz = coo_t.num_nonzeros();

        std::vector<guint> sorted_modes(nmodes);
        for(guint i = 0; i < nmodes; i++)
            sorted_modes[i] = i;
        std::sort(sorted_modes.begin(), sorted_modes.end(), 
                [&] (const guint &i, const guint &j) {
                return dimensions[i] < dimensions[j];
                });

        tensors.reserve(nmodes);
        for(guint m = 0; m < nmodes; m++){
            std::vector<guint> sort_order(nmodes);
            sort_order[0] = m;
            int flag = 0;
            for(guint n = 0; n < nmodes; n++){
                if(sorted_modes[n] != m){
                    if(!flag) sort_order[n+1] = sorted_modes[n];
                    else sort_order[n] = sorted_modes[n];
                }
                else flag = 1;
            }
            coo_t.sort(sort_order);
            tensors.push_back(Tree::tree(coo_t, sort_order));
        }
    }

    csf::csf(std::ifstream &f) {
        uint64_t guint_size, gfloat_size;
        f.read(reinterpret_cast<char*>(&guint_size), sizeof(uint64_t));
        f.read(reinterpret_cast<char*>(&gfloat_size), sizeof(uint64_t));
        if(guint_size != sizeof(guint) || gfloat_size != sizeof(gfloat)) {
            std::cout << "Tensor has types of width larger than gist unsigned int and gist float!\n" << std::flush;
            assert(guint_size == sizeof(guint) || gfloat_size == sizeof(gfloat));
        }
        f.read(reinterpret_cast<char*>(&nmodes), sizeof(nmodes));
        guint dims[nmodes];
        f.read(reinterpret_cast<char*>(dims), nmodes * sizeof(guint));
        std::copy(&dims[0], &dims[nmodes], std::back_inserter(dimensions));
        f.read(reinterpret_cast<char*>(&nnz), sizeof(guint));
        tensors.reserve(nmodes);
        for(guint i = 0; i < nmodes; i++)
            tensors.push_back(Tree::tree(f, nmodes));
    }

    void csf::print(ostream &f) const {
        stringstream ss;
        stringify(ss);
        f << ss.rdbuf();
    }

    void csf::print(const guint m, ostream &f) const {
        stringstream ss;
        stringify(m, ss);
        f << ss.rdbuf();
    }

    void csf::stringify(const guint m, stringstream &f) const {
        f << std::fixed;
        f << std::setprecision(3);
        f << "CSF tree\n";
        f << nmodes << std::endl;
        for(guint i = 0; i < dimensions.size(); i++)
            f << dimensions[i] << " ";
        f << std::endl;
        tensors[m].stringify(f);
    }

    void csf::stringify(stringstream &f) const {
        f << std::fixed;
        f << std::setprecision(3);
        f << "CSF tree\n";
        f << nmodes << std::endl;
        for(guint i = 0; i < dimensions.size(); i++)
            f << dimensions[i] << " ";
        f << std::endl;
        for(guint i = 0; i < nmodes; i++)
            tensors[i].stringify(f);
    }

    void csf::write(ofstream &f) {
        uint64_t guint_size = sizeof(guint);
        uint64_t gfloat_size = sizeof(gfloat);
        f.write(reinterpret_cast<char*>(&guint_size), sizeof(uint64_t));
        f.write(reinterpret_cast<char*>(&gfloat_size), sizeof(uint64_t));
        f.write(reinterpret_cast<char*>(&nmodes), sizeof(nmodes));
        f.write(reinterpret_cast<char*>(dimensions.data()), nmodes * sizeof(guint));
        f.write(reinterpret_cast<char*>(&nnz), sizeof(guint));
        for(guint i = 0; i < nmodes; i++)
            tensors[i].write(f);
    }

    std::vector<gfloat> csf::mttkrp_cpu(guint mode, vector<Matrix::matrix>& factors) {
        if(nmodes == 3)
            return tensors[mode].mttkrp3_cpu(factors);
    }

    darray<gfloat> csf::mttkrp(guint mode, vector<Matrix::matrix>& factors) {
        if(nmodes == 3) 
            return tensors[mode].mttkrp3(factors);
        if(nmodes == 4)
            return tensors[mode].mttkrp4(factors);
    }

    gfloat csf::norm() const {
        return tensors[0].norm();
    }

    std::vector<guint> csf::level_map(guint mode) const { 
        return tensors[mode].level_map();
    }

    std::vector<guint> csf::rootsize() const {
        std::vector<guint> ret;
        for(guint i = 0; i < nmodes; i++){
            guint len = tensors[i].root_idx().size();
            ret.push_back(len);
        }
        return ret;
    }

    void csf::init_tree_offsets(guint F) {
        for(guint m = 0; m < nmodes; m++)
            tensors[m].create_cub_offsets(F);
    }

    std::vector<guint> csf::max_lsize() const {
        std::vector<guint> ret;
        for(guint i = 0; i < nmodes; i++)
            ret.push_back(tensors[i].max_lsize());
        return ret;
    }

    bool operator==(const csf &lhs, const csf &rhs) {
        bool retval = false;
        if(lhs.nmodes != rhs.nmodes || lhs.nnz != rhs.nnz) return retval;
        if(lhs.dimensions != rhs.dimensions) return retval;
        for(guint i = 0; i < lhs.nmodes; i++)
            if(!(lhs.tensors[i] == rhs.tensors[i])) return retval;
        return true;
    }
}
}
