#include <type.h>
#include <matrix.h>
#include <tree.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cassert>
#include <random>
#include <iomanip>

namespace Gist {
namespace Matrix {

    matrix::matrix() : nrows{0}, ncols{0}, values{nullptr} {}

    matrix::matrix(uint64_t nr, uint64_t nc) : nrows{nr}, ncols{nc} {
        values = new gfloat[nr * nc]();
    }

    matrix::matrix(uint64_t nr, uint64_t nc, gfloat v) : matrix(nr, nc) {
        for(uint64_t i = 0; i < nr * nc; i++)
            values[i] = v;
    }

    matrix::matrix(const uint64_t nr, const uint64_t nc, const std::vector<gfloat> &v) : matrix(nr, nc) {
        assert(v.size() >= nr * nc);
        for(uint64_t i = 0; i < nr * nc; i++)
            values[i] = v[i];
    }

    matrix::~matrix() {
        if(values) {
            delete[] values;
            values = nullptr;
        }
        nrows = 0; ncols = 0;
    }

    matrix::matrix(const matrix& rhs) : matrix(rhs.nrows, rhs.ncols) {
        std::memcpy(values, rhs.values, (uint64_t)nrows * (uint64_t)ncols * (uint64_t)(sizeof(gfloat)));
    }

    void matrix::swap(matrix &rhs) {
        guint swap_guint;
        gfloat *swap_double_ptr;
        swap_guint = nrows; nrows = rhs.nrows; rhs.nrows = swap_guint;
        swap_guint = ncols; ncols = rhs.ncols; rhs.ncols = swap_guint;
        swap_double_ptr = values; values = rhs.values; rhs.values = swap_double_ptr;
    }

    matrix::matrix(matrix &&rhs) : matrix() {
        rhs.swap(*this);
    }

    matrix& matrix::operator=(matrix &&rhs) {
        rhs.swap(*this);
        return *this;
    }

    matrix::matrix(std::vector<std::vector<gfloat>> &U) : matrix(U.size(), U[0].size()) {
        for(uint64_t i = 0; i < ncols; i++)
            for(uint64_t j = 0; j < nrows; j++)
                values[i * nrows + j] = U[j][i];
    }

    matrix::matrix(const std::initializer_list<std::initializer_list<gfloat>> il) : 
        matrix(il.size(), il.begin()->size()) {
        uint64_t row_pos = 0, col_pos = 0;
        for(auto row = il.begin(); row != il.end(); row++){
            col_pos = 0;
            for(auto col = row->begin(); col != row->end(); col++){
                values[col_pos * nrows + row_pos] = *col;
                col_pos++;
            }
            row_pos++;
        }
    }

    void matrix::random_init(gfloat a, gfloat b) {
        std::random_device r;
        std::mt19937 gen(r());
        std::uniform_real_distribution<> distr(a, b);
        for(uint64_t i = 0; i < (uint64_t)nrows*(uint64_t)ncols; i++)
            values[i] = distr(gen);
    }

    void matrix::print(std::ostream &f) const {
        std::stringstream ss;
        stringify(ss);
        f << ss.rdbuf();
    }

    void matrix::stringify(std::stringstream &f) const {
        f << std::fixed;
        f << std::setprecision(3);
        f << "Column-major matrix (ncols=" << ncols << ", nrows=" << nrows << ") : \n";
        for(guint i = 0; i < ncols; i++){
            for(guint j = 0; j < nrows; j++)
                f << values[i * nrows + j] << " ";
            f << std::endl;
        }

    }

    bool operator==(const matrix &lhs, const matrix &rhs) {
        bool retval = false;
        if((lhs.nrows == rhs.nrows) && (lhs.ncols == rhs.ncols)) retval = true;
        if(retval)
            for(uint64_t i = 0; i < (uint64_t)lhs.nrows*(uint64_t)lhs.ncols; i++){
                bool is_same = (std::fabs(lhs.values[i] - rhs.values[i]) < 1E-5);
                retval = retval && (is_same);
            }
        return retval;
    }

    void matrix::select_scale(const matrix &in, const Tree::dev_level &pos, const std::vector<gfloat> &scale) {
        assert(pos.lsize == scale.size() && nrows == pos.lsize && ncols == in.ncols);
        uint64_t idx;
        for(uint64_t j = 0; j < ncols; j++)
            for(uint64_t i = 0; i < nrows; i++) {
                idx = pos.lidx[i] - 1;
                values[j * nrows + i] = in.values[j * in.nrows + idx] * scale[i];
            }
    }

    matrix matrix::hadamard_product(
            const std::vector<guint> &row_selector, const matrix &rhs) const {

        assert(nrows == row_selector.size());
        assert(ncols == rhs.ncols);
        matrix ret(nrows, ncols);

        guint idx;
        for(guint j = 0; j < ncols; j++)
            for(guint i = 0; i < nrows; i++){
                idx = row_selector[i] - 1;
                ret.values[j * nrows + i] = values[j * nrows + i] * 
                    rhs.values[j * rhs.nrows + idx];
            }

        return ret;
    }

    //Solve for X : XA = B
    int matrix::lapacke_linsolve(matrix &B) {
        
        int info = 0;
        assert(ncols == B.num_cols());
#ifdef USE_LAPACKE
        lapack_int info, m = ncols, n = nrows;
        lapack_int nrhs = B.num_rows(), lda = ncols;
        lapack_int ldb = B.num_rows(), rank;
        gfloat s[nrows];

        info = LAPACKE_sgelss(LAPACK_ROW_MAJOR, m, n, nrhs, values, lda, 
                B.values, ldb, s, -1, &rank);

        std::cout << "Rank of matrix : " << rank;
        std::cout << " with singular values : ";
        for(guint i = 0; i < nrows; i++)
            std::cout << s[i] << " ";
        std::cout << std::endl;
#endif
        return info;
    }

    matrix operator*(const matrix &lhs, const matrix &rhs) {
        assert(lhs.ncols == rhs.nrows);
        matrix m(lhs.nrows, rhs.ncols);
        memset(m.values, 0, m.nrows * m.ncols);
        for(guint j = 0; j < m.ncols; j++)
            for(guint i = 0; i < m.nrows; i++)
                for(guint k = 0; k < lhs.ncols; k++)
                    m.values[j * m.nrows + i] += lhs.values[k * lhs.nrows + i] * 
                        rhs.values[j * rhs.nrows + k];
        return m;
    }
}
}
