#include <type.h>
#include <constants.h>
#include <matrix.h>
#include <tree.h>
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <cstring>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <random>
#include <cmath>

#ifdef USE_LAPACKE
#include <lapacke.h>
#endif

namespace Gist {
namespace Matrix {

    __device__ void matrix::select_scale(volatile gfloat *__restrict__ shmem, const guint num_elements_per_block, Tree::dev_level const *__restrict__ pos, gfloat const *__restrict__ scale) const {

        guint num_elements = pos->lsize * ncols;
        guint num_elements_per_thread = (num_elements_per_block / blockDim.x) + (num_elements_per_block % blockDim.x ? 1 : 0);
        int res_offset = blockIdx.x * num_elements_per_block, p, i;
        guint row, col, sc_pos;

        for(i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x + (i * blockDim.x); 
            if(p < min(num_elements_per_block, num_elements - res_offset)) {
                sc_pos = (p + res_offset) % pos->lsize;
                col = (p + res_offset) / pos->lsize;
                row = pos->lidx[sc_pos] - 1;
                shmem[p] = values[col * nrows + row] * scale[sc_pos];
            }
        }
    }

    __device__ void matrix::select_scale(Tree::dev_level const *__restrict__ pos, gfloat const *__restrict__ scale, gfloat *vals) const {
        //assume one element per thread
        guint row, col, p, size = pos->lsize;

        #pragma unroll
        for(int i = 0; i < num_elements_per_thread; i++) {
            p = (threadIdx.x + (blockIdx.x * blockDim.x)) * num_elements_per_thread + i;
            col = min(p / size, ncols - 1);
            row = pos->lidx[p % size] - 1;
            vals[i] = values[col * nrows + row] * scale[p % size];
        }
    }

    __device__ void matrix::hadamard(guint const numflags, Tree::dev_level const *__restrict__ l1pos, guint const *__restrict__ block_segments, guint const offset, guint const l2_nsegments, gfloat *vals, guint start) const {
        guint p, i, col, row;
        #pragma unroll
        for(i = 0; i < num_elements_per_thread; i++) {
            //p = (threadIdx.x + (blockIdx.x * blockDim.x)) * num_elements_per_thread + i;
            //p = (threadIdx.x + start) * num_elements_per_thread + i;
            p = start + (threadIdx.x * num_elements_per_thread) + i - 1;
            col = min(p / numflags, ncols - 1);
            //std::printf("p %d col %d block_segments[%d] %d offset %d numflags %d l2_segments %d lidx index %d\n", p, col, i, block_segments[i], offset, numflags, l2_nsegments, block_segments[i] + offset - (p / numflags * l2_nsegments) - 1);
            row = l1pos->lidx[block_segments[i] + offset - (p / numflags * l2_nsegments) - 1] - 1;
            vals[i] *= values[col * nrows + row];
        }
    }

    __device__ void matrix::hadamard(Tree::dev_level const *__restrict__ l2pos, Tree::dev_level const *__restrict__ l1pos, guint const *__restrict__ l2_segments, gfloat &val) const {
        guint p = threadIdx.x + (blockIdx.x * blockDim.x);
        guint numflags = l2pos->lsize;
        guint col = p / numflags, row;
        row = l1pos->lidx[l2_segments[p % numflags] - 1] - 1;
        if(col < ncols && row < nrows) val *= values[col * nrows + row];
    }

    __forceinline__ __device__ unsigned dynamic_smem_size() {
        unsigned ret;
        asm volatile("mov.u32 %0, %dynamic_smem_size;" : "=r"(ret));
        return ret;
    }

    __device__ void matrix::multiply_rows(Tree::dev_level const *__restrict__ rows, gfloat const *__restrict__ vals, const guint out_ncols, gfloat *s) const {

        guint total_nelem = rows->lsize * out_ncols;
        guint shmem = dynamic_smem_size();
        guint block_nelem = shmem / sizeof(gfloat);
        guint thread_nelem = (total_nelem / (block_nelem * blockDim.x)) + (total_nelem % (block_nelem * blockDim.x)) ? 1 : 0;
        
        int tid = threadIdx.x + blockIdx.x * blockDim.x;
        guint row, col, idx;
        for(int i = tid * thread_nelem; i < (tid+1) * thread_nelem && i < total_nelem; i++) {
            row = i % rows->lsize;
            col = i / rows->lsize;
            idx = rows->lidx[row] - 1;
            s[i] = values[col * nrows + idx] * vals[row];
        }
    }

    __device__ void matrix::multiply_rows(Tree::dev_level const *__restrict__ rows, 
            gfloat const *__restrict__ vals, 
            matrix *__restrict__ out) const {

        int i = threadIdx.x;
        int c = blockIdx.x;
        int thread_work = (out->nrows / blockDim.x) + 
            (out->nrows % blockDim.x ? 1 : 0);

        guint idx;
        for(int r = i * thread_work; r < (i+1) * thread_work; r++) {
            if(r < out->nrows) {
                idx = rows->lidx[r] - 1;
                if(idx < nrows)
                    out->values[c * out->nrows + r] = vals[r] * values[c * nrows + idx];
            }
        }
    }

    __device__ void matrix::multiply_rows(guint const *__restrict__ rows, 
            gfloat const *__restrict__ vals, 
            matrix *__restrict__ out) const {
        
        int i = threadIdx.x;
        int c = blockIdx.x;
        int thread_work = (out->nrows / blockDim.x) + 
            (out->nrows % blockDim.x ? 1 : 0);

        guint idx;
        for(int r = i * thread_work; r < (i+1) * thread_work; r++) {
            if(r < out->nrows) {
                idx = rows[r] - 1;
                if(idx < nrows) 
                    out->values[c * out->nrows + r] = vals[r] * values[c * nrows + idx];
            }
        }

        /*
        int c = threadIdx.y + blockDim.y * blockIdx.y;
        int r = threadIdx.x + blockDim.x * blockIdx.x;
        std::printf("Blk: (%d,%d) Thread: (%d,%d) -> Row/Col = (%d,%d)\n", 
                blockIdx.x, blockIdx.y, threadIdx.x, threadIdx.y, i, r);
        guint idx;
        if(c < out->ncols && r < out->nrows){
            idx = rows[r] - 1;
            out->values[c * out->nrows + r] = vals[r] * values[c * nrows + idx];
        }
        */
    }

    __device__ void matrix::divide_cols(matrix *__restrict__ A, 
            gfloat const *__restrict__ s) {

        assert(nrows == A->nrows);
        assert(ncols == A->ncols);
        gfloat tol = 1e-7;
        int i = threadIdx.x;
        int thread_work = (A->nrows * A->ncols / blockDim.x) + 
            ((A->nrows * A->ncols) % blockDim.x ? 1 : 0);

        int col;
        for(int j = i * thread_work; j < (i+1)*thread_work; j++)
            if(j < (A->nrows * A->ncols)) {
                col = j / A->nrows;
                /*
                std::printf("Blk: %d Thread: %d -> j: %d col: %d\n", 
                        blockIdx.x, threadIdx.x, j, col);
                        */
                if(std::abs(s[col]) > tol) values[j] = A->values[j] / s[col];
                else values[j] = 0;
            }
    }

    __device__ void matrix::divide_cols(gfloat const *__restrict__ s) {

        int i = threadIdx.x;
        int c = blockIdx.x;
        int thread_work = (nrows / blockDim.x) + 
            (nrows % blockDim.x ? 1 : 0);

        for(int r = i * thread_work; r < (i+1)*thread_work; r++)
            if(r < nrows) {
                values[c * nrows + r] /= s[c];
            }
    }

    __device__ void matrix::hadamard_product(Tree::dev_level const *__restrict__ rows, matrix const *__restrict__ lhs, matrix const *__restrict__ rhs) {

        int i = threadIdx.x;
        int c = blockIdx.x;
        int thread_work = (nrows / blockDim.x) + (nrows % blockDim.x ? 1 : 0);

        guint idx;
        for(int r = i * thread_work; r < (i+1) * thread_work; r++) {
            if(r < nrows) {
                idx = rows->lidx[r] - 1;
                if(idx < rhs->nrows)
                    values[c * nrows + r] = lhs->values[c * lhs->nrows + r] * 
                        rhs->values[c * rhs->nrows + idx];
            }
        }
    }


    __device__ void matrix::hadamard_product(matrix **__restrict__ mats, const guint n) {

        int i = threadIdx.x + (blockDim.x * blockIdx.x);
        gfloat v;
        if(i < nrows * ncols){
            v = 1.0f;
            for(guint m = 0; m < n; m++){
                v *= mats[m]->values[i];
            }
            values[i] = v;
        }

        /*
        int i = threadIdx.x;
        int c = blockIdx.x;
        int thread_work = (nrows / blockDim.x) + (nrows % blockDim.x ? 1 : 0);

        for(int r = i * thread_work; r < (i+1) * thread_work; r++) {
            if(r < nrows) 
                values[c * nrows + r] *= aTa->values[c * aTa->nrows + r];
        }
        */
    }

    __device__ void matrix::printf() const {
        std::printf("Column major matrix (ncols=%zu, nrows=%zu) : \n", ncols, nrows);
        for(guint i = 0; i < ncols; i++){
            for(guint j = 0; j < nrows; j++)
                std::printf("%.2f ", values[i * nrows + j]);
            std::printf("\n");
        }
    }

}
}
