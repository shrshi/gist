#include <type.h>
#include <tree.h>
#include <coo.h>
#include <utils.h>
#include <constants.h>
#include <cstring>
#include <cassert>
#include <vector>
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <iterator>

namespace Gist {
namespace Tree {

    void tree::swap(tree &other){
        std::swap(depth, other.depth);
        std::swap(level_mode_map, other.level_mode_map);
        std::swap(level_size, other.level_size);
        std::swap(level_ptr, other.level_ptr);
        std::swap(level_flags, other.level_flags);
        std::swap(level_idx, other.level_idx);
        std::swap(values, other.values);
        std::swap(rank, other.rank);
        std::swap(cub_offsets, other.cub_offsets);
    }

    void tree::insert_nodes(guint level, std::vector<guint> &parent_level_marker, guint &nparent_round, const Coo::coo &coo_t) {
        guint mode = level_mode_map[level];
        guint num_nodes = 0, nround = 0; // number of additional nodes added to round up to multiple of num_elements_per_thread
        std::vector<guint> cur_level_marker;
        cur_level_marker.reserve(parent_level_marker.size());

        for(guint i = 0; i < parent_level_marker.size() - 1; i++) 
            for(guint j = parent_level_marker[i]; j < parent_level_marker[i+1]; j++)
                if(j == parent_level_marker[i] || 
                    coo_t.indices[mode][j] != coo_t.indices[mode][j-1]){
                    num_nodes++;
                    cur_level_marker.push_back(j);
                }
        if(level) {
            if(num_nodes & (num_elements_per_thread - 1))
                nround = num_elements_per_thread - (num_nodes & (num_elements_per_thread - 1));
            if(nround < nparent_round) {
                guint k = ((nparent_round - nround) / num_elements_per_thread) + ((nparent_round - nround) % num_elements_per_thread ? 1 : 0);
                nround += k*num_elements_per_thread;
            }
        }
        level_size[level] = num_nodes + nround;
        level_idx[level] = new guint[level_size[level]];

        if(level) {
            level_ptr[level-1] = new guint[level_size[level-1]+1];
            level_ptr[level-1][0] = 0;
        }

        guint current_parent_marker = 1, cur_children_ct = 0;
        for(guint i = 0; i < cur_level_marker.size(); i++) {
            level_idx[level][i] = coo_t.indices[mode][cur_level_marker[i]];
            if(level){
                if(cur_level_marker[i] < parent_level_marker[current_parent_marker])
                    cur_children_ct++;
                else {
                    level_ptr[level-1][current_parent_marker] = 
                        level_ptr[level-1][current_parent_marker-1] + cur_children_ct;
                    cur_children_ct = 1;
                    current_parent_marker++;
                }
            }
        }
        if(level) level_ptr[level-1][current_parent_marker] = 
            level_ptr[level-1][current_parent_marker-1] + cur_children_ct;

        for(guint j = cur_level_marker.size(); j < level_size[level]; j++)
            level_idx[level][j] = 1;
        if(level) {
            current_parent_marker++;
            guint npr = nparent_round;
            while(nparent_round) {
                level_ptr[level-1][current_parent_marker] = level_ptr[level-1][current_parent_marker-1] + 1;
                current_parent_marker++;
                nparent_round--;
            }
            level_ptr[level-1][current_parent_marker - 1] += nround - npr;
        }

        cur_level_marker.push_back(coo_t.nnz);
        parent_level_marker.swap(cur_level_marker);
        cur_level_marker.clear();
        nparent_round = nround;
    }

    tree::tree(const Coo::coo &coo_t, const std::vector<guint> &sort_order) {

        depth = coo_t.num_modes();
        level_mode_map = new guint[depth];
        std::copy(sort_order.begin(), sort_order.end(), level_mode_map);
        
        level_size = new guint[depth];
        level_ptr = new guint*[depth - 1];
        level_flags = new Utils::bitops[depth-1];
        level_idx = new guint*[depth];
        /*
         * need to round nnz to multiple of num_elements_per_thread
         * for values and level_flags[depth-2] and level_idx[depth-1].
         * also rounding level_flags[depth-3] and level_idx[depth-2].
         * Note that level sizes are also being updated
         * TODO: level_ptr is not being updated!
         */
        /*
         * Scrap that, let's just round all levels of the tree except the
         * first, up to a multiple of num_elements_per_thread. The first 
         * level is left out since rank of mttkrp is usually >= 4 anyway
         */
        std::vector<guint> parent_level_marker{0, coo_t.nnz};
        guint level, ptr, nparent_round = 0;
        for(level = 0; level < depth - 1; level++){
            insert_nodes(level, parent_level_marker, nparent_round, coo_t);
        }
        guint nround = 0;
        if(coo_t.nnz & (num_elements_per_thread - 1))
            nround = num_elements_per_thread - (coo_t.nnz & (num_elements_per_thread - 1));
        //if(nround < nparent_round) nround += num_elements_per_thread;
        if(nround < nparent_round) {
            guint k = ((nparent_round - nround) / num_elements_per_thread) + ((nparent_round - nround) % num_elements_per_thread ? 1 : 0);
            nround += k*num_elements_per_thread;
        }
        level_size[level] = coo_t.nnz + nround;

        level_idx[level] = new guint[level_size[level]];
        std::memcpy(level_idx[level], coo_t.indices[level_mode_map[level]], coo_t.nnz * sizeof(*level_idx[level]));
        for(guint i = coo_t.nnz; i < level_size[level]; i++)
            level_idx[level][i] = 1;

        level_ptr[level-1] = new guint[level_size[level-1]+1];
        std::copy(parent_level_marker.begin(), parent_level_marker.end(), level_ptr[level-1]);
        guint current_parent_marker = parent_level_marker.size();
        guint npr = nparent_round;
        while(nparent_round) {
            level_ptr[level-1][current_parent_marker] = level_ptr[level-1][current_parent_marker-1] + 1;
            current_parent_marker++;
            nparent_round--;
        }
        level_ptr[level-1][current_parent_marker - 1] += nround - npr;

        values = new gfloat[level_size[level]]();
        std::memcpy(values, coo_t.values, coo_t.nnz * sizeof(*values));

        for(level = 0; level < depth - 1; level++){
            level_flags[level].resize(level_size[level+1]);
            for(ptr = 0; ptr < level_size[level]; ptr++)
                level_flags[level].set(level_ptr[level][ptr]);
        }
        assert(level_flags != nullptr);
    }

    void tree::create_cub_offsets(guint matrix_ncols) {
        rank = matrix_ncols;
        cub_offsets = new guint*[depth - 1];
        for(guint level = depth - 1; level > 0; level--) {
            guint nsegments = level_size[level-1];
            cub_offsets[depth - level - 1] = new guint[matrix_ncols * nsegments + 1];
            guint start_offset = 0, offsets_size = nsegments + 1, j=0, pos=0;
            for(guint i = 0; i < matrix_ncols; i++) {
                for(; j < offsets_size; j++, pos++)
                    cub_offsets[depth-level-1][pos] = level_ptr[level-1][j] + start_offset;
                start_offset = cub_offsets[depth-level-1][pos-1];
                j = 1;
            }
        }
    }


    tree::tree(std::ifstream &f, guint nmodes) {
        depth = nmodes;
        level_mode_map = new guint[depth];
        level_size = new guint[depth];
        level_idx = new guint*[depth];
        level_ptr = new guint*[depth-1];
        level_flags = new Utils::bitops[depth-1];

        f.read(reinterpret_cast<char*>(level_mode_map), depth * sizeof(guint));
        f.read(reinterpret_cast<char*>(level_size), depth * sizeof(guint));
        for(guint i = 0; i < depth; i++) {
            level_idx[i] = new guint[level_size[i]];
            f.read(reinterpret_cast<char*>(level_idx[i]), level_size[i] * sizeof(guint));
            if(i < depth - 1){
                //TODO: read the flags instead AFTER write function is modified
                level_ptr[i] = new guint[level_size[i] + 1];
                f.read(reinterpret_cast<char*>(level_ptr[i]), (level_size[i] + 1) * sizeof(guint));
            }
            else{
                values = new gfloat[level_size[i]];
                f.read(reinterpret_cast<char*>(values), level_size[i] * sizeof(gfloat));
            }
        }

        level_flags = new Utils::bitops[depth-1];
        for(guint level = 0; level < depth - 1; level++){
            level_flags[level].resize(level_size[level+1]);
            for(guint ptr = 0; ptr < level_size[level]; ptr++)
                level_flags[level].set(level_ptr[level][ptr]);
        }
        assert(level_flags != nullptr);
    }

    tree::~tree() {
        if(level_mode_map) delete[] level_mode_map;
        if(level_size) delete[] level_size;
        if(level_idx) {
            for(guint i = 0; i < depth; i++)
                delete[] level_idx[i];
            delete[] level_idx;
        }
        if(level_ptr) {
            for(guint i = 0; i < depth-1; i++)
                delete[] level_ptr[i];
            delete[] level_ptr;
        }
        if(values) delete[] values;
        if(cub_offsets) {
            for(guint i = 0; i < depth - 1; i++)
                delete[] cub_offsets[i];
            delete[] cub_offsets;
        }
        if(level_flags)
            delete[] level_flags;
    }

    void tree::print(std::ostream &f) const {
        std::stringstream ss;
        stringify(ss);
        f << ss.rdbuf();
    }

    void tree::stringify(std::stringstream &f) const { 
        f << std::fixed;
        f << std::setprecision(3);
        for(guint m = 0; m < depth; m++){
            f << "Level " << m << " idx : ";
            f << "Mode : " << level_mode_map[m] << std::endl;
            for(guint i = 0; i < level_size[m]; i++)
                f << level_idx[m][i] << " ";
            f << std::endl;
            if(m < depth-1){
                f << "Level " << m << " ptr : ";
                for(guint i = 0; i < level_size[m] + 1; i++)
                    f << level_ptr[m][i] << " ";
                f << std::endl;
                f << "Level " << m << " flags : ";
                level_flags[m].print(f);
            }
        }
        f << "Values of size " << level_size[depth-1] << " : ";
        for(guint i = 0; i < level_size[depth-1]; i++)
            f << values[i] << " ";
        f << std::endl;
    }

    void tree::write(std::ofstream &f) { 
        f.write(reinterpret_cast<char*>(level_mode_map), depth * sizeof(guint));
        f.write(reinterpret_cast<char*>(level_size), depth * sizeof(guint));
        for(guint i = 0; i < depth; i++){
            f.write(reinterpret_cast<char*>(level_idx[i]), level_size[i] * sizeof(guint));
            if(i < depth - 1){
                //TODO: write the flags instead - better compression
                f.write(reinterpret_cast<char*>(level_ptr[i]), (level_size[i] + 1) * sizeof(guint));
            }
            else
                f.write(reinterpret_cast<char*>(values), level_size[i] * sizeof(gfloat));
        }
    }

    std::vector<guint> tree::root_idx() const {
        return std::vector<guint>(level_idx[0], level_idx[0] + level_size[0]);
    }

    std::vector<guint> tree::level_map() const {
        return std::vector<guint>(level_mode_map, level_mode_map + depth);
    }

    guint tree::max_lsize() const {
        return level_size[depth-1];
    }

    gfloat tree::norm() const {
        gfloat ret = 0.0;
        for(guint i = 0; i < level_size[depth-1]; i++)
            ret += values[i] * values[i];
        return std::sqrt(ret);
    }

    dev_level::dev_level(const tree &t, guint l) {
        assert(l < t.depth);
        lsize = t.level_size[l];
        lidx = t.level_idx[l];
    }

    dev_level::~dev_level() {
        lsize = 0;
        lidx = nullptr;
    }

    dev_level::dev_level(guint s, guint *li) : lsize(s), lidx(li) {}

    bool operator==(const tree &lhs, const tree &rhs) { 
        auto cmplambda = [](auto *a, auto *b, guint len) {
            for(guint i = 0; i < len; i++)
               if(a[i] != b[i]) return false;
            return true; 
        };
        bool retval = false;
        if(lhs.depth != rhs.depth) return retval;
        if(!cmplambda(lhs.level_mode_map, rhs.level_mode_map, lhs.depth)) return retval;
        if(!cmplambda(lhs.level_size, rhs.level_size, lhs.depth)) return retval;
        for(guint i = 0; i < lhs.depth; i++){
            if(!cmplambda(lhs.level_idx[i], rhs.level_idx[i], lhs.level_size[i])) return retval;
            if(i < lhs.depth - 1 && !cmplambda(lhs.level_ptr[i], rhs.level_ptr[i], lhs.level_size[i]+1)) return retval;
        }
        if(!cmplambda(lhs.values, rhs.values, lhs.level_size[lhs.depth-1])) return retval;
        return true;
    }
}
}
