#include <type.h>
#include <utils.h>
#include <csf.h>
#include <matrix.h>
#include <device.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <vector>
#include <cstring>
#include <algorithm>
#include <ostream>
#include <cassert>

namespace Gist {
namespace Utils {

    void permute(std::vector<Matrix::matrix> &mats, const std::vector<guint> order, bool rev) {
        guint len = order.size();
        std::vector<guint> visited(len, 0);

        if(!rev) {
            guint cur;
            for(guint i = 0; i < len; i++) {
                cur = i;
                while(!visited[cur] && order[cur] != i) {
                    visited[cur] = 1;
                    mats[cur].swap(mats[order[cur]]);
                    cur = order[cur];
                }
                visited[cur] = 1;
            }
        }
        else {
            guint cur;
            for(guint i = len - 1; i < len; i--) {
                cur = i;
                while(!visited[cur] && order[cur] != i) {
                    visited[cur] = 1;
                    mats[i].swap(mats[order[cur]]);
                    cur = order[cur];
                }
                visited[cur] = 1;
            }
        }
    }

    guint max_element(std::vector<guint> &&arr) {
        guint ret = arr[0];
        for(guint i = 1; i < arr.size(); i++)
            ret = std::max(ret, arr[i]);
        return ret;
    }

    guint max_element(const std::vector<Matrix::matrix> &arr, const guint except) {
        guint ret = 0;
        for(guint i = 0; i < arr.size(); i++)
            if(i != except) ret = std::max(ret, arr[i].num_rows());
        return ret;
    }

    bitops::bitops() {}

    bitops::bitops(guint nb) {
        size = (nb / width) + ((nb & (width - 1)) ? 1 : 0);
        nbits = nb;
        buf = new guint[size];
        for(guint i = 0; i < size; i++)
            buf[i] = 0;
    }

    bitops::~bitops() {
        delete[] buf;
        buf = nullptr;
        nbits = 0;
        size = 0;
    }

    void bitops::resize(guint nb) {
        size = (nb / width) + ((nb & (width - 1)) ? 1 : 0);
        nbits = nb;
        if(buf) delete[] buf;
        buf = new guint[size];
        for(guint i = 0; i < size; i++)
            buf[i] = 0;
    }

    __host__ __device__ void bitops::set(guint pos) {
        //assert(pos < nbits);
        if(pos < nbits) buf[pos / width] |= 1 << (pos & (width - 1));
    }

    __host__ __device__ void bitops::unset(guint pos) {
        //assert(pos < nbits);
        if(pos < nbits) buf[pos / width] &= ~(1 << (pos & (width - 1)));
    }

    __host__ __device__ void bitops::toggle(guint pos) {
        //assert(pos < nbits);
        if(pos < nbits) buf[pos / width] ^= 1 << (pos & (width - 1));
    }

    __host__ __device__ bool bitops::test(guint pos) const {
        //if(pos >= nbits) std::printf("pos = %d, nbits = %d\n", pos, nbits);
        //assert(pos < nbits);
        if(pos < nbits && buf[pos / width] & (1 << (pos & (width - 1))))
            return true;
        return false;
    }

    bool wordtest(guint &a, const guint &pos) {
        if(a & (1 << (pos % guint_typewidth)))
            return true;
        return false;
    }

    /* 
       * Retrieves n elements starting at (and including) startpos.
       * Assumes n < guint_typewidth
       * If startpos > nbits, it wraps around buffer
       * If startpos + n > nbits, it wraps around buffer
       */
    __host__ __device__ void bitops::retrieve(guint startpos, guint n_, guint &ret) const {
        guint n = n_;
        startpos = startpos % nbits;
        guint pos = startpos / width, t1, w, j, retrieved = 0, t;
        if(pos < size - 1) w = width;
        else w = nbits % width ? nbits % width : width;
        startpos = startpos & (width - 1); //startpos = startpos % width;

        ret = buf[pos] >> startpos;
        w = w - startpos;
        t = ~0 >> (width-w);
        if(n <= w) t = t >> (w-n);
        ret = ret & t;
        retrieved += n < w ? n : w;
        n = n - w;

        pos++;
        while(n && n < width) {
            pos = pos % size;
            if(pos < size - 1) w = width;
            else w = nbits % width ? nbits % width : width;

            t = ~0 >> (width-w);
            if(n <= w) t = t >> (w-n);
            t = buf[pos] & t; 
            t = t << retrieved;
            ret = t | ret;

            retrieved += n < w ? n : w;
            n = n - w; 
            pos++;
        }
        assert(n_ == retrieved);
        /*
        if(n) {
            t1 = buf[(pos + 1) % size];
            t1 = t1 << (w - startpos);
            ret = ret | t1;
            n = n - (w - startpos);
            w = (pos + 1) % size < size - 1 ? width : nbits % width;
            n = n - w;
        }
        if(n && n < width) {
            t1 = buf[(pos + 2) % size];
            t1 = t1 << (width - startpos) + (nbits % width);
            ret = ret | t1;
        }
        */
    }

    void bitops::multiretrieve(guint startpos, guint n, guint &ret, guint &ret2) const {
        guint pos, t1, w, j, retrieved = 0;

        n += 1;
        startpos = startpos % nbits;
        ret = ~0;
        pos = startpos / width;
        w = pos < size - 1 ? width : nbits % width;
        startpos = startpos & (width - 1);

        t1 = buf[pos];
        if(n <= w - startpos) {
            ret = t1 & (ret >> (w - n - startpos));
            n = 0;
        }
        else {
            ret = t1;
            n = n - (w - startpos);
            retrieved += w - startpos;
        }
        ret = ret >> startpos;

        /*
        for(int i = 0; i < guint_typewidth; i++)
            std::printf("%d ", wordtest(ret, i));
        std::printf("\n");
        */
        j = 1;
        while(n && n < width) {
            t1 = buf[(pos + j) % size];
            t1 = t1 << retrieved;
            ret = ret | t1;
            w = (pos + j) % size < size - 1 ? width : nbits % width;
            n = n - w;
            retrieved += w;
            j++;
        }
        ret2 = ret >> 1;
        /*
        for(int i = 0; i < guint_typewidth; i++)
            std::printf("%d ", wordtest(ret2, i));
        std::printf("\n");
        */
    }

    void bitops::print(std::ostream &f) const {
        f << "Bitops vector of size " << nbits << " : ";
        f.flush();
        for(guint i = 0; i < nbits; i++)
            if(test(i)) f << "1 ";
            else f << "0 ";
        f << std::endl;
    }

    void bitops::printf() const {
        std::printf("Bitops vector of size %d : ", nbits);
        for(guint i = 0; i < nbits; i++)
            if(test(i)) std::printf("1 ");
            else std::printf("0 ");
        std::printf("\n");
    }

    //TODO: can do better than bit-wise copy
    void bitops::copy(const bitops &in, guint bit_pos) {
        assert(nbits >= bit_pos + in.nbits);
        if(!bit_pos) std::memcpy(buf, in.buf, in.size * sizeof(guint));
        else
            for(guint i = bit_pos; i < bit_pos + in.nbits; i++)
                if(in.test(i - bit_pos))
                    set(i);
    }

    __device__ void bitops::subseq(guint start, guint end, guint *outbuf, bitops *out) const {
        if(threadIdx.x == 0) {
            out->nbits = end - start;
            out->size = (out->nbits / width) + (out->nbits & (width - 1)) ? 1 : 0;
            out->buf = outbuf;
            std::printf("start %d, end %d", start, end);
        }
        guint nb = end - start;
        guint s = (nb / width) + (nb & (width - 1) ? 1 : 0);
        if(threadIdx.x == 0) std::printf(" size %d\n", s);
        guint num_elements_per_thread = (s / blockDim.x) + (s % blockDim.x ? 1 : 0);
        guint p;
        for(int i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x + (i * blockDim.x); 
            if(p < s) outbuf[p] = 0;
        }
        __syncthreads();
        num_elements_per_thread = (nb / blockDim.x) + (nb % blockDim.x ? 1 : 0);
        for(int i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x + (i * blockDim.x); 
            //if(p < nb && test(start + p)) out->set(p);
            if(p < nb) {
                if(test(start + p)) out->set(p);
                std::printf("thd %d flags[%d] %d out->flags[%d] %d\n", threadIdx.x, start + p, test(start+p), p, out->test(p)); 
            }
        }

        __syncthreads();
        for(int i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x + (i * blockDim.x); 
            if(p < nb) 
                std::printf("Utils : Blk %d Thd %d flags[%d] %d\n", blockIdx.x, threadIdx.x, p, (out->test(p) ? 1 : 0));
        }
    }

    __device__ void bitops::subseq(guint start, guint end, volatile guint *outbuf) const {
        guint s = end - start;
        guint num_elements_per_thread = (s / blockDim.x) + (s % blockDim.x ? 1 : 0);
        guint p;
        for(int i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x + (i * blockDim.x); 
            if(p < s) outbuf[p] = 0;
        }
        __syncthreads();
        for(int i = 0; i < num_elements_per_thread; i++) {
            p = threadIdx.x + (i * blockDim.x); 
            if(p < s && test(start + p)) outbuf[p] = 1;
        }
    }

    std::vector<guint> bitops::toarray() const {
        std::vector<guint> ret(nbits, 0);
        for(int i = 0; i < nbits; i++)
            if(test(i)) ret[i] = 1;
        return ret;
    }
}
}
