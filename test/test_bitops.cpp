#include <type.h>
#include <utils.h>
#include <catch.hpp>
#include <iostream>
#include <vector>

using namespace Gist;
using namespace Gist::Utils;

bool wordtest(guint &a, const guint &pos) {
    if(a & (1 << (pos % guint_typewidth)))
        return true;
    return false;
}

TEST_CASE("Element ops") {

    size_t nbits = 100;
    bitops bitvec(nbits);
    
    SECTION("Set flag") {
        bitvec.set(50);
        REQUIRE(bitvec.test(50));
    }
    SECTION("Unset flag") {
        bitvec.unset(49);
        REQUIRE(bitvec.test(49) == false);
        bitvec.unset(50);
        REQUIRE(bitvec.test(50) == false);
    }
    SECTION("Toggle flag") {
        bitvec.toggle(5);
        REQUIRE(bitvec.test(5));
        bitvec.toggle(5);
        REQUIRE(bitvec.test(5) == false);
    }
    SECTION("Resize") {
        REQUIRE(!bitvec.test(50));
        bitvec.resize(10);
        REQUIRE(!bitvec.test(2));
    }
    SECTION("To array") {
        bitvec.print(std::cout);
        std::vector<guint> bitarr = bitvec.toarray();
        for(guint i = 0; i < bitarr.size(); i++)
            std::cout << bitarr[i] << " ";
        std::cout << std::endl;
    }
}

TEST_CASE("Word ops - basic") {
    size_t nbits = 32;
    bitops bitvec(nbits);
    for(int i = 5; i <= nbits; i += 5)
        bitvec.set(i-1); 
    guint startpos, n, ret;

    SECTION("Retrieve") {
        startpos = 0;
        n = 15;
        bitvec.retrieve(startpos, n, ret);
    }
    for(int i = startpos; i < startpos + n; i++)
        REQUIRE(wordtest(ret, i - startpos) == bitvec.test(i % bitvec.num_bits()));
}

TEST_CASE("Word ops") {
    size_t nbits = 100;
    bitops bitvec(nbits);
    for(int i = 5; i <= nbits; i += 5)
        bitvec.set(i-1); 
    guint startpos, n, ret;

    SECTION("Retrieve") {
        startpos = 0;
        n = 15;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 9;
        n = 15;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 23;
        n = 9;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 30;
        n = 8;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 32;
        n = 8;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 43;
        n = 31;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 94;
        n = 12;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 97;
        n = 12;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 143;
        n = 17;
        bitvec.retrieve(startpos, n, ret);
    }
    /*
    for(int i = 0; i < guint_typewidth; i++)
        std::printf("%d ", wordtest(ret, i));
    std::printf("\n");
    */
    for(int i = startpos; i < startpos + n; i++)
        REQUIRE(wordtest(ret, i - startpos) == bitvec.test(i % bitvec.num_bits()));
}

TEST_CASE("Word ops1") {
    size_t nbits = 6;
    bitops bitvec(nbits);
    bitvec.set(0); 
    bitvec.set(1); 
    bitvec.set(3); 
    bitvec.set(5); 
    guint startpos, n, ret;

    SECTION("Retrieve") {
        startpos = 1;
        n = 8;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 9;
        n = 8;
        bitvec.retrieve(startpos, n, ret);
    }
    SECTION("Retrieve") {
        startpos = 17;
        n = 8;
        bitvec.retrieve(startpos, n, ret);
    }
    /*
    for(int i = 0; i < guint_typewidth; i++)
        std::printf("%d ", wordtest(ret, i));
    std::printf("\n");
    */
    for(int i = startpos; i < startpos + n; i++)
        REQUIRE(wordtest(ret, i - startpos) == bitvec.test(i % bitvec.num_bits()));
}

TEST_CASE("multi word ops") {
    size_t nbits = 100;
    bitops bitvec(nbits);
    for(int i = 5; i <= nbits; i += 5)
        bitvec.set(i-1); 
    guint startpos, n, ret1, ret2;

    SECTION("Retrieve") {
        startpos = 0;
        n = 15;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    SECTION("Retrieve") {
        startpos = 9;
        n = 1;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    SECTION("Retrieve") {
        startpos = 23;
        n = 9;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    SECTION("Retrieve") {
        startpos = 30;
        n = 8;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    SECTION("Retrieve") {
        startpos = 43;
        n = 31;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    SECTION("Retrieve") {
        startpos = 94;
        n = 12;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    SECTION("Retrieve") {
        startpos = 97;
        n = 12;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    SECTION("Retrieve") {
        startpos = 143;
        n = 17;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    /*
    for(int i = 0; i < guint_typewidth; i++)
        std::printf("%d ", wordtest(ret, i));
    std::printf("\n");
    */
    for(int i = startpos; i < startpos + n; i++)
        REQUIRE(wordtest(ret1, i - startpos) == bitvec.test(i % bitvec.num_bits()));
    for(int i = startpos + 1; i < startpos + n + 1; i++)
        REQUIRE(wordtest(ret2, i - startpos - 1) == bitvec.test(i % bitvec.num_bits()));
}

TEST_CASE("multi word ops1") {
    size_t nbits = 6;
    bitops bitvec(nbits);
    bitvec.set(0); 
    bitvec.set(1); 
    bitvec.set(3); 
    bitvec.set(5); 
    guint startpos, n, ret1, ret2;

    SECTION("Retrieve") {
        startpos = 1;
        n = 8;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    SECTION("Retrieve") {
        startpos = 9;
        n = 8;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    SECTION("Retrieve") {
        startpos = 17;
        n = 8;
        bitvec.multiretrieve(startpos, n, ret1, ret2);
    }
    /*
    for(int i = 0; i < guint_typewidth; i++)
        std::printf("%d ", wordtest(ret, i));
    std::printf("\n");
    */
    for(int i = startpos; i < startpos + n; i++)
        REQUIRE(wordtest(ret1, i - startpos) == bitvec.test(i % bitvec.num_bits()));
    for(int i = startpos + 1; i < startpos + n + 1; i++)
        REQUIRE(wordtest(ret2, i - startpos - 1) == bitvec.test(i % bitvec.num_bits()));
}
