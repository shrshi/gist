#include <type.h>
#include <coo.h>
#include <catch.hpp>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using namespace Gist;

TEST_CASE("Constructing COO tensor from file") {

    std::string path = "../../tensors/3d_8.tns";
    std::ifstream f(path);
    Coo::coo X_coo(f);

    X_coo.print(std::cout);
}

TEST_CASE("Sorting COO tensor") {
    std::string path = "../../tensors/3d_8.tns";
    std::ifstream f(path);
    Coo::coo X_coo(f);

    std::vector<guint> sort_order{1, 0, 2};
    REQUIRE(sort_order.size() == X_coo.num_modes());
    X_coo.sort(sort_order);
    X_coo.print(std::cout);
}

