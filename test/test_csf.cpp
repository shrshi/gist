#include <type.h>
#include <constants.h>
#include <csf.h>
#include <coo.h>
#include <tree.h>
#include <utils.h>
#include <matrix.h>
#include <device.h>
#include <catch.hpp>
#include <fstream>
#include <vector>
#include <iostream>
#include <sstream>
#include <random>
#include <cmath>
#include <limits>

using namespace Gist;
using Matrix::matrix;
using Csf::csf;

Coo::coo setup(std::string &path, std::vector<matrix> &matrices, guint R) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 0.5);

    std::ifstream f(path);
    Coo::coo X_coo(f);
    guint nmodes = X_coo.num_modes();
    std::vector<guint> dimensions = X_coo.dims();
    matrices.reserve(nmodes);
    for(guint i = 0; i < nmodes; i++) {
        guint nrows = dimensions[i], ncols = R;
        std::vector<std::vector<gfloat>> m(nrows, std::vector<gfloat>(ncols, 0));
        for(guint i = 0; i < nrows; i++)
            for(guint j = 0; j < ncols; j++)
                m[i][j] = dis(gen);
                //m[i][j] = i + j + 1;
                //m[i][j] = i + 1;
                //m[i][j] = (i + j + 1) * 0.01;
                //m[i][j] = (i + 1) * 0.001;
        matrices.push_back(matrix(m));
        //std::cout << "Matrix in mode " << i << std::endl;
        //matrices.back().print(std::cout);
    }
    return X_coo;
}

bool nearlyEqual(const gfloat &a, const gfloat &b, const gfloat &eps) {
    gfloat absA = abs(a);
    gfloat absB = abs(b);
    gfloat diff = abs(a - b);

    if(a == b) return true;
    else if (a == 0 || b == 0 || (absA + absB < std::numeric_limits<float>::min()))
        return diff < (eps * std::numeric_limits<float>::min());
    else
        return (diff / (std::min(absA + absB, std::numeric_limits<float>::max()))) < eps;
    return false;
}

TEST_CASE("Constructing CSF tree (non-root level rounding)") {
    
    SECTION("no rounding") {
        std::string path = "../../tensors/3d_8.tns";
        std::ifstream fi(path);
        Coo::coo X_coo(fi);

        csf X_csf(X_coo);
        X_csf.print(std::cout);
    }
    SECTION("rounding up") {
        std::string path = "../../tensors/3d_10.tns";
        std::ifstream fi(path);
        Coo::coo X_coo(fi);

        csf X_csf(X_coo);
        X_csf.print(std::cout);
    }
    SECTION("rounding up") {
        std::string path = "../../tensors/3d_7.tns";
        std::ifstream fi(path);
        Coo::coo X_coo(fi);

        csf X_csf(X_coo);
        X_csf.print(std::cout);
    }
}

TEST_CASE("Converting CSF tree") {
    
    std::string path = "../../tensors/3d_8.tns";
    std::ifstream fi(path);
    Coo::coo X_coo(fi);
    fi.close();
    fi.clear();

    csf X_csf(X_coo);
    path = "../../tensors/3d_8.bin";
    std::ofstream fo(path);
    X_csf.write(fo);
    fo.close();
    fo.clear();

    fi.open(path);
    csf X_csf_(fi);
    REQUIRE(X_csf_ == X_csf);
}

TEST_CASE("MTTKRP3 operation - mode 0 (3d) - deli") {
    std::vector<matrix> matrices;
    guint R;
    std::string path;
    guint mode = 0;

    path = "/storage/home/hcoda1/8/sshivakumar9/p-saluru8-0/tensors/delicious-3d.tns";
    R = 32;
    std::cout << path << " : ";

    Coo::coo X_coo = setup(path, matrices, R);
    csf X_csf(X_coo);

    //X_coo.sort(X_csf.level_map(mode));
    Device::array<gfloat> out = X_csf.mttkrp(mode, matrices);
    std::vector<gfloat> csf_gpu = out.retrieve();
    //std::vector<gfloat> coo_out = X_coo.mttkrp(mode, matrices);
    std::vector<gfloat> csf_cpu = X_csf.mttkrp_cpu(mode, matrices);

    REQUIRE(csf_cpu.size() == csf_gpu.size());
    guint count = 0;
    for(guint i = 0; i < csf_gpu.size(); i++) {
        if(!nearlyEqual(csf_cpu[i], csf_gpu[i], 1.e-4) && count < 10) {
            std::printf("csf_cpu[%d] = %.10f, csf_gpu[%d] = %.10f\n", i, csf_cpu[i], i, csf_gpu[i]);
            count++;
        }
        else if(count >= 10) {
            std::cout << "And many more are not equal!\n";
            break;
        }
    }
}

TEST_CASE("MTTKRP3 operation - mode 0 (3d) - NELL1") {
    std::vector<matrix> matrices;
    guint R;
    std::string path;
    guint mode = 0;

    path = "/storage/home/hcoda1/8/sshivakumar9/p-saluru8-0/tensors/nell-1.tns";
    R = 32;
    std::cout << path << " : ";

    Coo::coo X_coo = setup(path, matrices, R);
    csf X_csf(X_coo);

    //X_coo.sort(X_csf.level_map(mode));
    Device::array<gfloat> out = X_csf.mttkrp(mode, matrices);
    std::vector<gfloat> csf_gpu = out.retrieve();
    //std::vector<gfloat> coo_out = X_coo.mttkrp(mode, matrices);
    std::vector<gfloat> csf_cpu = X_csf.mttkrp_cpu(mode, matrices);

    REQUIRE(csf_cpu.size() == csf_gpu.size());
    guint count = 0;
    for(guint i = 0; i < csf_gpu.size(); i++) {
        if(!nearlyEqual(csf_cpu[i], csf_gpu[i], 1.e-4) && count < 10) {
            std::printf("csf_cpu[%d] = %.10f, csf_gpu[%d] = %.10f\n", i, csf_cpu[i], i, csf_gpu[i]);
            count++;
        }
        else if(count >= 10) {
            std::cout << "And many more are not equal!\n";
            break;
        }
    }
}

TEST_CASE("MTTKRP3 operation - mode 0 (3d) - NELL2") {
    std::vector<matrix> matrices;
    guint R;
    std::string path;
    guint mode = 0;

    path = "/storage/home/hcoda1/8/sshivakumar9/p-saluru8-0/tensors/nell-2.tns";
    R = 32;
    std::cout << path << " : ";

    Coo::coo X_coo = setup(path, matrices, R);
    csf X_csf(X_coo);

    //X_coo.sort(X_csf.level_map(mode));
    Device::array<gfloat> out = X_csf.mttkrp(mode, matrices);
    std::vector<gfloat> csf_gpu = out.retrieve();
    //std::vector<gfloat> coo_out = X_coo.mttkrp(mode, matrices);
    std::vector<gfloat> csf_cpu = X_csf.mttkrp_cpu(mode, matrices);

    REQUIRE(csf_cpu.size() == csf_gpu.size());
    for(guint i = 0; i < csf_gpu.size(); i++) {
        if(!nearlyEqual(csf_cpu[i], csf_gpu[i], 1.e-4))
            std::printf("csf_cpu[%d] = %.10f, csf_gpu[%d] = %.10f\n", i, csf_cpu[i], i, csf_gpu[i]);
    }
}

TEST_CASE("MTTKRP3 operation - mode 0 (3d) - DARPA") {
    std::vector<matrix> matrices;
    guint R;
    std::string path;
    guint mode = 0;

    path = "/storage/home/hcoda1/8/sshivakumar9/p-saluru8-0/tensors/1998DARPA/1998DARPA.tensor";
    R = 32;
    std::cout << path << " : ";

    Coo::coo X_coo = setup(path, matrices, R);
    csf X_csf(X_coo);

    //X_coo.sort(X_csf.level_map(mode));
    Device::array<gfloat> out = X_csf.mttkrp(mode, matrices);
    std::vector<gfloat> csf_gpu = out.retrieve();
    //std::vector<gfloat> coo_out = X_coo.mttkrp(mode, matrices);
    std::vector<gfloat> csf_cpu = X_csf.mttkrp_cpu(mode, matrices);

    REQUIRE(csf_cpu.size() == csf_gpu.size());
    for(guint i = 0; i < csf_gpu.size(); i++) {
        if(!nearlyEqual(csf_cpu[i], csf_gpu[i], 1.e-4))
            std::printf("csf_cpu[%d] = %.10f, csf_gpu[%d] = %.10f\n", i, csf_cpu[i], i, csf_gpu[i]);
    }
}

TEST_CASE("MTTKRP3 operation - mode 0 (3d) - fr-m") {
    std::vector<matrix> matrices;
    guint R;
    std::string path;
    guint mode = 0;

    path = "/storage/home/hcoda1/8/sshivakumar9/p-saluru8-0/tensors/freebase_music/freebase_music.tensor";
    R = 32;
    std::cout << path << " : ";

    Coo::coo X_coo = setup(path, matrices, R);
    csf X_csf(X_coo);

    //X_coo.sort(X_csf.level_map(mode));
    Device::array<gfloat> out = X_csf.mttkrp(mode, matrices);
    std::vector<gfloat> csf_gpu = out.retrieve();
    //std::vector<gfloat> coo_out = X_coo.mttkrp(mode, matrices);
    std::vector<gfloat> csf_cpu = X_csf.mttkrp_cpu(mode, matrices);

    REQUIRE(csf_cpu.size() == csf_gpu.size());
    for(guint i = 0; i < csf_gpu.size(); i++) {
        if(!nearlyEqual(csf_cpu[i], csf_gpu[i], 1.e-4))
            std::printf("csf_cpu[%d] = %.10f, csf_gpu[%d] = %.10f\n", i, csf_cpu[i], i, csf_gpu[i]);
    }
}

TEST_CASE("MTTKRP3 operation - mode 0 (3d) - flickr") {
    std::vector<matrix> matrices;
    guint R;
    std::string path;
    guint mode = 0;

    path = "/storage/home/hcoda1/8/sshivakumar9/p-saluru8-0/tensors/flickr-3d.tns";
    R = 32;
    std::cout << path << " : ";

    Coo::coo X_coo = setup(path, matrices, R);
    csf X_csf(X_coo);

    //X_coo.sort(X_csf.level_map(mode));
    Device::array<gfloat> out = X_csf.mttkrp(mode, matrices);
    std::vector<gfloat> csf_gpu = out.retrieve();
    //std::vector<gfloat> coo_out = X_coo.mttkrp(mode, matrices);
    std::vector<gfloat> csf_cpu = X_csf.mttkrp_cpu(mode, matrices);

    REQUIRE(csf_cpu.size() == csf_gpu.size());
    for(guint i = 0; i < csf_gpu.size(); i++) {
        if(!nearlyEqual(csf_cpu[i], csf_gpu[i], 1.e-4))
            std::printf("csf_cpu[%d] = %.10f, csf_gpu[%d] = %.10f\n", i, csf_cpu[i], i, csf_gpu[i]);
    }
}

TEST_CASE("MTTKRP3 operation - mode 0 (3d) - fr-s") {
    std::vector<matrix> matrices;
    guint R;
    std::string path;
    guint mode = 0;

    path = "/storage/home/hcoda1/8/sshivakumar9/p-saluru8-0/tensors/freebase_sampled/freebase_sampled.tensor";
    R = 32;
    std::cout << path << " : ";

    Coo::coo X_coo = setup(path, matrices, R);
    csf X_csf(X_coo);

    //X_coo.sort(X_csf.level_map(mode));
    Device::array<gfloat> out = X_csf.mttkrp(mode, matrices);
    std::vector<gfloat> csf_gpu = out.retrieve();
    //std::vector<gfloat> coo_out = X_coo.mttkrp(mode, matrices);
    std::vector<gfloat> csf_cpu = X_csf.mttkrp_cpu(mode, matrices);

    REQUIRE(csf_cpu.size() == csf_gpu.size());
    guint count = 0;
    for(guint i = 0; i < csf_gpu.size(); i++) {
        if(!nearlyEqual(csf_cpu[i], csf_gpu[i], 1.e-4) && count < 10) {
            std::printf("csf_cpu[%d] = %.10f, csf_gpu[%d] = %.10f\n", i, csf_cpu[i], i, csf_gpu[i]);
            count++;
        }
        else if(count >= 10) {
            std::cout << "And many more are not equal!\n";
            break;
        }
    }
}

TEST_CASE("MTTKRP3 operation - mode 0 (3d)") {

    std::vector<matrix> matrices;
    guint R;
    std::string path;
    guint mode = 0;

    if(num_elements_per_thread >= 2) {
        SECTION("single full warp") {
            path = "/storage/home/hcoda1/8/sshivakumar9/gist/tensors/3d_8.tns";
            R = 10;
            std::cout << path << " : ";
        }
        SECTION("single incomplete warp") {
            path = "../../tensors/3d_8.tns";
            R = 16;
            std::cout << path << " : ";
        }
        SECTION("multiple warp - l1 spans across warps") {
            path = "../../tensors/3d_10.tns";
            R = 16;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_10.tns";
            R = 28;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/small3.tns";
            R = 32;
            std::cout << path << " : ";
        }
        SECTION("single full warp") {
            path = "../../tensors/small3_.tns";
            R = 32;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_128.tns";
            R = 731;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/med3.tns";
            R = 32;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_39.tns";
            R = 17;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_114.tns";
            R = 32;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_10.tns";
            R = 32;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_39.tns";
            R = 731;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_395.tns";
            R = 32;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/massive_int.tns";
            R = 32;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/test.tns";
            R = 32;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/test1.tns";
            R = 17;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/massive.tns";
            R = 32;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/frs.tns";
            R = 2;
            std::cout << path << " : ";
        }
    }

    if(num_elements_per_thread == 1) {
        SECTION("single warp") {
            path = "../../tensors/3d_8.tns";
            R = 4;
        }
        SECTION("multiple warp") {
            path = "../../tensors/3d_8.tns";
            R = 5;
        }
        SECTION("multiple warp - l2 spans across warps") {
            path = "../../tensors/3d_10.tns";
            R = 4;
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_39.tns";
            R = 7;
            std::cout << path << " : ";
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_39.tns";
            R = 10;
        }
        SECTION("multiple block") {
            path = "../../tensors/3d_10.tns";
            R = 8;
        }
        SECTION("multiple block") {
            path = "../../tensors/small3.tns"; // has 51 nnz
            R = 4;
        }
    }

    Coo::coo X_coo = setup(path, matrices, R);
    csf X_csf(X_coo);
    //X_csf.print(mode, std::cout);

    //X_coo.sort(X_csf.level_map(mode));
    Device::array<gfloat> out = X_csf.mttkrp(mode, matrices);
    std::vector<gfloat> csf_gpu = out.retrieve();
    //std::vector<gfloat> coo_out = X_coo.mttkrp(mode, matrices);
    std::vector<gfloat> csf_cpu = X_csf.mttkrp_cpu(mode, matrices);

    REQUIRE(csf_cpu.size() == csf_gpu.size());
    for(guint i = 0; i < csf_gpu.size(); i++) {
        if(!nearlyEqual(csf_cpu[i], csf_gpu[i], 1.e-4))
            std::printf("csf_cpu[%d] = %.10f, csf_gpu[%d] = %.10f\n", i, csf_cpu[i], i, csf_gpu[i]);
    }
    /*
    REQUIRE(csf_out.size() == coo_out.size());
    for(guint i = 0; i < csf_out.size(); i++) {
        if(!nearlyEqual(csf_out[i], coo_out[i], 1.e-2))
            std::printf("csf_out[%d] = %.10f, coo_out[%d] = %.10f\n", i, csf_out[i], i, coo_out[i]);
    }
    */
    //REQUIRE_THAT(csf_out, Catch::Approx(coo_out).margin(1.e-7));
}

TEST_CASE("MTTKRP4 operation - mode 0 (4d)") {

    std::vector<matrix> matrices;
    guint R;
    std::string path;

    if(num_elements_per_thread == 4) {
        SECTION("single incomplete warp") {
            path = "../../tensors/4d_3_16.tns";
            R = 4;
        }
        SECTION("single full warp") {
            path = "../../tensors/4d_3_16.tns";
            R = 8;
        }
        SECTION("multiple warps") {
            path = "../../tensors/4d_3_16.tns";
            R = 60;
        }
        SECTION("multiple block") {
            path = "../../tensors/4d_13.tns";
            R = 128;
        }
        SECTION("multiple block") {
            path = "../../tensors/4d_190.tns";
            R = 32;
        }
        SECTION("multiple block") {
            path = "../../tensors/4d_2011.tns";
            R = 2;
        }
        SECTION("multiple block") {
            path = "../../tensors/4d_2506.tns";
            R = 32;
        }
    }

    if(num_elements_per_thread == 1) {
        /*
        SECTION("single incomplete warp") {
            path = "../../tensors/4d_3_16.tns";
            R = 1;
        }
        SECTION("single full warp") {
            path = "../../tensors/4d_3_16.tns";
            R = 2;
        }
        SECTION("multiple warp") {
            path = "../../tensors/4d_3_16.tns";
            R = 3;
        }
        SECTION("multiple warp") {
            path = "../../tensors/4d_13.tns";
            R = 4;
        }
        */
        /*
        SECTION("multiple block") {
            path = "../../tensors/4d_13.tns";
            R = 6;
        }
        SECTION("multiple block") {
            path = "../../tensors/4d_3_16.tns";
            R = 5;
        }
        */
        SECTION("multiple block") {
            path = "../../tensors/4d_190.tns";
            R = 4;
        }
    }

    Coo::coo X_coo = setup(path, matrices, R);
    csf X_csf(X_coo);
    X_csf.print(0, std::cout);

    X_coo.sort(X_csf.level_map(0));
    Device::array<gfloat> out = X_csf.mttkrp(0, matrices);
    std::vector<gfloat> csf_out = out.retrieve();
    std::vector<gfloat> coo_out = X_coo.mttkrp(0, matrices);

    REQUIRE_THAT(csf_out, Catch::Approx(coo_out).epsilon(1.e-3));
}
