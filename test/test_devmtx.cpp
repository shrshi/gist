#include <device.h>
#include <kernels.h>
#include <matrix.h>
#include <utils.h>
#include <catch.hpp>
#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>

namespace Catch {
    template <> struct StringMaker<matrix::matrix> {
        static std::string convert(matrix::matrix const& value) {
            std::ostringstream f;
            value.to_string(f);
            return f.str();
        }
    };
}

TEST_CASE("Random device matrix") {

    size_t mtx_nrows = 3, mtx_ncols = 4;
    device::objptr<matrix::matrix> dev_mtx(mtx_nrows, mtx_ncols);
    dev_mtx.rand_init();
    dev_mtx.retrieve().print(std::cout);
}

TEST_CASE("Device matrix multiplication") {

    size_t lhs_nrows = 4, lhs_ncols = 6, rhs_ncols = 8;

    device::objptr<matrix::matrix> dev_lhs(lhs_nrows, lhs_ncols);
    dev_lhs.rand_init();
    matrix::matrix lhs = dev_lhs.retrieve();

    device::objptr<matrix::matrix> dev_rhs(lhs_ncols, rhs_ncols);
    dev_rhs.rand_init();
    matrix::matrix rhs = dev_rhs.retrieve();

    device::objptr<matrix::matrix> dev_product(lhs_nrows, rhs_ncols);
    dev_product.mmul(dev_lhs, dev_rhs);
    matrix::matrix out = dev_product.retrieve();

    REQUIRE(out == lhs*rhs);
}

TEST_CASE("Densify device matrix") {

    size_t nrows = 2, ncols = 4, out_nrows = 4;
    device::objptr<matrix::matrix> dev_mtx(nrows, ncols);
    dev_mtx.rand_init();
    matrix::matrix in = dev_mtx.retrieve();

    std::vector<size_t> cur_rows{2, 4};
    dev_mtx.densify(out_nrows, cur_rows);
    matrix::matrix out = dev_mtx.retrieve();

    matrix::matrix res(out_nrows, ncols);
    res.insert(in, cur_rows);

    REQUIRE(res == out);
}

TEST_CASE("Linsolve with symmetric matrix") {

    size_t F_ = 4, I_ = 10, It_ = 5;

    device::objptr<matrix::matrix> m(F_, F_);
    m.rand_init();
    device::objptr<matrix::matrix> M(F_, F_);
    M.mmul(m, m, true, false);

    device::objptr<matrix::matrix> At(It_, F_);
    At.rand_init();
    matrix::matrix at = At.retrieve();
    std::vector<size_t> inds(It_, 0);
    for(size_t i = 0; i < It_; i++)
        inds[i] = 2*(i+1);

    device::objptr<matrix::matrix> A(I_, F_);
    A.constant(0.0f, cudaStreamLegacy);
    int info = M.chol();
    device::array<float> dev_lambda = A.linsolve(M, info, At, inds, std::string("L2"));
    A.multiply_rows(dev_lambda);
    matrix::matrix att(I_, F_, 0);
    att.insert(at, inds);
    M.mmul(m, m, true, false);
    matrix::matrix m_ = M.retrieve();
    info = m_.lapacke_linsolve(att);
    REQUIRE(info == 0);

    matrix::matrix a = A.retrieve();
    REQUIRE(a == att);
}

TEST_CASE("Linsolve with singular symmetric matrix") {

    size_t F_ = 4, I_ = 10, It_ = 5;

    matrix::matrix q({
            {-0.39361769, -0.52015745, -0.53897812,  0.53292019},
            {-0.1777789 ,  0.08119576,  0.71508657,  0.67115803},
            {-0.1874132 , -0.76178026,  0.44514099, -0.43175991},
            {0.88223357, -0.37753717, -0.00181212,  0.28129405}
    });
    matrix::matrix lambda({
            {0, 0, 0, 0},
            {0, 2, 0, 0},
            {0, 0, -3, 0},
            {0, 0, 0, 0}
    });
    device::objptr<matrix::matrix> Q(q);
    device::objptr<matrix::matrix> Lambda(lambda);
    device::objptr<matrix::matrix> M_(F_, F_);
    device::objptr<matrix::matrix> M(F_, F_);
    M_.mmul(Q, Lambda, false, false);
    M.mmul(M_, Q, false, true);

    device::objptr<matrix::matrix> At(It_, F_);
    At.rand_init();
    matrix::matrix at = At.retrieve();
    std::vector<size_t> inds(It_, 0);
    for(size_t i = 0; i < It_; i++)
        inds[i] = 2*(i+1);

    device::objptr<matrix::matrix> A(I_, F_);
    A.constant(0.0f, cudaStreamLegacy);
    int info = M.chol();
    device::array<float> dev_lambda = A.linsolve(M, info, At, inds, std::string("L2"));
    A.multiply_rows(dev_lambda);

    matrix::matrix att(I_, F_, 0);
    att.insert(at, inds);
    device::objptr<matrix::matrix> Att(att);
    M_.mmul(Q, Lambda, false, false);
    M.mmul(M_, Q, false, true);
    
    Att.mmul(A, M, false, false, 1.0f, -1.0f);
    //Att.retrieve().print(std::cout);
    float residnrm1 = Att.norm();
    //std::cout << "residual : " << residnrm << std::endl;

    matrix::matrix m_ = M.retrieve();
    info = m_.lapacke_linsolve(att);
    REQUIRE(info == 0);

    matrix::matrix b(I_, F_, 0);
    b.insert(at, inds);
    device::objptr<matrix::matrix> B(b);
    device::objptr<matrix::matrix> X(att);
    B.mmul(X, M, false, false, 1.0f, -1.0f);
    //B.retrieve().print(std::cout);
    float residnrm2 = B.norm();
    //std::cout << "residual : " << residnrm << std::endl;

    REQUIRE(std::abs(residnrm1 - residnrm2) <= 1e-4f);
}

TEST_CASE("Hadamard product between multiple matrices") {

    size_t n = 5, F = 4;
    std::vector<device::objptr<matrix::matrix>> mats;
    for(size_t i = 0; i < n; i++){
        mats.push_back(device::objptr<matrix::matrix>(F, F));
        mats[i].constant(i+1, cudaStreamLegacy);
    }

    device::objptr<matrix::matrix> out(F, F);
    matrix::matrix **mtxs;
    cuErrChk(cudaMalloc((void***)&mtxs, n * sizeof(matrix::matrix*)));
    SECTION("All matrices") {
        out.hadamard_product(mats, -1, mtxs, cudaStreamLegacy);
        cuErrChk(cudaDeviceSynchronize());
        matrix::matrix res(F, F, 120.0f);
        REQUIRE(res == out.retrieve());
    }
    SECTION("Excluding matrix at pos 3") {
        out.hadamard_product(mats, 3, mtxs, cudaStreamLegacy);
        matrix::matrix res(F, F, 30.0f);
        REQUIRE(res == out.retrieve());
    }
    cuErrChk(cudaFree(mtxs));
}

TEST_CASE("Clone device matrix") {

    size_t nrows = 10, ncols = 5;
    cudaStream_t stream;
    cuErrChk(cudaStreamCreate(&stream));

    device::objptr<matrix::matrix> dev_m(nrows, ncols);
    dev_m.rand_init();

    device::objptr<matrix::matrix> dev_m_clone(nrows, ncols);
    dev_m_clone.clone(dev_m, stream);

    cuErrChk(cudaStreamSynchronize(stream));
    REQUIRE(dev_m.retrieve() == dev_m_clone.retrieve());
    cuErrChk(cudaStreamDestroy(stream));
}

TEST_CASE("Normalize matrix") {

    size_t nrows = 10, ncols = 5;

    device::objptr<matrix::matrix> dev_m(nrows, ncols);
    dev_m.rand_init();
    matrix::matrix m = dev_m.retrieve();

    device::array<float> dev_lambda = dev_m.normalize(std::string("L2"));
    std::vector<float> lambda = m.normalize(std::string("L2"));

    std::vector<float> d_lambda = dev_lambda.retrieve();
    for(size_t i = 0; i < ncols; i++)
        REQUIRE(std::abs(d_lambda[i] - lambda[i]) <= 1e-4f);
    REQUIRE(dev_m.retrieve() == m);

    dev_lambda = dev_m.normalize(std::string("max"));
    lambda = m.normalize(std::string("max"));

    d_lambda = dev_lambda.retrieve();
    for(size_t i = 0; i < ncols; i++)
        REQUIRE(std::abs(d_lambda[i] - lambda[i]) <= 1e-4f);
    REQUIRE(dev_m.retrieve() == m);
}

TEST_CASE("Cholesky decomposition") {

    size_t n = 6;
    SECTION("SPD") {
        device::objptr<matrix::matrix> dev_x(n, n);
        dev_x.rand_init();
        device::objptr<matrix::matrix> dev_m(n, n);
        dev_m.mmul(dev_x, dev_x, true, false);
        dev_m.retrieve().print(std::cout);

        dev_m.chol();
        dev_m.retrieve().print(std::cout);
    }
    SECTION("SND") {
        matrix::matrix m({
                {-4, -12, 16},
                {-12, -37, 43},
                {16, 43, -98}
                });
        device::objptr<matrix::matrix> dev_m(m);
        dev_m.chol();
        dev_m.retrieve().print(std::cout);
    }
}
