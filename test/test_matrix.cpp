#include <type.h>
#include <matrix.h>
#include <utils.h>
#include <catch.hpp>
#include "test_utils.h"
#include <vector>
#include <iostream>
#include <sstream>

using namespace Gist;
using namespace Gist::Matrix;

TEST_CASE("Permute array of matrices") {

    SECTION("Array of size 3") {
        guint len = 3, n = 2;
        std::vector<matrix> mats;
        for(guint i = 0; i < len; i++)
            mats.push_back(matrix(n, n, i+1));

        std::vector<guint> order = {1, 2, 0};

        Utils::permute(mats, order);

        std::vector<matrix> res;
        for(guint i = 0; i < len; i++)
            res.push_back(matrix(n, n, order[i] + 1));

        for(guint i = 0; i < len; i++)
            REQUIRE(mats[i] == res[i]);
    }

    SECTION("Array of size 10") {
        guint len = 10, n = 2;
        std::vector<matrix> mats;
        for(guint i = 0; i < len; i++)
            mats.push_back(matrix(n, n, i+1));

        std::vector<guint> order = {4, 0, 2, 1, 6, 7, 3, 9, 5, 8};

        Utils::permute(mats, order);

        std::vector<matrix> res;
        for(guint i = 0; i < len; i++)
            res.push_back(matrix(n, n, order[i] + 1));

        for(guint i = 0; i < len; i++)
            REQUIRE(mats[i] == res[i]);

        Utils::permute(mats, order, true);

        res.clear();
        for(guint i = 0; i < len; i++)
            res.push_back(matrix(n, n, i + 1));

        for(guint i = 0; i < len; i++)
            REQUIRE(mats[i] == res[i]);
    }
}
