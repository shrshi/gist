#include <type.h>
#include <constants.h>
#include <kernels.h>
#include <matrix.h>
#include <device.h>
#include <tree.h>
#include <utils.h>
#include <err.h>
#include <catch.hpp>
#include "test_utils.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <vector>
#include <iostream>
#include <sstream>

using namespace Gist;

TEST_CASE("Scale matrix rows") {

    using Matrix::matrix;
    using dtl = Device::objptr<Tree::dev_level>;
    using dm = Device::objptr<Matrix::matrix>;
    using da = Device::array<gfloat>;
    using tl = Tree::dev_level;

    std::random_device rnd_device;
    std::mt19937 mersenne_engine(rnd_device());

    guint matrix_nrows = 100, matrix_ncols = 32;
    guint vals_size = 300;
    
    //input matrix
    matrix in(matrix_nrows, matrix_ncols);
    in.random_init();
    dm dev_in(in);

    //create random scaling factor vector
    std::vector<gfloat> vals(vals_size);
    std::uniform_real_distribution<> distr(0.0, 5.0);
    for(guint i = 0; i < vals_size; i++)
        vals[i] = distr(mersenne_engine);
    da dev_vals(vals);

    //create random tl object
    std::vector<guint> pidx(vals_size);
    std::uniform_int_distribution<guint> disti(1, matrix_nrows);
    for(guint i = 0; i < vals_size; i++)
        pidx[i] = disti(mersenne_engine);
    tl pos(vals_size, pidx.data());
    dtl dev_pos(pos);

    //alloc space for output
    da dev_out(vals_size * matrix_ncols);
    matrix chk(vals_size, matrix_ncols);

    guint num_elements = vals_size * matrix_ncols;
    guint storage_size = 2*sizeof(gfloat);
    guint num_elements_per_block = std::floor(shmem_size / storage_size);
    //guint num_elements_per_block = 258;
    guint num_blocks = std::max((gfloat)1.0, std::ceil((gfloat) num_elements / (gfloat)num_elements_per_block));

    std::printf("num_elements = %lu, num_elements_per_block = %lu, num_blocks = %lu\n", num_elements, num_elements_per_block, num_blocks);

    Kernels::select_scale <<< num_blocks, blockdim, shmem_size >>> (
            num_elements_per_block, 
            dev_in.object(), 
            dev_pos.object(), 
            dev_vals.object(), 
            dev_out.object());
    cuErrChk(cudaPeekAtLastError());
    cuErrChk(cudaDeviceSynchronize());

    matrix out(vals_size, matrix_ncols, dev_out.retrieve());
    chk.select_scale(in, pos, vals);

    REQUIRE(chk == out);
}
