#include <type.h>
#include <constants.h>
#include <kernels.h>
#include <matrix.h>
#include <device.h>
#include <tree.h>
#include <utils.h>
#include <err.h>
#include <catch.hpp>
#include "test_utils.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <vector>
#include <iostream>
#include <sstream>

using namespace Gist;

TEST_CASE("Scan") {
    using Matrix::matrix;
    using da = Device::array<guint>;

    guint num_elements;
    std::vector<guint> vals;

    SECTION("single warp") {
        num_elements = 20 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = i + 1;
    }
    SECTION("single warp") {
        num_elements = 20 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = i + 1;
    }
    SECTION("single full warp") {
        num_elements = 32 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = i + 1;
    }
    SECTION("single warp") {
        num_elements = 26 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = i + 1;
    }
    SECTION("multiple warps") {
        num_elements = 60 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = 1;
    }

    da dev_in(vals);
    da dev_out(num_elements);

    guint num_elements_per_block = std::min((int)num_elements, (int)blockdim * num_elements_per_thread);
    guint num_blocks = std::max((gfloat)1.0, std::ceil((gfloat) num_elements / (gfloat)num_elements_per_block));
    guint shmem_alloc = warp_size * (sizeof(gfloat) + sizeof(guint));

    std::printf("num_elements = %lu, num_elements_per_block = %lu, num_blocks = %lu\n", num_elements, num_elements_per_block, num_blocks);

    Kernels::scan <<< num_blocks, blockdim, shmem_alloc >>> (
            num_elements, 
            dev_in.object(), 
            dev_out.object());
    cuErrChk(cudaPeekAtLastError());
    cuErrChk(cudaDeviceSynchronize());

    std::vector<guint> out(dev_out.retrieve());
    std::vector<guint> chk(out.size(), vals[0]);
    for(int i = 1; i < num_elements; i++)
        chk[i] = vals[i] + chk[i-1];
    REQUIRE_THAT(out, Catch::Approx(chk).epsilon(1.e-3));
    /*
    std::vector<gfloat> chk(num_elements, 1.0f); 
    for(int i = 1; i < num_elements; i++) {
        if(flags.test(i)) chk[i] = 1.0f;
        else chk[i] = (gfloat)(chk[i] + chk[i-1]);
    }
    REQUIRE(chk == out);
    */
}
TEST_CASE("Segmented scan") {
    using Matrix::matrix;
    using bits = Utils::bitops;
    using dbits = Device::objptr<Utils::bitops>;
    using da = Device::array<gfloat>;

    guint num_elements, nsegments;
    bits flags;
    std::vector<gfloat> vals;

    SECTION("single warp with multiple segment") {
        num_elements = 20 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = i + 1;
        nsegments = 6;
        flags.resize(num_elements);
        for(guint i = 1; i <= nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }
    SECTION("single warp with single segment") {
        num_elements = 20 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = i + 1;
        nsegments = 1;
        flags.resize(num_elements);
        for(guint i = 0; i < nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }
    SECTION("single full warp with multiple segments") {
        num_elements = 32 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = i + 1;
        nsegments = 5;
        flags.resize(num_elements);
        for(guint i = 0; i < nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }
    SECTION("single warp with multiple segments") {
        num_elements = 26 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = i + 1;
        nsegments = 5;
        flags.resize(num_elements);
        for(guint i = 0; i < nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }
    SECTION("multiple warps with multiple segments") {
        num_elements = 60 * num_elements_per_thread;
        vals.resize(num_elements);
        for(guint i = 0; i < num_elements; i++)
            vals[i] = 1;
        nsegments = 10;
        flags.resize(num_elements);
        for(guint i = 0; i < nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }

    da dev_in(vals);
    dbits dev_flags(flags);
    da dev_out(num_elements);

    guint num_elements_per_block = std::min((int)num_elements, (int)blockdim * num_elements_per_thread);
    guint num_blocks = std::max((gfloat)1.0, std::ceil((gfloat) num_elements / (gfloat)num_elements_per_block));
    guint shmem_alloc = warp_size * (sizeof(gfloat) + sizeof(guint));

    std::printf("num_elements = %lu, num_elements_per_block = %lu, num_blocks = %lu\n", num_elements, num_elements_per_block, num_blocks);

    Kernels::segscan <<< num_blocks, blockdim, shmem_alloc >>> (
            num_elements, 
            dev_in.object(), 
            dev_flags.object(), 
            dev_out.object());
    cuErrChk(cudaPeekAtLastError());
    cuErrChk(cudaDeviceSynchronize());

    std::vector<gfloat> out(dev_out.retrieve());
    std::vector<gfloat> chk(out.size(), vals[0]);
    for(int i = 1; i < num_elements; i++)
        if(flags.test(i)) chk[i] = vals[i];
        else chk[i] = vals[i] + chk[i-1];
    REQUIRE(out == chk);
    /*
    std::vector<gfloat> chk(num_elements, 1.0f); 
    for(int i = 1; i < num_elements; i++) {
        if(flags.test(i)) chk[i] = 1.0f;
        else chk[i] = (gfloat)(chk[i] + chk[i-1]);
    }
    REQUIRE(chk == out);
    */
}

/*
TEST_CASE("Segmented reduction - single block") {

    using Matrix::matrix;
    using bits = Utils::bitops;
    using dbits = Device::objptr<Utils::bitops>;
    using da = Device::array<gfloat>;

    guint num_elements, nsegments;
    bits flags;

    SECTION("single full warp with single segment") {
        num_elements = 32;
        nsegments = 1;
        flags.resize(num_elements);
        for(guint i = 0; i < nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }
    SECTION("single full warp with multiple equal segments") {
        num_elements = 32;
        nsegments = 8;
        flags.resize(num_elements);
        for(guint i = 0; i < nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }
    SECTION("single full warp with multiple unequal segments") {
        num_elements = 32;
        nsegments = 3;
        flags.resize(num_elements);
        flags.set(0); flags.set(9); flags.set(21);
    }
    SECTION("single partially full warp with unequal segments") {
        num_elements = 26;
        nsegments = 3;
        flags.resize(num_elements);
        flags.set(0); flags.set(9); flags.set(21);
    }
    SECTION("multiple warps with equal segments") {
        num_elements = 40;
        nsegments = 8;
        flags.resize(num_elements);
        for(guint i = 0; i < nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }
    SECTION("multiple warps with equal segments - num_elements < blockDim.x") {
        num_elements = 250;
        nsegments = 25;
        flags.resize(num_elements);
        for(guint i = 0; i < nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }
    SECTION("multiple warps with equal segments - num_elements > blockDim.x") {
        num_elements = 260;
        nsegments = 26;
        flags.resize(num_elements);
        for(guint i = 0; i < nsegments; i++)
            flags.set(i * num_elements/nsegments);
    }


    flags.print(std::cout);
    dbits dev_flags(flags);
    da dev_out(num_elements);
    // number of values per block + size of bitops object for those values = shmem
    // assume num_elements_per_block is a multiple of 4
    //guint flags_size = 2*sizeof(guint) + sizeof(guint*); // data members
    //guint max_datatype_size = max(sizeof(guint), sizeof(gfloat));
    //guint num_elements_per_block = std::min((double)(num_elements), std::ceil((shmem_size - flags_size) / (2*max_datatype_size)));
    guint num_elements_per_block = std::min((double)(num_elements), std::ceil(shmem_size / (sizeof(gfloat) + sizeof(guint))));

    guint num_blocks = std::max((gfloat)1.0, std::ceil((gfloat) num_elements / (gfloat)num_elements_per_block));

    std::printf("num_elements = %lu, num_elements_per_block = %lu, num_blocks = %lu\n", num_elements, num_elements_per_block, num_blocks);

    Kernels::segred <<< num_blocks, blockdim, shmem_size >>> (
            num_elements_per_block, 
            num_elements, 
            dev_flags.object(), 
            dev_out.object());
    cuErrChk(cudaPeekAtLastError());
    cuErrChk(cudaDeviceSynchronize());

    std::vector<gfloat> out(dev_out.retrieve());
    
    std::vector<gfloat> chk(num_elements, 1.0f); 
    for(int i = 1; i < num_elements; i++) {
        if(flags.test(i)) chk[i] = 1.0f;
        else chk[i] = (gfloat)(chk[i] + chk[i-1]);
    }
    REQUIRE(chk == out);
}
*/
