#include <svd.h>
#include <device.h>
#include <matrix.h>
#include <catch.hpp>
#include <iostream>
#include <vector>
#include <sstream>

namespace Catch {
    template <> struct StringMaker<matrix::matrix> {
        static std::string convert(matrix::matrix const& value) {
            std::ostringstream f;
            value.to_string(f);
            return f.str();
        }
    };
}

TEST_CASE("Randomized svd of submatrix") {
    
    size_t nrows = 8, ncols = 4, out_nrows = 16;
    device::objptr<matrix::matrix> dev_mtx(nrows, ncols);
    dev_mtx.rand_init();

    std::vector<size_t> cur_rows{2, 4, 6, 8, 10, 12, 14, 16};
    device::objptr<matrix::matrix> dev_U = svd::rsvd(out_nrows, ncols, cur_rows, dev_mtx);

    dev_U.retrieve().print(std::cout);
}
