#include <matrix.h>
#include <csf.h>
#include <random>
#include <string>
#include <sstream>
namespace Catch {
    template <> struct StringMaker<Gist::Matrix::matrix> {
        static std::string convert(Gist::Matrix::matrix const& value) {
            std::stringstream f;
            value.stringify(f);
            return f.str();
        }
    };

    template <> struct StringMaker<Gist::Csf::csf> {
        static std::string convert(Gist::Csf::csf const& value) {
            std::stringstream f;
            value.stringify(f);
            return f.str();
        }
    };
}
